
  import React from 'react';
  import { AdminDashboard } from './views/Login';
  import { Login } from './views/Login';
  import { ManageUser } from './views/Login';
  import { ManageRole } from './views/Login';
  import { ManageLead } from './views/Login';
  import { ManageSource } from './views/Login';
  import { ManageCourse } from './views/Login';
  import { ManageCampaign } from './views/Login';
  import { ManageAdmission } from './views/Login';
  import { ManageAccess } from './views/Login';
  import { ManageLeaves } from './views/Login';
  import { ChangePassword } from './views/Login';
  import { ForgotPassword } from './views/Login';
  import { Route, Switch, Redirect } from 'react-router-dom';
  import './assets/styles/style.css';
  import './assets/plugin/waves/waves.min.css';
  import './assets/plugin/bootstrap/css/bootstrap.min.css';
  import {toast} from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';
  import 'bootstrap/dist/js/bootstrap.min.js';
  
  //import {toast} from 'react-toastify';
  import "react-datepicker/dist/react-datepicker.css";
 
  toast.configure() 

function App() {
  return (
    <div className="App">
        {/* <NavBar /> */}
        <Switch>
          <Route exact path="/AdminDashboard" component={AdminDashboard} />
          <Route exact path="/">
            <Redirect to="/Login" />
          </Route>
          <Route exact path="/ManageUser" component={ManageUser} />
          <Route exact path="/Login" component={Login} />
          <Route exact path="/ManageRole" component={ManageRole} />
          <Route exact path="/ManageLead" component={ManageLead} />
          <Route exact path="/ManageSource" component={ManageSource} />
          <Route exact path="/ManageCourse" component={ManageCourse} />
          <Route exact path="/ManageCampaign" component={ManageCampaign} />
          <Route exact path="/ManageAdmission" component={ManageAdmission} />
          <Route exact path="/ManageAccess" component={ManageAccess} />
          <Route exact path="/ManageLeaves" component={ManageLeaves} />
          <Route exact path="/ChangePassword" component = {ChangePassword} />
          <Route exact path ="/ForgotPassword" component={ForgotPassword}/>
          {/* <Route exact path="/Modal" component={Modal}/> */}
        </Switch>
    </div>
  );
}

export default App;
