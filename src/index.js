// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
import React from 'react';

import ReactDOM from 'react-dom';
import App from './App';
import { HashRouter as Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import './assets/plugin/bootstrap/css/bootstrap.min.css';
// import 'bootstrap/dist/css/bootstrap.css';
//import 'bootstrap/dist/css/bootstrap-theme.css';
// var cors = require('cors');

// App.use(cors()) // Use this after the variable declaration
//import { Routes } from './routes'; // where we are going to specify our routes

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById('root')
);




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
