import React from 'react';
import { Login } from './views/Login/index';
// import { About } from './views/About';
import { NavBar } from './components/NavBar';
import { Route, Switch, Redirect } from 'react-router-dom';

export const Routes = () => {
  return (
    <div>
      <NavBar />
      <Switch>
        <Route exact path="/Login" component={Login} />
        <Route exact path="/">
          <Redirect to="/Login" />
        </Route>
        {/* <Route exact path="/About" component={About} /> */}
      </Switch>
    </div>
  );
};