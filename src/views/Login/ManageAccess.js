import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import {toast} from 'react-toastify';
import ReactPaginate from 'react-paginate';
import { Multiselect } from 'multiselect-react-dropdown';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');

	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
    }
	const dropDownStyle = { chips: { background: "blue" } }
	class ManageAccess extends React.Component {
		constructor(props){
			super(props)
			this.state = {
				offset: 0,
				perPage: 10,
				currentPage: 0,
				addaccess :false,
				editaccess :false,
				accessList: [],
				userList : [],
				accessId : '',
				selectUser : '',
				selectScreen : [],
				Edit : false,
				View : false,
				Delete : false,
			}
			this.logout = this.logout.bind(this);
			this.handlePageClick = this.handlePageClick.bind(this);
		}
		ShowDivfunction = () =>{
			const {addaccess} = this.state;
			this.setState({
				addaccess : !addaccess
			}
			)
		}
		
		ShowEditDivFunction = (accessData) =>{
			const {editaccess} = this.state;
			this.setState({
				editaccess : !editaccess,
				accessId : accessData.access_id,
				selectUser :accessData.user_id,
				selectScreen: accessData.aceess_screen,
			}
			)
			if(accessData.edit_permission === 'true'){
				this.setState({Edit : true})
			}else{
				this.setState({Edit : false})
			}
				
			if(accessData.view_permission === 'true'){
				this.setState({View : true})
			}else{
				this.setState({View : false})
			}

			if(accessData.delete_permission === 'true'){
				this.setState({Delete : true})
			}else{
				this.setState({Delete : false})
			}
				// View : accessData.view_permission,
				// Delete : accessData.delete_permission,
			
			console.log(accessData);
		}

		getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				const apiUrl = apiConfig.API_Path +'ManageAccess/getAccessPermissions';
				const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_role'));
				formData.append('ManageUser', "Manage Access");
				const options = {
					method: 'POST',
					body: formData
				}
				fetch(apiUrl,options)
				.then(res => res.json() )
				.then(
					(result) => {
						
						localStorage.setItem("editPermission",result[0]['edit_permission']);
						localStorage.setItem('deletePermission',result[0]['delete_permission']);
						localStorage.setItem('viewPermission',result[0]['view_permission']);
						console.log(result)
					}
					
				)
			}
		}
		componentDidMount() {
			$(".commonRemove").removeClass('current');
			$("#ManageAccess").addClass('current');
			this.getDetails();
			this.getAccessPermissions();
		}

		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};

		getDetails() {
			const apiUrl = apiConfig.API_Path +'ManageAccess/getDetails';
		
			fetch(apiUrl)
			  	.then(res => res.json() )
			  	.then(
				(result) => {
					const accessList = result;
					const slice = accessList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
						pageCount: Math.ceil(accessList.length / this.state.perPage),
						accessList : slice
					})
				})
		}
		
		deleteaccess(accessId) {
			const { accesss } = this.state;
		
			const apiUrl = apiConfig.API_Path +'ManageAccess/deleteDetails';
			const formData = new FormData();
			formData.append('accessId', accessId);
		
			const options = {
			  method: 'POST',
			  body: formData
			}
		
			fetch(apiConfig.API_Path +'ManageAccess/deleteDetails', options)
			
			.then(res => res.json())
			.then(
				(result) => {
					toast.success(result.message)
					this.getDetails();
				},
				(error) => {
					toast.error("Opps! Something went Wrong.")
				}
			)
			}
		
			
		onChangeSearch(e) {
			const formData = new FormData();
			formData.append('searchData', e.target.value);
		
			const options = {
				method: 'POST',
				body: formData
			}
			fetch(apiConfig.API_Path +'ManageAccess/onChangeSearch',options)
				.then(res => res.json() )
				.then(
				(result) => {
					this.setState({
						accessList: result
					});
				}
				// ,
				// (error) => {
				//   this.setState({ error });
				// }
				)
				
				
			}
			
		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}
		
		render() {
			
			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				const { accessList} = this.state;
				return (
						<div>
							<header id="header">
								<div className="main-menu">
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<Navigation />
									</div>
								</div>
				
			
								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Manage Access</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
					<div id="wrapper">
						<div className="main-content">
			
						<div className="row small-spacing">
							{this.state.addaccess? <ShowDiv /> :null}
			
							{this.state.editaccess? <ShowDiv 
								accessId = {this.state.accessId} 
								selectUser = {this.state.selectUser}
								selectScreen = {this.state.selectScreen}
								Edit = {this.state.Edit}
								View = {this.state.View}
								Delete  = {this.state.Delete}
								
							/> :null}
						</div>
						<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">Access List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-access-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add Access details</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch"placeholder="Search by Role" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
										<table id="access-list" className="table table-striped table-bordered display" >
										<thead>
												<tr>
													<th>Sr. No.</th>
													<th>Role</th>
													<th>Selected screen</th>
													<th>Permissions</th>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? <th>Action</th> : null}
												</tr>
											</thead>
											{localStorage.getItem('viewPermission') === 'true' ?
											<tbody>
											{accessList.map((accessData,index) => (
												<tr key={accessData.access_id}>
													<React.Fragment> 
													<td>{index+1}</td>
													<td>{accessData.role_name}</td>
													<td>{accessData.aceess_screen}</td>
													{/* <td>{(accessData.edit_permission != 'null' && accessData.edit_permission != 'false'&& accessData.edit_permission != 'undefined') ? 'Edit' : '' }&nbsp;&nbsp;{(accessData.view_permission != 'null' && accessData.edit_permission != 'false'&& accessData.edit_permission != 'undefined') ? 'View' : '' } &nbsp;&nbsp;{(accessData.delete_permission != 'null' && accessData.edit_permission != 'false'&& accessData.edit_permission != 'undefined') ? 'Delete' : '' } </td> */}
													<td>{(accessData.edit_permission != 'null' && accessData.edit_permission != 'false'&& accessData.edit_permission != 'undefined') ? 'Edit' : '' }&nbsp;&nbsp;{(accessData.view_permission != 'null' && accessData.view_permission != 'false'&& accessData.view_permission != 'undefined') ? 'View' : '' } &nbsp;&nbsp;{(accessData.delete_permission != 'null' && accessData.delete_permission != 'false'&& accessData.delete_permission != 'undefined') ? 'Delete' : '' } </td>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? 
													<td>
														{localStorage.getItem('editPermission') === 'true' ?
														<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light"  title="Edit access Details" onClick={() => this.ShowEditDivFunction(accessData)}>
															<i className="ico fa fa-edit"></i>
														</button> : null }
														{localStorage.getItem('deletePermission') === 'true' ?
														<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete access" onClick={() => this.deleteaccess(accessData.access_id)}>
															<i className="ico fa fa-trash"></i>
														</button> : null}
													</td>
													:null}
													</React.Fragment> 
												</tr>
											))}
												
											</tbody>
											:null}
										</table>
										<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
									</div>
								</div>
								
							</div>
										
								<footer id="footer" className="footer">
									<ul className="list-inline">
										<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
										<li><a href="#">Privacy</a></li>
										<li><a href="#">Coockies</a></li>
										<li><a href="#">Legal Notice</a></li>
									</ul>
								</footer>
							</div>
						</div>
					</div>
				);
			}
		}
	}
	
		class ShowDiv extends Component{
			
			constructor(props){
				super(props)
				this.state = {
					hideDiv :true,
					userList : [],
					selectUser : '',
					selectScreen : '',
					// Edit : false,
					// View : false,
					// Delete : false,
					objectArray : [{name : 'Screen 1', key : 'Screen 1'},{name : 'Screen 2', key : 'Screen 2'},{name : 'Screen 3', key : 'Screen 3'},{name : 'Screen 4', key : 'Screen 4'},{name : 'Screen 5', key : 'Screen 5'},{name : 'Screen 6', key : 'Screen 6'}]
					
				}
				this.getUserRoleDeatils();
				this.onChangeselectUser = this.onChangeselectUser.bind(this);
				this.onChangeselectScreen = this.onChangeselectScreen.bind(this);
				this.handleCheckEdit = this.handleCheckEdit.bind(this);
				this.handleCheckView = this.handleCheckView.bind(this);
				this.handleCheckDelete = this.handleCheckDelete.bind(this);
				this.onSubmit = this.onSubmit.bind(this);
				
			}
			
			getUserRoleDeatils(){
				const apiUrl = apiConfig.API_Path +'ManageAccess/getUserRoleDeatils';
			
				fetch(apiUrl)
				  .then(res => res.json() )
				  .then(response => {
					let coursedetails = response.map(userData => {
					  return {value: userData.role_id, display: userData.role_name, Selected : false}
					}); 
					
						// this.setState({
						// userList: [{value: '', display: 'Select Course',Selected: true}].concat(coursedetails)
						// });
						const tempArray = [];
						coursedetails.forEach(element => {
							
							if(element.value === this.props.selectUser){
								tempArray.push({value: element.value, display: element.display,Selected: true})
							}else{
								tempArray.push({value: element.value, display: element.display,Selected: false})
							}
						});
						
						this.setState({
							userList: [{value: '', display: 'Select Role',Selected: true}].concat(tempArray)
							});
						// this.setState({
						// 	userList: response
						// 	});
						
					}
					
				  )
				 
			  }
			handleCheckEdit(e){
				this.setState({ 
					Edit : e.target.checked
				})
			}
			handleCheckView(e){
				this.setState({ 
					View : e.target.checked
				})
			}
			handleCheckDelete(e){
				this.setState({ 
					Delete : e.target.checked
				})
			}

			onChangeselectUser(e) {
				this.setState({
					selectUser :  e.target.value
				});
			}
			onChangeselectScreen(e) {
				this.setState({
					selectScreen :  e.target.value
				});
				
				// const tempArray = [];
				// selectedList.forEach(element => {
				// 	tempArray.push(element.name)
				// 			//}
				// });
				// console.log(selectedList);
				// console.log(tempArray);
				// this.setState({
				// 	selectScreen :  tempArray
				// });

			}
			
			
			onSubmit(e){
			e.preventDefault();
			
			
                if(this.state.selectUser  || this.props.selectUser){
                    if((this.state.selectScreen !='' && this.state.selectScreen != undefined)|| (this.props.selectScreen != '' && this.props.selectScreen != undefined)){
						if(this.state.Edit  || this.props.Edit || this.state.View  || this.props.View || this.state.Delete  || this.props.Delete){
                        
	
                                const formData = new FormData();
                                
                                if(this.state.selectUser ){
                                    formData.append("selectUser", this.state.selectUser);
                                }else{
                                    formData.append("selectUser", this.props.selectUser);
                                }
                                //subjectName
                                if(this.state.selectScreen ){
                                    formData.append("selectScreen", this.state.selectScreen);
                                }else{
                                    formData.append("selectScreen", this.props.selectScreen);
								}
								if(this.state.Delete === false || this.state.Delete === true){
									
                                    formData.append("DeleteAccess", this.state.Delete);
                                }else{
									formData.append("DeleteAccess", this.props.Delete);
									
								}
								
								if(this.state.Edit === false || this.state.Edit === true){
                                    formData.append("EditAccess", this.state.Edit);
                                }else{
									formData.append("EditAccess", this.props.Edit);
								}
								
								if(this.state.View === false || this.state.View === true){
                                    formData.append("ViewAccess", this.state.View);
                                }else{
									 formData.append("ViewAccess", this.props.View);
                                }

                                
                                formData.append("accessId", this.props.accessId);
                                
                                if(this.props.accessId){
                                    axios.post(apiConfig.API_Path +'ManageAccess/updateDetails',formData,{
                                        // headers: { 'Content-Type': 'application/json,charset=UTF-8' }
                                        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
                                    })
                                    .then(res => 
                                        {
                                        if(res.data.status.status === '1'){
                                            toast.success(res.data.status.message)
                                            this.setState(
                                                { 
                                                    accessId : '',
                                                    selectUser : '',
                                                    selectScreen : [],
													hideDiv : false,
													Edit : false,
													View : false,
													Delete : false,
                                
                                                })
                                                
                                                window.location.reload(false);
                                        } else{
                                            toast.error(res.data.status.message)
                                            this.setState(
                                            { 
                                                selectUser : '',
                                                selectScreen : [],
												accessId : '',
												Edit : false,
												View : false,
												Delete : false,
                                            })
                                        } 
                                        }
                                        );
                                }else{
                                    
                                    
                                axios.post(apiConfig.API_Path +'ManageAccess/insertDetails',formData,{
                                    // headers: { 'Content-Type': 'application/json,charset=UTF-8' }
                                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
                                    })
                                    .then(res => 
                                        {
                                        if(res.data.status.status === '1'){
                                            toast.success(res.data.status.message)
                                            this.setState(
                                                { 
                                                    selectUser : '',
                                                    selectScreen : [],
													hideDiv : false,
													Edit : false,
													View : false,
													Delete : false,
                                
                                                })
                                                
                                                window.location.reload(false);
                                        } else{
                                            toast.error(res.data.status.message)
                                            this.setState(
                                            { 
                                                selectUser : '',
												selectScreen : [],
												Edit : false,
												View : false,
												Delete : false,
                                                
                                            })
                                        } 
                                        }
                                        ) ;
                                    }
						}else{
							//this.refs.selectScreen.focus();
							toast.error("Select at least one permission.")
							} 
                    }else{
                        this.refs.selectScreen.focus();
                        toast.error("Select screen is required.")
                        }
                }else{
                    this.refs.selectUser.focus();
                    toast.error("Select course is required.")
                    }	
						
		}
	
		HideDivFunction = () =>{
			const {hideDiv} = this.state;
			this.setState({
				hideDiv : !hideDiv
				
			}
			)
		
		}
		
		render(){
			
			return(
				<div>
					{this.state.hideDiv?
					<div className="col-xs-12">
						<div className="add-access">
							<div className="col-xs-12">
								<div className="box-content card white">
									{this.props.accessId? <h4 className="box-title">Update access Details</h4> : <h4 className="box-title">Add access Details </h4> }
									<form action="#" method="post">
										<div className="card-content">
											<div className="col-lg-4 col-md-6">
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-user"></i></label></div>
													{/* <select id="ig-4" className="form-control" ref="selectaccess" defaultValue={this.state.selectaccess ||this.props.selectaccess } onChange={(e)=>this.onChangeselectaccess(e)}> */}
													<select id="ig-4" className="form-control" ref="selectUser" defaultValue={this.state.selectUser ||this.props.selectUser } onChange={(e)=>this.onChangeselectUser(e)}>
														{this.state.userList.map((userData) => <option key={userData.value} value={userData.value} selected={userData.Selected}>{userData.display}</option>)}
														{/* <option value=''>Select Course</option>
														{this.state.userList.map((userData) => <option key={userData.course_id} value={userData.course_id }  >{userData.course_name}</option>)} */}
				
													</select>
												</div>
											</div>
											
											<div className="col-lg-4 col-md-6">
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-33" className="btn btn-default"><i className="fa fa-user"></i></label></div>
													<select id="ig-33"  className="form-control" ref="selectScreen" defaultValue={this.state.selectScreen ||this.props.selectScreen } onChange={(e)=>this.onChangeselectScreen(e)}>
														
													<option>Select Screens</option>
													<option value="Dashboard" >Dashboard</option>
													<option value="Manage User" >Manage User</option>
													<option value="Manage Role">Manage Role</option>
													<option value="Manage Leads">Manage Leads</option>
													<option value="Manage Source">Manage Source</option>
													<option value="Manage Course">Manage Course</option>
													<option value="Manage Campaign">Manage Campaign</option>
													<option value="Manage Admission">Manage Admission</option>
													<option value="Manage Access">Manage Access</option>
													<option value="Manage Leaves">Manage Leaves</option>
													</select>          
												</div>
												
											</div>
											<div className="col-lg-4 col-md-6">
												<div className="input-group margin-bottom-20">
												
													<label><b>Permissions:&nbsp;&nbsp;  </b></label>
													<label><input type="checkbox" onChange={this.handleCheckEdit} defaultChecked={this.state.Edit || this.props.Edit} />&nbsp;Edit &nbsp;&nbsp;</label>   
													<label><input type="checkbox" onChange={this.handleCheckView} defaultChecked={this.state.View || this.props.View }/>&nbsp;View &nbsp;&nbsp;</label>
													<label><input type="checkbox" onChange={this.handleCheckDelete} defaultChecked={this.state.Delete || this.props.Delete}/>&nbsp;Delete &nbsp;&nbsp;</label>
												</div>
												
											</div>
											<div className="input-group">
												<div className="col-lg-6 col-md-6">
												{this.props.accessId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
													
												</div>
												<div className="col-lg-6 col-md-6">
													<button id="hide-add-role" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>: null}
				</div>
			
			)
		}
		}
	
		
	
	


	export default ManageAccess;