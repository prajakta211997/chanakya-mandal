import React from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import {toast} from 'react-toastify';
const apiConfig = require('../../api_config/apiConfig.json');
  const IconSize = {
    width: '170px',
    height : '80px',
      
  }

class Login extends React.Component {
    constructor(props){
      super(props);
      this.onChangeEmailId = this.onChangeEmailId.bind(this);
      this.onChangePassword = this.onChangePassword.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
      this.state = {
        email_id : '',
        password : ''
      }
    }
    onChangeEmailId(e) {
      this.setState({
        email_id :  e.target.value
      });
      
    }
    onChangePassword(e) {
      this.setState({
        password :  e.target.value
      });
    }

    onSubmit(e){
      e.preventDefault();
      if(this.state.email_id !== ''){
        if(this.state.password ){
          const obj = {
            email_id : this.state.email_id,
            password : this.state.password

          };
          axios.post(apiConfig.API_Path + 'Login/checkLogin',obj,{
          // headers: { 'Content-Type': 'application/json,charset=UTF-8' }
          headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        })
          .then(res => 
            {
              
              if(res.data.status.status == '1'){
                localStorage.setItem("user_id",res.data.data.user_id);
                localStorage.setItem("user_role",res.data.data.user_role);
                localStorage.setItem("userLoggedIn", "True");
                localStorage.setItem("role_name",res.data.data.role_name);
                //console.log(res.data.data.accessScreen);
                var activeScreen = '';
                const accessDetails = res.data.data.accessScreen;
                
                if(res.data.data.role_name === 'Master Admin'){
                  localStorage.setItem('Manage User',"True");
                  localStorage.setItem('Dashboard', "True");
                  localStorage.setItem('Manage Role', "True");
                  localStorage.setItem('Manage Leads', "True");
                  localStorage.setItem('Manage Source', "True");
                  localStorage.setItem('Manage Course', "True");
                  localStorage.setItem('Manage Campaign', "True");
                  localStorage.setItem('Manage Admission', "True");
                  localStorage.setItem('Manage Access', "True");
                  localStorage.setItem('Manage Leaves', "True");
                  
                  activeScreen = apiConfig.Server_Path +'ManageUser';
                  
                  toast.success("Login Successfully.")
                }else{
                accessDetails.forEach(element => {
                  localStorage.setItem(element.aceess_screen, "True");
                  if(element.aceess_screen === 'Dashboard'){
                    activeScreen = apiConfig.Server_Path +'AdminDashboard';
                  }
                  if(element.aceess_screen === 'Manage User'){
                    activeScreen = apiConfig.Server_Path +'ManageUser';
                  }
                  if(element.aceess_screen === 'Manage Role'){
                    activeScreen = apiConfig.Server_Path +'ManageRole';
                  }
                  if(element.aceess_screen === 'Manage Leads'){
                    activeScreen = apiConfig.Server_Path +'ManageLead';
                  }
                  if(element.aceess_screen === 'Manage Source'){
                    activeScreen = apiConfig.Server_Path +'ManageSource';
                  }
                  if(element.aceess_screen === 'Manage Course'){
                    activeScreen = apiConfig.Server_Path +'ManageCourse';
                  }
                  if(element.aceess_screen === 'Manage Campaign'){
                     activeScreen = apiConfig.Server_Path +'ManageCampaign';
                  }
                  if(element.aceess_screen === 'Manage Admission'){
                    activeScreen = apiConfig.Server_Path +'ManageAdmission';
                  }
                  if(element.aceess_screen === 'Manage Access'){
                   activeScreen = apiConfig.Server_Path +'ManageAccess';
                  }
                  if(element.aceess_screen === 'Manage Leaves'){
                    activeScreen = apiConfig.Server_Path +'ManageLeaves';
                  }
                  
                });
                toast.success(res.data.status.message)
              
                // var activeScreen = '';
                // if(accessDetails[0]['aceess_screen']=== 'Dashboard'){
                //   activeScreen = 'AdminDashboard';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage User'){
                //   activeScreen = 'ManageUser';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Role'){
                //   activeScreen = 'ManageRole';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Leads'){
                //   activeScreen = 'ManageLead';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Source'){
                //   activeScreen = 'ManageSource';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Course'){
                //   activeScreen = 'ManageCourse';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Campaign'){
                //   activeScreen = 'ManageCampaign';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Admission'){
                //   activeScreen = 'ManageAdmission';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Access'){
                //   activeScreen = 'ManageAccess';
                // }
                // if(accessDetails[0]['aceess_screen']=== 'Manage Leaves'){
                //   activeScreen = 'ManageLeaves';
                // }
              }
                setTimeout(() => {
                  window.location = activeScreen;
              },1000)
                
              } else{
                toast.error(res.data.status.message)
                this.setState(
                  { email_id : '',
                    password : ''
                  })
              } 
            });
          }else{
            this.refs.password.focus();
            toast.error("Password is required.")
          }
        }else{
          this.refs.email_id.focus();
          toast.error("User name is required.")
        }
    }

    render() {
      return (
        <div id="single-wrapper">
            <form action="#" className="frm-single">
                <div className="inside">
                <div className="title">
                    <img src={logo} alt="Chanakya Mandal" title="Chanakya Mandal User Login" style={IconSize} />
                </div>
                <div className="frm-title">Login</div>
                <div className="frm-input"><input type="text" ref="email_id" placeholder="Username" className="frm-inp" value={this.state.email_id} onChange={this.onChangeEmailId} /><i className="fa fa-user frm-ico"></i></div>
                <div className="frm-input"><input type="password" ref="password" placeholder="Password" className="frm-inp" value={this.state.password} onChange={this.onChangePassword} /><i className="fa fa-lock frm-ico"></i></div>
                <div className="clearfix margin-bottom-20">
                    <div className="pull-left">
                    </div>
                    <div className="pull-right"><a href="ForgotPassword" className="a-link"><i className="fa fa-unlock-alt"></i>Forgot password?</a></div>
                </div>
                
              <input type="submit" value="Login" className="frm-submit"  onClick={this.onSubmit}/>
              
                
                <div className="frm-footer">© Chanakya Mandal Pariwar <script type="text/JavaScript"> document.write(new Date().getFullYear()); </script>.</div>
            </div>
            </form>
        </div>
      );
    }
  }

// const Login = props => {
//   return (
    
//   );
// };

export default Login;