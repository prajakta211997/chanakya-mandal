import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import {toast} from 'react-toastify';
import DatePicker from "react-datepicker";
import ReactPaginate from 'react-paginate';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');


	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
    }

	class ManageLeaves extends React.Component {
		constructor(props){
			super(props)
			this.state = {
				offset: 0,
				perPage: 10,
				currentPage: 0,
				addleave :false,
				editleave :false,
				leaveList: [],
				leaveId : '',
				selectUser : '',
				leaveType : '',
				leaveReason : '',
				startDate : '',
				endDate : '',
				leaveDays: '',
				
				
			}
			this.logout = this.logout.bind(this);
			this.handlePageClick = this.handlePageClick.bind(this);
			this.sendLeaveRequest =  this.sendLeaveRequest.bind(this);
			this.acceptLeaveRequest = this.acceptLeaveRequest.bind(this);
			this.rejectLeaveRequest = this.rejectLeaveRequest.bind(this);
		}

		ShowDivfunction = () =>{
			const {addleave} = this.state;
			this.setState({
				addleave : !addleave
			}
			)
		}
		
		ShowEditDivFunction = (leaveData) =>{
			const {editleave} = this.state;
			this.setState({
				editleave : !editleave,
				leaveId : leaveData.leave_id,
				selectUser : leaveData.selected_user,
				leaveType : leaveData.leave_type,
				leaveReason : leaveData.leave_reason,
				startDate : leaveData.start_date,
				endDate :leaveData.end_date,
				leaveDays: leaveData.no_of_days,
				
			}
			)
		}

		sendLeaveRequest = (leaveData) =>{
			const formData = new FormData();
				formData.append("leaveId", leaveData.leave_id);
				axios.post(apiConfig.API_Path +'ManageLeaves/sendLeaveRequest',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
				.then(res => 
					{
					if(res.data.status.status === '1'){
						toast.success(res.data.status.message)
						this.setState(
							{ 
								leaveId : '',
							})
							
							window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								leaveId : '',
							})
						} 
						}
						);
		}

		acceptLeaveRequest = (leaveData) =>{
			const formData = new FormData();
				formData.append("leaveId", leaveData.leave_id);
				axios.post(apiConfig.API_Path +'ManageLeaves/acceptLeaveRequest',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
				.then(res => 
					{
					if(res.data.status.status === '1'){
						toast.success(res.data.status.message)
						this.setState(
							{ 
								leaveId : '',
							})
							
							window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								leaveId : '',
							})
						} 
						}
						);
		}
		
		
		rejectLeaveRequest = (leaveData) =>{
			const formData = new FormData();
				formData.append("leaveId", leaveData.leave_id);
				axios.post(apiConfig.API_Path +'ManageLeaves/rejectLeaveRequest',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
				.then(res => 
					{
					if(res.data.status.status === '1'){
						toast.success(res.data.status.message)
						this.setState(
							{ 
								leaveId : '',
							})
							
							window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								leaveId : '',
							})
						} 
						}
						);
		}

		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};

		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}

		getDetails() {
			const apiUrl = apiConfig.API_Path +'ManageLeaves/getDetails';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	leaveList: result
					// });
					const leaveList = result;
					const slice = leaveList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(leaveList.length / this.state.perPage),
						leaveList : slice
					})
				}
				
			  )
			  
			  
		  }
		  
		  
		  getIndividualLeavesDetails() {
			const apiUrl = apiConfig.API_Path +'ManageLeaves/getIndividualLeavesDetails';
			const formData = new FormData();
			formData.append('userId', localStorage.getItem('user_id'));
			const options = {
				method: 'POST',
				body: formData
			}

			fetch(apiUrl,options)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	leaveList: result
					// });
					const leaveList = result;
					const slice = leaveList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(leaveList.length / this.state.perPage),
						leaveList : slice
					})
				}
				
			  )
			  
			  
		  }
		  getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				const apiUrl = apiConfig.API_Path +'ManageLeaves/getAccessPermissions';
				const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_role'));
				formData.append('ManageUser', "Manage Leaves");
				const options = {
					method: 'POST',
					body: formData
				}
				fetch(apiUrl,options)
				.then(res => res.json() )
				.then(
					(result) => {
						
						localStorage.setItem("editPermission",result[0]['edit_permission']);
						localStorage.setItem('deletePermission',result[0]['delete_permission']);
						localStorage.setItem('viewPermission',result[0]['view_permission']);
						console.log(result)
					}
					
				)
			}
		}
		componentDidMount() {
			$(".commonRemove").removeClass('current');
			$("#ManageLeaves").addClass('current');
			this.checkRole();
			// this.getDetails();
		 	this.getAccessPermissions();
		}

		checkRole(){
			var roledata = localStorage.getItem('role_name');
			if(roledata === 'Master Admin'){
				this.getDetails();
				
			}else{
				this.getIndividualLeavesDetails();
				
			}
		}
		deleteleave(leaveId) {
		const { leaves } = this.state;
		const formData = new FormData();
		formData.append('leaveId', leaveId);
	
		const options = {
			method: 'POST',
			body: formData
		}
	
		fetch(apiConfig.API_Path +'ManageLeaves/deleteDetails', options)
		
		.then(res => res.json())
		.then(
			(result) => {
				toast.success(result.message)
				this.getDetails();
			},
			(error) => {
				toast.error("Opps! Something went Wrong.")
			}
		)
		}
		
			
		onChangeSearch(e) {
			//const searchData = e.target.value
			const formData = new FormData();
			formData.append('searchData', e.target.value);
		
			const options = {
				method: 'POST',
				body: formData
			}
			fetch(apiConfig.API_Path +'ManageLeaves/onChangeSearch',options)
				.then(res => res.json() )
				.then(
				(result) => {
					this.setState({
						leaveList: result
					});
				}
				// ,
				// (error) => {
				//   this.setState({ error });
				// }
				)
				
				
			}
		
		render() {
			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				const { leaveList} = this.state;
				return (
						<div>
							<header id="header">
								<div className="main-menu">
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<Navigation />
									</div>
								</div>
				
			
								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Manage leave</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
					<div id="wrapper">
						<div className="main-content">
			
						<div className="row small-spacing">
							{this.state.addleave? <ShowDiv /> :null}
			
							{this.state.editleave? <ShowDiv 
								leaveId = {this.state.leaveId} 
								selectUser = {this.state.selectUser}
								leaveType = {this.state.leaveType}
								leaveReason  = {this.state.leaveReason}
								startDate = {this.state.startDate}
								endDate = {this.state.endDate}
								leaveDays = {this.state.leaveDays}
								
								
							/> :null}
						</div>
						<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">Leaves List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-leave-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add Leave</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch"placeholder="Search by User name" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
										<table id="leave-list" className="table table-striped table-bordered display" >
										<thead>
												<tr>
													<th>Sr. No.</th>
													{localStorage.getItem('role_name') === 'Master Admin'? <th>User Name</th> :null}
													<th>Leave Type</th>
													<th>Leave Reason</th>
													<th>Start Date</th>
													<th>End Date</th>
													<th>No. of Days</th>
													{localStorage.getItem('role_name') !== 'Master Admin'? <th>Request for Leave</th> : null}
													{localStorage.getItem('role_name') === 'Master Admin'? <th>Request for Leave</th> : null}
													<th>Leave Status</th>
													{(localStorage.getItem('editPermission') === 'true' && localStorage.getItem('role_name') !== 'Master Admin') || (localStorage.getItem('deletePermission') === 'true'  && localStorage.getItem('role_name') !== 'Master Admin') ? <th>Action</th> : null}
												</tr>
											</thead>
											{/* <tfoot>
												<tr>
													<th>Sr. No.</th>
													<th>leave Name</th>
													<th>Action</th>
												</tr>
											</tfoot> */}
											{localStorage.getItem('viewPermission') === 'true' ?
											<tbody>
											{leaveList.map((leaveData,index) => (
												<tr key={leaveData.leave_id}>
													<React.Fragment> 
													<td>{index+1}</td>
													{localStorage.getItem('role_name') === 'Master Admin'? 
													<td>{leaveData.user_name}</td>
													:null}
													<td>{leaveData.leave_type}</td>
													<td>{leaveData.leave_reason}</td>
													<td>{leaveData.start_date}</td>
													<td>{leaveData.end_date}</td>
													<td>{leaveData.no_of_days}</td>
													{localStorage.getItem('role_name') !== 'Master Admin'  ? <td><button type="button" className="btn btn-info  btn-xs waves-effect waves-light"  title="Edit leave Details" onClick={() => this.sendLeaveRequest(leaveData)}>Send Request</button></td> : null}
													{localStorage.getItem('role_name') === 'Master Admin' && leaveData.approve_leave != 0 ? <td><button type="button" className="btn btn-info  btn-xs waves-effect waves-light"  title="Edit leave Details" onClick={() => this.acceptLeaveRequest(leaveData)}>Approve Request</button> <br/><br/><button type="button" className="btn btn-danger  btn-xs waves-effect waves-light"  title="Edit leave Details" onClick={() => this.rejectLeaveRequest(leaveData)}>Reject Request</button></td> : null}
													 {leaveData.approve_leave == 2  ? <td><b>Approved</b></td> : null}
													 {leaveData.approve_leave == 3 ? <td><b>Rejected</b></td> : null}
													 {leaveData.approve_leave == 1 || leaveData.approve_leave == 0 ? <td></td> : null}
													{(localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true') && localStorage.getItem('role_name') !== 'Master Admin' ? 
													<td>
													
														{localStorage.getItem('editPermission') === 'true' && localStorage.getItem('role_name') !== 'Master Admin' ?
														<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light"  title="Edit leave Details" onClick={() => this.ShowEditDivFunction(leaveData)}>
															<i className="ico fa fa-edit"></i>
														</button> : null}
														{localStorage.getItem('deletePermission') === 'true'  && localStorage.getItem('role_name') !== 'Master Admin' ?
														<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete leave" onClick={() => this.deleteleave(leaveData.leave_id)}>
															<i className="ico fa fa-trash"></i>
														</button> : null}
													</td> : null}
													</React.Fragment> 
												</tr>
											))}
												
											</tbody>
											:null}
										</table>
										<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
									</div>
								</div>
								
							</div>
										
								<footer id="footer" className="footer">
									<ul className="list-inline">
										<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
										<li><a href="#">Privacy</a></li>
										<li><a href="#">Coockies</a></li>
										<li><a href="#">Legal Notice</a></li>
									</ul>
								</footer>
							</div>
						</div>
					</div>
				);
			}
		}
		  }
	
		class ShowDiv extends Component{
			constructor(props){
				super(props)
				this.state = {
					hideDiv :true,
					userList : [],
					selectUser : '',
					leaveType : '',
					leaveReason : '',
					startDate : '',
					endDate : '',
					leaveDays: '',
					
					
					
				}
				this.getUserDeatils();
				this.onChangeselectUser = this.onChangeselectUser.bind(this);
				this.onChangeleaveType = this.onChangeleaveType.bind(this);
				this.onChangeleaveReason = this.onChangeleaveReason.bind(this);
				this.onChangestartDate = this.onChangestartDate.bind(this);
				this.onChangeendDate = this.onChangeendDate.bind(this);
				this.onChangeleaveDays = this.onChangeleaveDays.bind(this);
				this.onSubmit = this.onSubmit.bind(this);
				
			}
			getUserDeatils(){
				const apiUrl = apiConfig.API_Path +'ManageLeaves/getUserDeatils';
			
				fetch(apiUrl)
				  .then(res => res.json() )
				  .then(response => {
					let coursedetails = response.map(userData => {
					  return {value: userData.user_id, display: userData.user_name, Selected : false}
					}); 
					
						// this.setState({
						// userList: [{value: '', display: 'Select Course',Selected: true}].concat(coursedetails)
						// });
						const tempArray = [];
						coursedetails.forEach(element => {
							
							if(element.value === this.props.selectUser){
								tempArray.push({value: element.value, display: element.display,Selected: true})
							}else{
								tempArray.push({value: element.value, display: element.display,Selected: false})
							}
						});
						
						this.setState({
							userList: [{value: '', display: 'Select User',Selected: true}].concat(tempArray)
							});
						// this.setState({
						// 	userList: response
						// 	});
						
					}
					
				  )
				 
			  }
			onChangeselectUser(e) {
				this.setState({
					selectUser :  e.target.value
				});
			}
			onChangeleaveType(e) {
				this.setState({
					leaveType :  e.target.value
				});
			}
			onChangeleaveReason(e) {
				this.setState({
					leaveReason :  e.target.value
				});
			}

			onChangestartDate = startDate  => {
				this.setState({
					startDate :  startDate
				});
			}
			
			// onChangeendDate = endDate  => {
			// 	this.setState({
			// 		endDate :  endDate
			// 	});
			// }
			
			// onChangestartDate(e) {
			// 	this.setState({
			// 		startDate :  e.target.value
			// 	});
				
			// }
			onChangeendDate = endDate  =>  {
				this.setState({
					endDate :  endDate
				});
				if(this.state.startDate){
				var date1 = new Date(this.state.startDate);
				}else{
					var date1 = new Date(this.props.startDate);
				}
				var date2 = new Date(endDate);
				
				
				if(Date.parse(date2) <= Date.parse(date1)){
	
					toast.error("Please select valid Start And End Date.");
					this.refs.startDate.focus();
					return false;

				}else{
					var timeDiff = Math.abs(date2.getTime() - date1.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
					this.setState({
						leaveDays :  diffDays
					});
				}
			}
			
			onChangeleaveDays(e) {
				this.setState({
					leaveDays :  e.target.value
				});
			}
			
			
			onSubmit(e){
			e.preventDefault();
			
			if(this.state.selectUser  || this.props.selectUser){
				if(this.state.leaveType  || this.props.leaveType){
					if(this.state.leaveReason  || this.props.leaveReason){
						if(this.state.startDate  || this.props.startDate){
							if(this.state.endDate  || this.props.endDate){
								if(this.state.startDate){
									var startDate = this.state.startDate;
								}
								if(this.props.startDate){
									var startDate = this.props.startDate;
								}
								if(this.state.endDate){
									var endDate = this.state.endDate;
								}
								if(this.props.endDate){
									var endDate = this.props.endDate;
								}
								if(Date.parse(endDate) <= Date.parse(startDate)){
	
									toast.error("Please select valid Start And End Date.");
									this.refs.startDate.focus();
									return false;
	
								}
								if(this.state.leaveDays  || this.props.leaveDays){
									
									
				
											const formData = new FormData();
											//coureName
											if(this.state.selectUser){
												formData.append("selectUser", this.state.selectUser);
											}else{
												formData.append("selectUser", this.props.selectUser);
											}
	
											//leaveType
											if(this.state.leaveType){
												formData.append("leaveType", this.state.leaveType);
											}else{
												formData.append("leaveType", this.props.leaveType);
											}
	
											//leaveReason
											if(this.state.leaveReason){
												formData.append("leaveReason", this.state.leaveReason);
											}else{
												formData.append("leaveReason", this.props.leaveReason);
											}
	
											//startDate
											var startnow = this.state.startDate
											var startdate = startnow.toLocaleDateString("en-GB", {year: "numeric",month: "long",day: "2-digit" });
											if(this.state.startDate){
												formData.append("startDate", startdate);
											}else{
												formData.append("startDate", this.props.startDate);
											}
				
											//endDate
											var endnow = this.state.endDate
											var enddate = endnow.toLocaleDateString("en-GB", {year: "numeric",month: "long",day: "2-digit" });
											if(this.state.endDate){
												formData.append("endDate", enddate);
											}else{
												formData.append("endDate", this.props.endDate);
											}
											//leaveDays
											if(this.state.leaveDays){
												formData.append("leaveDays", this.state.leaveDays);
											}else{
												formData.append("leaveDays", this.props.leaveDays);
											}
	
											
											formData.append("leaveId", this.props.leaveId);
											
											if(this.props.leaveId){
												axios.post(apiConfig.API_Path +'ManageLeaves/updateDetails',formData,{
													// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
													headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
												})
												.then(res => 
													{
													if(res.data.status.status === '1'){
														toast.success(res.data.status.message)
														this.setState(
															{ 
																leaveId : '',
																selectUser : '',
																leaveType : '',
																leaveReason : '',
																startDate : '',
																endDate : '',
																leaveDays: '',
																hideDiv : false
											
															})
															
															window.location.reload(false);
													} else{
														toast.error(res.data.status.message)
														this.setState(
														{ 
															selectUser : '',
															leaveType : '',
															leaveReason : '',
															startDate : '',
															endDate : '',
															leaveDays: '',
															leaveId : '',
														})
													} 
													}
													);
											}else{
												
												
											axios.post(apiConfig.API_Path +'ManageLeaves/insertDetails',formData,{
												// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
												headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
												})
												.then(res => 
													{
													if(res.data.status.status === '1'){
														toast.success(res.data.status.message)
														this.setState(
															{ 
																selectUser : '',
																leaveType : '',
																leaveReason : '',
																startDate : '',
																endDate : '',
																leaveDays: '',
																hideDiv : false
											
															})
															
															window.location.reload(false);
													} else{
														toast.error(res.data.status.message)
														this.setState(
														{ 
															selectUser : '',
															leaveType : '',
															leaveReason : '',
															startDate : '',
															endDate : '',
															leaveDays: '',
														})
													} 
													}
													) ;
												}
										
								}else{
									this.refs.leaveDays.focus();
									toast.error("no.of days is required.")
									}
							}else{
								// this.refs.endDate.focus();
								toast.error("End date is required.")
								}	
						}else{
							// this.refs.startDate.focus();
							toast.error("Start date is required.")
							}	
					}else{
						this.refs.leaveReason.focus();
						toast.error("Leave reason is required.")
						}						
				}else{
					this.refs.leaveType.focus();
					toast.error("Leave type is required.")
					}				
			}else{
				this.refs.selectUser.focus();
				toast.error("Select user is required.")
				}
		}
	
			HideDivFunction = () =>{
				const {hideDiv} = this.state;
				console.log(hideDiv);
				this.setState({
					hideDiv : !hideDiv
					
				}
				)
			
			}
		
			render(){
				const { startDate } = this.state;
				const { endDate } = this.state;
				return(
					<div>
						{this.state.hideDiv?
						<div className="col-xs-12">
							<div className="add-leave">
								<div className="col-xs-12">
									<div className="box-content card white">
										{this.props.leaveId? <h4 className="box-title">Update leave </h4> : <h4 className="box-title">Add leave </h4> }
										<form action="#" method="post">
											<div className="card-content">
												<div className="col-lg-4 col-md-4">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-user"></i></label></div>
														<select id="ig-4" className="form-control" ref="selectUser" defaultValue={this.state.selectUser ||this.props.selectUser } onChange={(e)=>this.onChangeselectUser(e)}>
															{this.state.userList.map((userData) => <option key={userData.value} value={userData.value} selected={userData.Selected}>{userData.display}</option>)}
															{/* <option value=''>Select Course</option>
															{this.state.userList.map((userData) => <option key={userData.course_id} value={userData.course_id }  >{userData.course_name}</option>)} */}
					
														</select>
													</div>
												</div>
												<div className="col-lg-4 col-md-4">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-adjust"></i></label></div>
														<select id="ig-4" className="form-control" ref="leaveType" defaultValue={this.state.leaveType ||this.props.leaveType } onChange={(e)=>this.onChangeleaveType(e)}>
															<option>Select Leave Type</option>
															<option value="Sick Leave">Sick Leave</option>
															<option value="Paid Leave">Paid Leave</option>
															<option value="Paid Leave">General Leave</option>
														</select>
													</div>
												</div>
												<div className="col-lg-4 col-md-4">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-info"></i></label></div>
														<input id="ig-3" type="text" className="form-control" ref="leaveReason" placeholder="Leave Reason" defaultValue={this.state.leaveReason ||this.props.leaveReason } onChange={(e)=>this.onChangeleaveReason(e)}/>
													</div>
												</div>
												<div className="col-lg-4 col-md-4">
													{/* <label>Start Date</label> */}
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-8" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
														{/* <input type="date" className="form-control" ref="startDate" placeholder="mm/dd/yyyy" id="startDate" defaultValue={this.state.startDate ||this.props.startDate } onChange={(e)=>this.onChangestartDate(e)}/> */}
														<DatePicker selected={startDate} dateFormat="dd MMMM yyyy"   isClearable placeholderText="Start Date" onChange={(date)=>this.onChangestartDate(date)} className="form-control" />
													</div>
												</div>
												<div className="col-lg-4 col-md-4">
													{/* <label>End Date</label> */}
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-9" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
														{/* <input type="date" className="form-control"  ref="endDate" placeholder="mm/dd/yyyy" id="endDate" defaultValue={this.state.endDate ||this.props.endDate } onChange={(e)=>this.onChangeendDate(e)}/> */}
														<DatePicker selected={endDate} dateFormat="dd MMMM yyyy"   isClearable placeholderText="End Date" onChange={(date)=>this.onChangeendDate(date)} className="form-control" />
													</div>
												</div>
												<div className="col-lg-4 col-md-4">
													{/* <label></label> */}
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-book"></i></label></div>
														<input id="ig-3" type="text" className="form-control" ref="leaveDays" placeholder="Number of days" defaultValue={this.state.leaveDays ||this.props.leaveDays } onChange={(e)=>this.onChangeleaveDays(e)}/>
													</div>
												</div>
												
												<div className="input-group pull">
													<div className="col-lg-6 col-md-6">
													{this.props.leaveId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
														
													</div>
													<div className="col-lg-6 col-md-6">
														<button id="hide-add-role" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>: null}
					</div>
				
				)
			}
		}
	
		
	
	


	export default ManageLeaves;