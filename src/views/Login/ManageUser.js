import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import {toast} from 'react-toastify';
import DatePicker from "react-datepicker";
import $ from 'jquery';
import Navigation from './Navigation';
const apiConfig = require('../../api_config/apiConfig.json');

	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
	}
	
	
	class ManageUser extends React.Component {
		
    constructor(props){
        super(props)
        this.state = {
			addUser :false,
			editUser :false,
			userList: [],
			permissionList :[],
			userId : '',
			userName : '',
			emailId : '',
			mobileNumber : '',
			userRole : '',
			userPhoto : null,
			userBirthDate : '',
			userAddress : '',
			uploading: false,
			images: [],
			offset: 0,
			perPage: 10,
			currentPage: 0,
			userPassword : ''
			
		}
		this.logout = this.logout.bind(this);
		this.handlePageClick = this.handlePageClick.bind(this);
		
	}
	
		ShowDivfunction = () =>{
			const {addUser} = this.state;
			this.setState({
				addUser : !addUser
			}
			)
		}
	
		ShowEditDivFunction = (userData) =>{
			const {editUser} = this.state;
			this.setState({
				editUser : !editUser,
				userId : userData.user_id,
				userName : userData.user_name,
				emailId : userData.user_email_id,
				mobileNumber : userData.user_mobile_number,
				userRole : userData.user_role,
				userPhoto : userData.user_uploaded_photo,
				userBirthDate : userData.user_birth_date,
				userAddress : userData.user_address,
				uploading: false,
				userPassword : userData.user_password,
				images: []

			}
			)
		}
		
		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};

		getDetails() {
			const apiUrl = apiConfig.API_Path + 'ManageUser/getDetails';
		
			fetch(apiUrl)
			.then(res => res.json() )
			.then(
				(result) => {
					// this.setState({
					// 	userList: result
					// });
					
					const userList = result;
					const slice = userList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(userList.length / this.state.perPage),
						userList : slice
					})
				}
				
			)
			
			
		}
		getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				var role = localStorage.getItem('role_name');
				if(role=== 'Master Admin'){
					localStorage.setItem("editPermission",'true');
					localStorage.setItem('deletePermission','true');
					localStorage.setItem('viewPermission','true');
				}else{
					const apiUrl = apiConfig.API_Path + 'ManageUser/getAccessPermissions';
					const formData = new FormData();
					formData.append('userId', localStorage.getItem('user_role'));
					formData.append('ManageUser', "Manage User");
					const options = {
						method: 'POST',
						body: formData
					}
					fetch(apiUrl,options)
					.then(res => res.json() )
					.then(
						
						(result) => {
							this.setState({
								permissionList: result
							});
							
							localStorage.setItem("editPermission",result[0]['edit_permission']);
							localStorage.setItem('deletePermission',result[0]['delete_permission']);
							localStorage.setItem('viewPermission',result[0]['view_permission']);
						}
						
					)
				}
			}
		}
		
		componentDidMount() {
			$(".commonRemove").removeClass('current');
			$("#ManageUser").addClass('current');
			this.getDetails();
			this.getAccessPermissions();
		}

		deleteUser(userId) {
		const { users } = this.state;

		const formData = new FormData();
		formData.append('userId', userId);

		const options = {
			method: 'POST',
			body: formData
		}

		fetch(apiConfig.API_Path + 'ManageUser/deleteUser', options)
		
		.then(res => res.json())
		.then(
			(result) => {
				toast.success(result.message)
				this.getDetails();
			},
			(error) => {
				toast.error("Opps! Something went Wrong.")
			}
		)
		}

		onChangeSearch(e) {
			const formData = new FormData();
			formData.append('searchData', e.target.value);
		
			const options = {
				method: 'POST',
				body: formData
			}
			fetch(apiConfig.API_Path + 'ManageUser/onChangeSearch',options)
				.then(res => res.json() )
				.then(
				(result) => {
					this.setState({
						userList: result
					});
				}
				// ,
				// (error) => {
				//   this.setState({ error });
				// }
				)
				
				
		}
		
		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}
	
		render() {
			const { userList} = this.state;

			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				
				return (
						<div>
							<header id="header">
								<div className="main-menu">
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<Navigation />
									</div>
								</div>
				

								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Manage Users</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
							<div id="wrapper">
					<div className="main-content">

						<div className="row small-spacing">
							{this.state.addUser? <ShowDiv /> :null}

							{this.state.editUser? <ShowDiv 
								userId = {this.state.userId} 
								userName = {this.state.userName}
								emailId = {this.state.emailId}
								mobileNumber = {this.state.mobileNumber}
								userRole  = {this.state.userRole}
								userPhoto = {this.state.userPhoto}
								userBirthDate = {this.state.userBirthDate}
								userAddress = {this.state.userAddress}
								userPassword = {this.state.userPassword}
							/> :null}
						</div>
						<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">User List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-user-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add User</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch"placeholder="Search by user name, email id, mobile number, user role" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
									
										<table id="user-list" className="table table-striped table-bordered display" >
											<thead>
												<tr>
													<th>Sr. No.</th>
													<th>Photo</th>
													<th>User Name</th>
													<th>Email</th>
													<th>Mobile Number</th>
													<th>User Role</th>
													<th>Date of Birth</th>
													<th>Address</th>
													<th>Password</th>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? <th>Action</th> : null}
												</tr>
											</thead>
											{/* <tfoot>
												<tr>
													<th>Sr. No.</th>
													<th>Photo</th>
													<th>User Name</th>
													<th>Email</th>
													<th>Mobile Number</th>
													<th>User Role</th>
													<th>Date of Birth</th>
													<th>Address</th>
													<th>Action</th>
												</tr>
											</tfoot> */}
											{localStorage.getItem('viewPermission') === 'true' ?
											<tbody>
											
											{userList.map((userdata,index)  => ( 
											
												<tr >
												<React.Fragment> 
													<td>{index+1}</td>
													<td><img src={apiConfig.Upload_Path + `${userdata.user_uploaded_photo}`} className="img-responsive img-thumbna" style={IconSize}/></td>
													<td>{userdata.user_name}</td>
													<td>{userdata.user_email_id}</td>
													<td>{userdata.user_mobile_number}</td>
													<td>{userdata.user_role}</td>
													<td>{userdata.user_birth_date}</td>
													<td>{userdata.user_address}</td>
													<td>{userdata.user_password}</td>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ?  
												<td>
														
														{localStorage.getItem('editPermission') === 'true' ?
														<button type="button" className="btn btn-warning btn-circle btn-lg waves-effect waves-light"  title="Edit User Details" onClick={() => this.ShowEditDivFunction(userdata)}>
															<i className="ico fa fa-edit"></i>
														</button> :null
														}
														{localStorage.getItem('deletePermission') === 'true' ?
														<button type="button" className="btn btn-danger btn-circle btn-lg waves-effect waves-light" title="Delete User" onClick={() => this.deleteUser(userdata.user_id)}>
															<i className="ico fa fa-trash"></i>
														</button> : null 
														}
														
													</td>
													: null}
												</React.Fragment>
												</tr>
											
													) )}
												
											</tbody>
											:null}
										</table>
										{/* {this.state.postData} */}
										<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
									</div>
								</div>
								
							</div>
										
								<footer id="footer" className="footer">
									<ul className="list-inline">
										<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
										<li><a href="#">Privacy</a></li>
										<li><a href="#">Coockies</a></li>
										<li><a href="#">Legal Notice</a></li>
									</ul>
								</footer>
							</div>
						</div>
					</div>
				);
			}
		}
  	}

	class ShowDiv extends Component{
		constructor(props){
			super(props)
			this.state = {
				hideDiv :true,
				userName : '',
				emailId : '',
				mobileNumber : '',
				roleList : [],
				userRole : '',
				userPhoto : null,
				userBirthDate : '',
				userAddress : '',
				uploading: false,
				userPassword : '',
				  images: [],
				  
			}
			this.getRoleDetails();
			this.onChangeUserName = this.onChangeUserName.bind(this);
			this.onChangeEmailId = this.onChangeEmailId.bind(this);
			this.onChangeMobileNumber = this.onChangeMobileNumber.bind(this);
			this.onChangeUserRole = this.onChangeUserRole.bind(this);
			this.onChangeUserPhoto = this.onChangeUserPhoto.bind(this);
			this.onChangeUserBirthDate = this.onChangeUserBirthDate.bind(this);
			this.onChangeUserAddress = this.onChangeUserAddress.bind(this);
			this.onChangeUserPassword = this.onChangeUserPassword.bind(this);
			this.onImageChange = this.onImageChange.bind(this);
			this.onSubmit = this.onSubmit.bind(this);
			
		}
		getRoleDetails(){
			const apiUrl = apiConfig.API_Path + 'ManageUser/getRoleDetails';
			
				fetch(apiUrl)
				  .then(res => res.json() )
				  .then(response => {
					let userdetails = response.map(userData => {
					  return {value: userData.role_id, display: userData.role_name, Selected : false}
					}); 
					
						// this.setState({
						// userList: [{value: '', display: 'Select Course',Selected: true}].concat(coursedetails)
						// });
						const tempArray = [];
						userdetails.forEach(element => {
							
							if(element.value === this.props.userRole){
								tempArray.push({value: element.value, display: element.display,Selected: true})
							}else{
								tempArray.push({value: element.value, display: element.display,Selected: false})
							}
						});
						
						this.setState({
							roleList: [{value: '', display: 'Select Role',Selected: true}].concat(tempArray)
							});
						// this.setState({
						// 	userList: response
						// 	});
						
					}
					
				  )
		}
		onChangeUserName(e) {
			this.setState({
				userName :  e.target.value
			});
		}
		onChangeEmailId(e) {
			this.setState({
				emailId :  e.target.value
			});
		}
		onChangeMobileNumber(e) {
			this.setState({
				mobileNumber :  e.target.value
			});
		}
		onChangeUserRole(e) {
			this.setState({
				userRole :  e.target.value
			});
		}
		onChangeUserPhoto(e) {
			this.setState({
			userPhoto :  e.target.value
			});
		}
		onChangeUserBirthDate = startDate  => {
			this.setState({
				userBirthDate :  startDate
			});
		}
		onChangeUserAddress(e) {
			this.setState({
				userAddress :  e.target.value
			});
		}
		onChangeUserPassword(e){
			this.setState({
				userPassword :  e.target.value
			});
		}
		onImageChange = e => {
			
			//const files = Array.from(e.target.files)
			var file = e.target.files[0];
                
                var fileSize = e.target.files[0].size / 1024 / 1024;
                if(fileSize > 2){
                    toast.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 2MB.");
                    return false;
                }
                else { 
                    if (file.name.split('.').pop()==='jpg' || file.name.split('.').pop()==='png'|| file.name.split('.').pop()==='jpeg') {
                        this.setState({ uploading: true,
							userPhoto : e.target.files[0]})
                    } else {
                        toast.error('Please upload correct File , File extension should be (.jpg|.png|.jpeg)');
                       // $scope.ShowSpinnerStatus = false;
                        return false;
                    }
                }
			// this.setState({ uploading: true,
			// 	userPhoto : e.target.files[0]})
			
		};
	  
		onSubmit(e){
		e.preventDefault();
		
		if(this.state.userName  || this.props.userName){
			if(this.state.userName){
				if (!(this.state.userName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
					this.refs.userName.focus();
					toast.error("Valid user name is required.")
					return false;
				}
			}
			if(this.props.userName){
				if (!(this.props.userName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
					this.refs.userName.focus();
					toast.error("Valid user name is required.")
					return false;
				}
			}
			
			
			if(this.state.emailId || this.props.emailId){
				if(this.state.emailId){
					if (!(this.state.emailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/))) {
						this.refs.emailId.focus();
						toast.error("Valid email id is required.")
						return false;
					}
				}
				if(this.props.emailId){
					if (!(this.props.emailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/))) {
						this.refs.emailId.focus();
						toast.error("Valid email id is required.")
						return false;
					}
				}
				if(this.state.mobileNumber || this.props.mobileNumber){
					if(this.state.mobileNumber){
						if (!(this.state.mobileNumber.match(/^[7-9][0-9]{9}$/))) {
							this.refs.mobileNumber.focus();
							toast.error("Valid mobile number is required.")
							return false;
						}
					}
					if(this.props.mobileNumber){
						if (!(this.props.mobileNumber.match(/^[7-9][0-9]{9}$/))) {
							this.refs.mobileNumber.focus();
							toast.error("Valid mobile number is required.")
							return false;
						}
					}
					
					if(this.state.userRole || this.props.userRole){
						if(this.state.userPhoto || this.props.userPhoto){
							if(this.state.userBirthDate || this.props.userBirthDate){
								if(this.state.userBirthDate){
									
									const birthdate = this.state.userBirthDate;
									const birth = birthdate.toDateString();
									const DOBSplit = birth.split(" ");
									const birthYear = DOBSplit[3];
									var dtCurrent = new Date();
									if ((dtCurrent.getFullYear() - birthYear ) <= 18) {
										//this.refs.userBirthDate.focus();
											toast.error("Valid birth date is required.")
											return false;
									}
								}
								if(this.props.userBirthDate){
									// const birthdate = this.props.userBirthDate;
									// const birth = birthdate.toDateString();
									// const DOBSplit = birth.split(" ");
									// const birthYear = DOBSplit[3];
									// var dtCurrent = new Date();
									// if ((dtCurrent.getFullYear() - birthYear ) <= 18) {
									// 	this.refs.userBirthDate.focus();
									// 		toast.error("Valid birth date is required.")
									// 		return false;
									// }
								}
								if(this.state.userAddress || this.props.userAddress){
									if(this.state.userAddress){
										if (!(this.state.userAddress.match(/^([a-zA-Z0-9-',.\s]{1,500})$/))) {
											this.refs.userAddress.focus();
											toast.error("Valid address is required.")
											return false;
										}
									}
									if(this.props.userAddress){
										if (!(this.props.userAddress.match(/^([a-zA-Z0-9-',.\s]{1,500})$/))) {
											this.refs.userAddress.focus();
											toast.error("Valid address is required.")
											return false;
										}
									}
									
									const formData = new FormData();
									// formData.append("userName", this.state.userName);
									// //formData.append("file", selectedFile);
									// formData.append("emailId",  this.state.emailId);
									// formData.append("mobileNumber", this.state.mobileNumber);
									// formData.append("userRole", this.state.userRole);
									// formData.append("userPhoto", this.state.userPhoto);
									// formData.append("userBirthDate", this.state.userBirthDate);
									// formData.append("userAddress", this.state.userAddress);
									if(this.state.userName){
										formData.append("userName", this.state.userName);
									}else{
										formData.append("userName", this.props.userName);
									}
									
									formData.append("userId", this.props.userId);
									if(this.state.emailId){
										formData.append("emailId", this.state.emailId);
									}else{
										formData.append("emailId", this.props.emailId);
									}
									if(this.state.mobileNumber){
										formData.append("mobileNumber",this.state.mobileNumber);
									}else{
										formData.append("mobileNumber",this.props.mobileNumber);
									}
									if(this.state.userRole){
										formData.append("userRole", this.state.userRole);
									}else{
										formData.append("userRole", this.props.userRole);
									}
									if(this.state.userPhoto){
										formData.append("userPhoto", this.state.userPhoto);
									}else{
										formData.append("userPhoto", this.props.userPhoto);
										}
									
									
									var startnow = this.state.userBirthDate
									var userBirthDate = startnow.toLocaleDateString("en-GB", {year: "numeric",month: "long",day: "2-digit" });
									if(this.state.userBirthDate){
										formData.append("userBirthDate", userBirthDate);
									}else{
										formData.append("userBirthDate", this.props.userBirthDate);
									}
									if(this.state.userAddress){
										formData.append("userAddress", this.state.userAddress);
									}else{
										formData.append("userAddress", this.props.userAddress);
									}
									if(this.state.userPassword){
										formData.append("userPassword", this.state.userPassword);
									}else{
										formData.append("userPassword", this.props.userPassword);
									}
									if(this.props.userId){
										axios.post(apiConfig.API_Path + 'ManageUser/updateDetails',formData,{
											// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
											headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
										})
										.then(res => 
											{
											if(res.data.status.status === '1'){
												toast.success(res.data.status.message)
												this.setState(
													{ 
														userId : '',
														userName : '',
														emailId : '',
														mobileNumber : '',
														userRole : '',
														userPhoto : '',
														userBirthDate : '',
														userAddress : '',
														hideDiv : false
									
													})
													
													window.location.reload(false);
											} else{
												toast.error(res.data.status.message)
												this.setState(
												{ 
													userName : '',
													emailId : '',
													mobileNumber : '',
													userRole : '',
													userPhoto : '',
													userBirthDate : '',
													userAddress : '',
													userId : '',
												})
											} 
											}
											);
									}else{
										
										
									axios.post(apiConfig.API_Path + 'ManageUser/insertDetails',formData,{
										// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
										headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
										})
										.then(res => 
											{
											if(res.data.status.status === '1'){
												toast.success(res.data.status.message)
												this.setState(
													{ 
														userName : '',
														emailId : '',
														mobileNumber : '',
														userRole : '',
														userPhoto : '',
														userBirthDate : '',
														userAddress : '',
														hideDiv : false
									
													})
													
													window.location.reload(false);
											} else{
												toast.error(res.data.status.message)
												this.setState(
												{ 
													userName : '',
													emailId : '',
													mobileNumber : '',
													userRole : '',
													userPhoto : '',
													userBirthDate : '',
													userAddress : '',
												})
											} 
											}
											) ;
										}
									}else{
										this.refs.userAddress.focus();
										toast.error("User address is required.")
									  }
								}else{
									//this.refs.userBirthDate.focus();
									toast.error("Birth date is required.")
									}
							}else{
								this.refs.userPhoto.focus();
								toast.error("User photo is required.")
								}
						}else{
							this.refs.userRole.focus();
							toast.error("User role is required.")
							}
					}else{
						this.refs.mobileNumber.focus();
						toast.error("Mobile number is required.")
						}
				}else{
					this.refs.emailId.focus();
					toast.error("Email id is required.")
					}
			}else{
				this.refs.userName.focus();
				toast.error("Username is required.")
				}
		}

		HideDivFunction = () =>{
			const {hideDiv} = this.state;
			console.log(hideDiv);
			this.setState({
				hideDiv : !hideDiv
				
			}
			)
		
		}
	
		render(){
			const { userBirthDate } = this.state;
			return(
				<div>
					{this.state.hideDiv?
					<div className="col-xs-12">
						<div className="add-user">
							<div className="col-xs-12">
								<div className="box-content card white">
									{this.props.userId? <h4 className="box-title">Update User's details</h4> : <h4 className="box-title">Add User's Details</h4> }
									<form action="#" method="post">
										<div className="card-content">
											<div className="col-lg-6 col-md-6">
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-user"></i></label></div>
													<input id="ig-1" type="text" ref="userName" className="form-control" placeholder="User's Name" defaultValue={this.state.userName ||this.props.userName } onChange={(e)=>this.onChangeUserName(e)}  />
												</div>
												
											</div>
											<div className="col-lg-6 col-md-6">
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-2" className="btn btn-default"><i className="fa fa-envelope"></i></label></div>
												
													<input id="ig-2" type="email" ref="emailId"  className="form-control" placeholder="Email address" defaultValue={this.state.emailId ||this.props.emailId }  onChange={this.onChangeEmailId} />
												</div>
												
											</div>
											<div className="col-lg-6 col-md-6">
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-mobile"></i></label></div>
												
													<input id="ig-3" type="text" ref="mobileNumber" className="form-control" placeholder="Mobile Number" maxLength="10" defaultValue={this.state.mobileNumber ||this.props.mobileNumber }  onChange={this.onChangeMobileNumber} />
												</div>
												
											</div>
											<div className="col-lg-6 col-md-6">
												<div className="input-group margin-bottom-20">
												<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-user"></i></label></div>
													{/* <select id="ig-4" className="form-control" ref="userRole" defaultValue={this.state.userRole ||this.props.userRole }  onChange={this.onChangeUserRole} >
														<option>Select User Role</option>
														<option value="Master Admin">Master Admin</option>
														<option value="Faculty">Faculty</option>
														<option value="Senior Counsellor">Senior Counsellor</option>
														<option value="Telecaller">Telecaller</option>
													</select> */}
													<select id="ig-4" className="form-control" ref="userRole" defaultValue={this.state.userRole ||this.props.userRole } onChange={(e)=>this.onChangeUserRole(e)}>
														{this.state.roleList.map((courseData) => <option key={courseData.value} value={courseData.value} selected={courseData.Selected}>{courseData.display}</option>)}
													</select>
												</div>
												
											</div>
											<div className="col-lg-6 col-md-6">
												<div className="input-group margin-bottom-20">
													{/* <img src={this.state.userPhoto}/> */}
													<label>Upload Profile Picture</label>
													 <input id="ig-7" type="file"  name="imagename"  ref="userPhoto" onChange={this.onImageChange}/>  
													{/* // <br/>  */}
													{/* // <button input type="file" onChange={this.onImageChange} className="btn btn-primary btn-sm waves-effect waves-light">Upload</button> */}
													
													{this.props.userId ? <a  href={`${URL}${this.props.userPhoto}`}  target="_blank"><i class="ico fa fa-eye fa-lg"></i>View Photo</a> : null}
													
												</div>
											</div>
											<div className="col-lg-6 col-md-6">
												<label>Select Date of Birth</label>
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-8" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
													
													{/* <input id="ig-8" type="date" className=" datepicker2 form-control" ref="userBirthDate" placeholder="Date of birth" defaultValue={this.state.userBirthDate ||this.props.userBirthDate }  onChange={this.onChangeUserBirthDate} /> */}
													<DatePicker selected={userBirthDate} dateFormat="dd MMMM yyyy"   isClearable placeholderText="Date of birth" onChange={(date)=>this.onChangeUserBirthDate(date)} defaultValue={this.state.userBirthDate ||this.props.userBirthDate } className="form-control" style={{zIndex:1}}/>
												</div>
												
											</div>
											<div className="col-lg-12 col-md-12">
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-5" className="btn btn-default add-btn"><i className="fa fa-map"></i></label></div>
													
													<textarea id="ig-5" className="form-control" placeholder="Address"  ref="userAddress" defaultValue={this.state.userAddress ||this.props.userAddress }  onChange={this.onChangeUserAddress} ></textarea>
												</div>
											
											</div>
											{this.props.userPassword ? 
											<div className="col-lg-12 col-md-12">
												<div className="input-group margin-bottom-20">
												<div className="input-group-btn"><label htmlFor="ig-2" className="btn btn-default"><i className="fa fa-info"></i></label></div>
													<input id="ig-2" type="text" ref="password"  className="form-control" placeholder="password" defaultValue={this.state.userPassword ||this.props.userPassword }  onChange={this.onChangeUserPassword} />
												</div>
											</div> 
											: null}
											<div className="input-group">
												<div className="col-lg-6 col-md-6">
												{this.props.userId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
													
												</div>
												<div className="col-lg-6 col-md-6">
													<button id="hide-add-user" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>: null}
				</div>
			
			)
		}
	}

	

	export default ManageUser;