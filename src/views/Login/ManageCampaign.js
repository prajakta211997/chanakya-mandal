import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import {Button, Modal} from 'react-bootstrap/';
import '../../assets/plugin/bootstrap/css/bootstrap.min.css';
import axios from 'axios';
import {toast} from 'react-toastify';
import ReactPaginate from 'react-paginate';
import DatePicker from "react-datepicker";
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');

	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
	}
	
	class ManageCampaign extends React.Component {
		constructor(props){
			super(props)
			this.state = {
				offset: 0,
				perPage: 10,
				currentPage: 0,
				addcampaign :false,
				editcampaign :false,
				campaignList: [],
				campaignId : '',
				campaignName : '',
				selectcourse : '',
				campaignType : '',
				campaignDuration : '',
				startDate : '',
				endDate : ''
				
			}
			this.logout = this.logout.bind(this);
			this.handlePageClick = this.handlePageClick.bind(this);
		}
		ShowDivfunction = () =>{
			const {addcampaign} = this.state;
			this.setState({
				addcampaign : !addcampaign
			}
			)
		}
		
		ShowEditDivFunction = (campaignData) =>{
			const {editcampaign} = this.state;
			this.setState({
				editcampaign : !editcampaign,
				campaignId : campaignData.campaign_id,
				campaignName : campaignData.campaign_name,
				selectcourse : campaignData.select_course,
				campaignType : campaignData.campaign_type,
				campaignDuration : campaignData.campaign_duration,
				startDate : campaignData.start_date,
				endDate :campaignData.end_date,
				subjectName: campaignData.subject_name,
				campaignFees : campaignData.campaign_fees,
				campaignDescription : campaignData.campaign_description
			}
			)
			console.log(campaignData)
		}

		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};

		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}

		getDetails() {
			const apiUrl = apiConfig.API_Path +'ManageCampaign/getDetails';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	campaignList: result
					// });
					const campaignList = result;
					const slice = campaignList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(campaignList.length / this.state.perPage),
						campaignList : slice
					})
				}
				
			  )
			  
			  
		  }
		  
		  getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				const apiUrl = apiConfig.API_Path +'ManageCampaign/getAccessPermissions';
				const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_role'));
				formData.append('ManageUser', "Manage Campaign");
				const options = {
					method: 'POST',
					body: formData
				}
				fetch(apiUrl,options)
				.then(res => res.json() )
				.then(
					(result) => {
						
						localStorage.setItem("editPermission",result[0]['edit_permission']);
						localStorage.setItem('deletePermission',result[0]['delete_permission']);
						localStorage.setItem('viewPermission',result[0]['view_permission']);
						console.log(result)
					}
					
				)
			}
			
		}
		componentDidMount() {
			
		$(".commonRemove").removeClass('current');
		$("#ManageCampaign").addClass('current');
		this.getDetails();
		this.getAccessPermissions();
		}
		  deletecampaign(campaignId) {
			const { campaigns } = this.state;
		
			const apiUrl = apiConfig.API_Path +'ManageCampaign/deleteDetails';
			const formData = new FormData();
			formData.append('campaignId', campaignId);
		
			const options = {
			  method: 'POST',
			  body: formData
			}
		
			fetch(apiConfig.API_Path +'ManageCampaign/deleteDetails', options)
			
			.then(res => res.json())
			.then(
				(result) => {
					toast.success(result.message)
					this.getDetails();
				},
				(error) => {
					toast.error("Opps! Something went Wrong.")
				}
			)
			}
		
			
			onChangeSearch(e) {
				//const searchData = e.target.value
				const apiUrl = apiConfig.API_Path +'ManageCampaign/onChangeSearch';
				const formData = new FormData();
				formData.append('searchData', e.target.value);
			
				const options = {
				  method: 'POST',
				  body: formData
				}
				fetch(apiConfig.API_Path +'ManageCampaign/onChangeSearch',options)
				  .then(res => res.json() )
				  .then(
					(result) => {
						this.setState({
							campaignList: result
						});
					}
					// ,
					// (error) => {
					//   this.setState({ error });
					// }
				  )
				  
				  
			  }
		
		render() {
			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				const { campaignList} = this.state;
				return (
						<div>
							<header id="header">
								<div className="main-menu">
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<Navigation />
									</div>
								</div>
				
			
								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Manage campaign</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
					<div id="wrapper">
						<div className="main-content">
			
						<div className="row small-spacing">
							{this.state.addcampaign? <ShowDiv /> :null}
			
							{this.state.editcampaign? <ShowDiv 
								campaignId = {this.state.campaignId} 
								campaignName = {this.state.campaignName}
								startDate = {this.state.startDate}
								endDate = {this.state.endDate}
								selectcourse = {this.state.selectcourse}
								
								
							/> :null}
						</div>
						<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">campaign List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-campaign-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add campaign</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch"placeholder="Search by campaign name" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
										<table id="campaign-list" className="table table-striped table-bordered display" >
										<thead>
												<tr>
													<th>Sr. No.</th>
													<th>Campaign Name</th>
													<th>Course Name</th>
													<th>Start Date</th>
													<th>End Date</th>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? <th>Action</th> : null}
												</tr>
											</thead>
											{/* <tfoot>
												<tr>
													<th>Sr. No.</th>
													<th>campaign Name</th>
													<th>Action</th>
												</tr>
											</tfoot> */}
											{localStorage.getItem('viewPermission') === 'true' ?
											<tbody>
											{campaignList.map((campaignData,index) => (
												<tr key={campaignData.campaign_id}>
												<React.Fragment> 
													<td>{index+1}</td>
													<td>{campaignData.campaign_name}</td>
													<td>{campaignData.select_course}</td>
													<td>{campaignData.start_date}</td>
													<td>{campaignData.end_date}</td>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? 
													<td>
														{localStorage.getItem('editPermission') === 'true' ?
														<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light"  title="Edit campaign Details" onClick={() => this.ShowEditDivFunction(campaignData)}>
															<i className="ico fa fa-edit"></i>
														</button> : null}
														{localStorage.getItem('deletePermission') === 'true' ?
														<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete campaign" onClick={() => this.deletecampaign(campaignData.campaign_id)}>
															<i className="ico fa fa-trash"></i>
														</button>
														: null }
													</td>
													: null}
													</React.Fragment> 
												</tr>
											))}
												
											</tbody>
											:null}
										</table>
										<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
									</div>
								</div>
								
							</div>
										
								<footer id="footer" className="footer">
									<ul className="list-inline">
										<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
										<li><a href="#">Privacy</a></li>
										<li><a href="#">Coockies</a></li>
										<li><a href="#">Legal Notice</a></li>
									</ul>
								</footer>
							</div>
						</div>
					</div>
				);
			}
		}
		  }
	
		class ShowDiv extends Component{
			constructor(props){
				super(props)
				this.state = {
					hideDiv :true,
					campaignName : '',
					startDate : '',
					endDate : '',
					//endDate : new Date(),
					courseList : [],
					selectcourse : '',
					//startDate: new Date()
					
				}
				this.getCourseDeatils();
				this.onChangecampaignName = this.onChangecampaignName.bind(this);
				this.onChangeselectcourse = this.onChangeselectcourse.bind(this);
				this.onChangestartDate = this.onChangestartDate.bind(this);
				this.onChangeendDate = this.onChangeendDate.bind(this);
				this.onSubmit = this.onSubmit.bind(this);
				
			}
			onChangecampaignName(e) {
				this.setState({
					campaignName :  e.target.value
				});
			}
			
			onChangestartDate = startDate  => {
				this.setState({
					startDate :  startDate
				});
			}
			
			onChangeendDate = endDate  => {
				this.setState({
					endDate :  endDate
				});
			}
			
			onChangeselectcourse(e) {
				this.setState({
					selectcourse :  e.target.value
				});
			}

			getCourseDeatils(){
				const apiUrl1 = apiConfig.API_Path +'ManageLead/getCourseDeatils';
			
				fetch(apiUrl1)
					.then(res => res.json() )
					.then(response => {
					let courseList = response.map(courseData => {
						return {value: courseData.course_name, display: courseData.course_name, Selected : false}
					}); 
					
						const tempArray = [];
						courseList.forEach(element => {
							
							if(element.value === this.props.selectcourse){
								tempArray.push({value: element.value, display: element.display,Selected: true})
							}else{
								tempArray.push({value: element.value, display: element.display,Selected: false})
							}
						});
						
						this.setState({
							courseList: [{value: '', display: 'Select Course',Selected: true}].concat(tempArray)
							});
					}
					
					)
					
				}
			
			
			onSubmit(e){
			e.preventDefault();
			
			if(this.state.campaignName  || this.props.campaignName){
				if(this.state.campaignName){
					if (!(this.state.campaignName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.campaignName.focus();
						toast.error("Valid campaign name is required.")
						return false;
					}
				}
				if(this.props.campaignName){
					if (!(this.props.campaignName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.campaignName.focus();
						toast.error("Valid campaign name is required.")
						return false;
					}
				}
					if(this.state.selectcourse || this.props.selectcourse){
						if(this.state.startDate  || this.props.startDate){
							if(this.state.endDate  || this.props.endDate){
								if(this.state.startDate){
									var startDate = this.state.startDate;
								}
								if(this.props.startDate){
									var startDate = this.props.startDate;
								}
								if(this.state.endDate){
									var endDate = this.state.endDate;
								}
								if(this.props.endDate){
									var endDate = this.props.endDate;
								}
								if(Date.parse(endDate) <= Date.parse(startDate)){
		
									toast.error("Please select valid Start And End Date.");
									///this.refs.startDate.focus();
									return false;
		
								}
								
				
									const formData = new FormData();
									//coureName
									if(this.state.campaignName){
										formData.append("campaignName", this.state.campaignName);
									}else{
										formData.append("campaignName", this.props.campaignName);
									}
		
									
									//startDate
									var startnow = this.state.startDate
									var startdate = startnow.toLocaleDateString("en-GB", {year: "numeric",month: "long",day: "2-digit" });
									if(this.state.startDate){
										formData.append("startDate", startdate);
									}else{
										formData.append("startDate", this.props.startDate);
									}
		
									//endDate
									var endnow = this.state.endDate
									var enddate = endnow.toLocaleDateString("en-GB", {year: "numeric",month: "long",day: "2-digit" });
									if(this.state.endDate){
										formData.append("endDate", enddate);
									}else{
										formData.append("endDate", this.props.endDate);
									}

									if(this.state.selectcourse){
										formData.append("selectcourse",this.state.selectcourse);
									}else{
										formData.append("selectcourse",this.props.selectcourse);
									}
									
									formData.append("campaignId", this.props.campaignId);
									
									if(this.props.campaignId){
										axios.post(apiConfig.API_Path +'ManageCampaign/updateDetails',formData,{
											// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
											headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
										})
										.then(res => 
											{
											if(res.data.status.status === '1'){
												toast.success(res.data.status.message)
												this.setState(
													{ 
														campaignId : '',
														campaignName : '',
														selectcourse : '',
														startDate : '',
														endDate : '',
														hideDiv : false
									
													})
													
													window.location.reload(false);
											} else{
												toast.error(res.data.status.message)
												this.setState(
												{ 
													campaignName : '',
													selectcourse : '',
													startDate : '',
													endDate : '',
													campaignId : '',
												})
											} 
											}
											);
									}else{
										
										
									axios.post(apiConfig.API_Path +'ManageCampaign/insertDetails',formData,{
										// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
										headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
										})
										.then(res => 
											{
											if(res.data.status.status === '1'){
												toast.success(res.data.status.message)
												this.setState(
													{ 
														campaignName : '',
														selectcourse : '',
														startDate : '',
														endDate : '',
														hideDiv : false
									
													})
													
													window.location.reload(false);
											} else{
												toast.error(res.data.status.message)
												this.setState(
												{ 
													campaignName : '',
													selectcourse : '',
													startDate : '',
													endDate : '',
												})
											} 
											}
											) ;
										}
							}else{
								this.refs.endDate.focus();
								toast.error("End date is required.")
								}	
						}else{
							this.refs.startDate.focus();
							toast.error("Start date is required.")
							}	
					}else{
						this.refs.selectcourse.focus();
						toast.error("Select course is required.")
						}
			}else{
				this.refs.campaignName.focus();
				toast.error("Campaign name is required.")
				}
		}
	
			HideDivFunction = () =>{
				const {hideDiv} = this.state;
				console.log(hideDiv);
				this.setState({
					hideDiv : !hideDiv
					
				}
				)
			
			}
			
			render(){
				const { startDate } = this.state;
				const { endDate } = this.state;
				return(
					<div>
						{this.state.hideDiv?
						<div className="col-xs-12">
							<div className="add-campaign">
								<div className="col-xs-12">
									<div className="box-content card white">
										{this.props.campaignId? <h4 className="box-title">Update campaign </h4> : <h4 className="box-title">Add campaign </h4> }
										<form action="#" method="post">
											<div className="card-content">
												<div className="col-lg-6 col-md-6">
												{/* <label>Campaign Name</label> */}
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-book"></i></label></div>
														<input id="ig-1" type="text"ref="campaignName" className="form-control" placeholder="campaign Name" defaultValue={this.state.campaignName ||this.props.campaignName } onChange={(e)=>this.onChangecampaignName(e)}/>
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-user"></i></label></div>
														<select id="ig-4" className="form-control" ref="selectcourse" defaultValue={this.state.selectcourse ||this.props.selectcourse } onChange={(e)=>this.onChangeselectcourse(e)}>
															{this.state.courseList.map((courseData) => <option key={courseData.value} value={courseData.value} selected={courseData.Selected}>{courseData.display}</option>)}
														</select>
													</div>
												</div>
												<div className="col-lg-3 col-md-6">
													{/* <label>Start Date</label> */}
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-8" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
														<DatePicker selected={startDate} dateFormat="dd MMMM yyyy"   isClearable placeholderText="Start Date" onChange={(date)=>this.onChangestartDate(date)} className="form-control" />
														{/* <input type="date" className="form-control" ref="startDate" placeholder="mm/dd/yyyy" id="startDate" defaultValue={this.state.startDate ||this.props.startDate } onChange={(e)=>this.onChangestartDate(e)}/> */}
													</div>
												</div>
												<div className="col-lg-3 col-md-6">
													{/* <label>End Date</label> */}
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-9" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
														<DatePicker selected={endDate} dateFormat="dd MMMM yyyy"   isClearable placeholderText="End Date" onChange={(date)=>this.onChangeendDate(date)} className="form-control" />
														{/* <input type="date" className="form-control"  ref="endDate" placeholder="mm/dd/yyyy" id="endDate" defaultValue={this.state.endDate ||this.props.endDate } onChange={(e)=>this.onChangeendDate(e)}/> */}
													</div>
												</div>
												<div className="col-lg-6 col-md-6"></div>
											
												
												<div className="input-group">
											
													<div className="col-lg-6 col-md-6">
													{this.props.roleId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
														
													</div>
													<div className="col-lg-6 col-md-6">
														<button id="hide-add-role" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>: null}
					</div>
				
				)
			}
		}
	
		
	
	


	export default ManageCampaign;