import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import {toast} from 'react-toastify';
import ReactPaginate from 'react-paginate';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');

	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
    }
	class ManageSource extends React.Component {
		constructor(props){
			super(props)
			this.state = {
				offset: 0,
				perPage: 10,
				currentPage: 0,
				addSource :false,
				editSource :false,
				SourceList: [],
				sourceId : '',
				sourceName : '',
			}
			this.logout = this.logout.bind(this);
			this.handlePageClick = this.handlePageClick.bind(this);
		}
		ShowDivfunction = () =>{
			const {addSource} = this.state;
			this.setState({
				addSource : !addSource
			}
			)
		}
		
		ShowEditDivFunction = (SourceData) =>{
			const {editSource} = this.state;
			this.setState({
				editSource : !editSource,
				sourceId : SourceData.source_id,
				sourceName : SourceData.source_name,
				
			}
			)
		}

		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};

		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}

		getDetails() {
			const apiUrl = apiConfig.API_Path +'ManageSource/getDetails';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	SourceList: result
					// });
					const SourceList = result;
					const slice = SourceList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(SourceList.length / this.state.perPage),
						SourceList : slice
					})
				}
				
			  )
			  
			  
		  }
		  
		getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				const apiUrl = apiConfig.API_Path +'ManageSource/getAccessPermissions';
				const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_role'));
				formData.append('ManageUser', "Manage Source");
				const options = {
					method: 'POST',
					body: formData
				}
				fetch(apiUrl,options)
				.then(res => res.json() )
				.then(
					(result) => {
						
						localStorage.setItem("editPermission",result[0]['edit_permission']);
						localStorage.setItem('deletePermission',result[0]['delete_permission']);
						localStorage.setItem('viewPermission',result[0]['view_permission']);
						console.log(result)
					}
					
				)
			}
		}
		componentDidMount() {
		$(".commonRemove").removeClass('current');
		$("#ManageSource").addClass('current');
		this.getDetails();
		this.getAccessPermissions();
		}

		deleteSource(sourceId) {
		
		const formData = new FormData();
		formData.append('sourceId', sourceId);
	
		const options = {
			method: 'POST',
			body: formData
		}
	
		fetch(apiConfig.API_Path +'ManageSource/deleteSource', options)
		
		.then(res => res.json())
		.then(
			(result) => {
				toast.success(result.message)
				this.getDetails();
			},
			(error) => {
				toast.error("Opps! Something went Wrong.")
			}
		)
		}
		
			
		onChangeSearch(e) {
			const formData = new FormData();
			formData.append('searchData', e.target.value);
		
			const options = {
				method: 'POST',
				body: formData
			}
			fetch(apiConfig.API_Path +'ManageSource/onChangeSearch',options)
				.then(res => res.json() )
				.then(
				(result) => {
					this.setState({
						SourceList: result
					});
				}
				// ,
				// (error) => {
				//   this.setState({ error });
				// }
				)
				
				
			}
		
		render() {
			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				const { SourceList} = this.state;
				return (
						<div>
							<header id="header">
								<div className="main-menu">
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<Navigation />
									</div>
								</div>
				
			
								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Manage Source</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
					<div id="wrapper">
						<div className="main-content">
			
						<div className="row small-spacing">
							{this.state.addSource? <ShowDiv /> :null}
			
							{this.state.editSource? <ShowDiv 
								sourceId = {this.state.sourceId} 
								sourceName = {this.state.sourceName}
								
							/> :null}
						</div>
						<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">Source List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-Source-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add Source</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch"placeholder="Search by Source name" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
										<table id="Source-list" className="table table-striped table-bordered display" >
										<thead>
												<tr>
													<th>Sr. No.</th>
													<th>Source Name</th>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? <th>Action</th> : null}
												</tr>
											</thead>
											{/* <tfoot>
												<tr>
													<th>Sr. No.</th>
													<th>Source Name</th>
													<th>Action</th>
												</tr>
											</tfoot> */}
											{localStorage.getItem('viewPermission') === 'true' ?
											<tbody>
											{SourceList.map((Sourcedata,index) => (
												<tr key={Sourcedata.source_id}>
													<React.Fragment> 
													<td>{index+1}</td>
													<td>{Sourcedata.source_name}</td>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ?  
													<td>
														{localStorage.getItem('editPermission') === 'true' ?
														<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light"  title="Edit Source Details" onClick={() => this.ShowEditDivFunction(Sourcedata)}>
															<i className="ico fa fa-edit"></i>
														</button> : null }
														{localStorage.getItem('deletePermission') === 'true' ?
														<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete Source" onClick={() => this.deleteSource(Sourcedata.source_id)}>
															<i className="ico fa fa-trash"></i>
														</button> : null}
													</td>
												:null }
												</React.Fragment> 
												</tr>
											))}
												
											</tbody>
											:null}
										</table>
										<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
									</div>
								</div>
								
							</div>
										
								<footer id="footer" className="footer">
									<ul className="list-inline">
										<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
										<li><a href="#">Privacy</a></li>
										<li><a href="#">Coockies</a></li>
										<li><a href="#">Legal Notice</a></li>
									</ul>
								</footer>
							</div>
						</div>
					</div>
				);
			}
		}
		  }
	
		class ShowDiv extends Component{
			constructor(props){
				super(props)
				this.state = {
					hideDiv :true,
					sourceName : '',
					
				}
				
				this.onChangesourceName = this.onChangesourceName.bind(this);
				this.onSubmit = this.onSubmit.bind(this);
				
			}
			onChangesourceName(e) {
				this.setState({
					sourceName :  e.target.value
				});
			}
			
			onSubmit(e){
			e.preventDefault();
			
			if(this.state.sourceName  || this.props.sourceName){
				if(this.state.sourceName){
					if (!(this.state.sourceName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.sourceName.focus();
						toast.error("Valid Source name is required.")
						return false;
					}
				}
				if(this.props.sourceName){
					if (!(this.props.sourceName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.sourceName.focus();
						toast.error("Valid Source name is required.")
						return false;
					}
				}
				
				const formData = new FormData();
				
				if(this.state.sourceName){
					formData.append("sourceName", this.state.sourceName);
				}else{
					formData.append("sourceName", this.props.sourceName);
				}
				
				formData.append("sourceId", this.props.sourceId);
				
				if(this.props.sourceId){
					axios.post(apiConfig.API_Path +'ManageSource/updateDetails',formData,{
						// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
						headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
					})
					.then(res => 
						{
						if(res.data.status.status === '1'){
							toast.success(res.data.status.message)
							this.setState(
								{ 
									sourceId : '',
									sourceName : '',
									hideDiv : false
				
								})
								
								window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								sourceName : '',
								sourceId : '',
							})
						} 
						}
						);
				}else{
					
					
				axios.post(apiConfig.API_Path +'ManageSource/insertDetails',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
					})
					.then(res => 
						{
						if(res.data.status.status === '1'){
							toast.success(res.data.status.message)
							this.setState(
								{ 
									sourceName : '',
									hideDiv : false
				
								})
								
								window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								sourceName : '',
								
							})
						} 
						}
						) ;
					}
										
				}else{
					this.refs.sourceName.focus();
					toast.error("Source name is required.")
					}
			}
	
			HideDivFunction = () =>{
				const {hideDiv} = this.state;
				console.log(hideDiv);
				this.setState({
					hideDiv : !hideDiv
					
				}
				)
			
			}
		
			render(){
				return(
					<div>
						{this.state.hideDiv?
						<div className="col-xs-12">
							<div className="add-Source">
								<div className="col-xs-12">
									<div className="box-content card white">
										{this.props.sourceId? <h4 className="box-title">Update Source </h4> : <h4 className="box-title">Add Source </h4> }
										<form action="#" method="post">
											<div className="card-content">
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-user"></i></label></div>
														<input id="ig-1" type="text" ref="sourceName" className="form-control" placeholder="Source Name" defaultValue={this.state.sourceName ||this.props.sourceName } onChange={(e)=>this.onChangesourceName(e)}  />
													</div>
													
												</div>
												
												
												<div className="input-group">
													<div className="col-lg-6 col-md-6">
													{this.props.sourceId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
														
													</div>
													<div className="col-lg-6 col-md-6">
														<button id="hide-add-Source" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
													</div>
												</div>
												
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>: null}
					</div>
				
				)
			}
		}
	
		
	
	


	export default ManageSource;