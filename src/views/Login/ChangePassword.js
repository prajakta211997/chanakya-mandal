import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import {Button, Modal} from 'react-bootstrap/';
import '../../assets/plugin/bootstrap/css/bootstrap.min.css';
import axios from 'axios';
import {toast} from 'react-toastify';
import ReactPaginate from 'react-paginate';
const apiConfig = require('../../api_config/apiConfig.json');

	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
    }
	class ChangePassword extends React.Component {
		constructor(props){
			super(props)
			
            this.state = {
                confirmedpassword : '',
                oldpassword : '',
                newpassword : ''
            }
			this.onChangeConfirmedpassword = this.onChangeConfirmedpassword.bind(this);
            this.onChangePassword = this.onChangePassword.bind(this);
            this.onChangeNewPassword = this.onChangeNewPassword.bind(this);
            this.onSubmit = this.onSubmit.bind(this);
		    this.logout = this.logout.bind(this);
           
		}
        
        onChangeConfirmedpassword(e) {
            this.setState({
                confirmedpassword :  e.target.value
            });
        }

        onChangePassword(e) {
            this.setState({
                oldpassword :  e.target.value
            });
        }
        onChangeNewPassword(e) {
            this.setState({
                newpassword :  e.target.value
            });
        }
        
        logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}

        onSubmit(e){
            e.preventDefault();
            if(this.state.oldpassword){
                if(this.state.newpassword ){
                    if(this.state.confirmedpassword ){
                        if(this.state.newpassword  !== this.state.confirmedpassword){
                            this.refs.newpassword.focus();
                            toast.error("password does not match.")
                            return false;
                        }
                        const formData = new FormData();
                        formData.append("oldpassword", this.state.oldpassword);
                        formData.append("newpassword", this.state.newpassword);
                        formData.append("userId", localStorage.getItem('user_id'));
                        axios.post(apiConfig.API_Path +'Login/ChangePassword',formData,{
											// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
											headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
										})
										.then(res => 
											{
											if(res.data.status.status === '1'){
												toast.success(res.data.status.message)
												this.setState(
													{ 
                                                        oldpassword : '',
                                                        newpassword : '',
                                                        confirmedpassword : ''
									
													})
													
                                                    window.location.reload(false);
                                                }
                                            });
                    }else{
                        this.refs.confirmedpassword.focus();
                        toast.error("Confirmed password is required.")
                    }
                }else{
                    this.refs.newpassword.focus();
                    toast.error("New password is required.")
                }
            }else{
                this.refs.oldpassword.focus();
                toast.error("Old password is required.")
            }
        }
		render() {
			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				
				return (
						<div>
							<header id="header">
								<div className="main-menu">
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<div className="navigation">
											<h5 className="title">Navigation</h5>
												<ul className="menu js__accordion">
													{localStorage.getItem("Dashboard")==="True" ?
														<li  id="AdminDashboard">
															<a className="waves-effect" href="AdminDashboard"><i className="menu-icon fa fadashboard"></i><span>Dashboard</span></a>
														</li> : null
													}
													{localStorage.getItem("Manage User")==="True" ?
														<li  id="ManageUser" >
															<a className="waves-effect" href="ManageUser"><i className="menu-icon fa fa-clone"></i><span>Manage Users</span></a>
														</li> : null
													}		
													{localStorage.getItem("Manage Role")==="True" ?									
														<li  id="ManageRole">
															<a className="waves-effect" href="ManageRole"><i className="menu-icon fa fa-clone"></i><span>Manage Role</span></a>
														</li> : null
													}
													{localStorage.getItem("Manage Leads")==="True" ?
														<li  id="ManageLead">
															<a className="waves-effect" href="ManageLead"><i className="menu-icon fa fa-clone"></i><span>Manage Leads</span></a>
														</li> : null
													}
													{localStorage.getItem("Manage Source")==="True" ?
														<li  id="ManageSource">
															<a className="waves-effect" href="ManageSource"><i className="menu-icon fa fa-clone"></i><span>Manage Source</span></a>
														</li> : null
													}
													{localStorage.getItem("Manage Course")==="True" ?
														<li id="ManageCourse">
															<a className="waves-effect" href="ManageCourse"><i className="menu-icon fa fa-clone"></i><span>Manage Course</span></a>
														</li> : null 
													}
													{localStorage.getItem("Manage Campaign")==="True" ?
														<li id="ManageCampaign" >
															<a className="waves-effect" href="ManageCampaign"><i className="menu-icon fa fa-clone"></i><span>Manage Campaign</span></a>
														</li>: null 
													}
													{localStorage.getItem("Manage Admission")==="True" ?
														<li id="ManageAdmission">
															<a className="waves-effect" href="ManageAdmission"><i className="menu-icon fa fa-clone"></i><span>Manage Admissions</span></a>
														</li>: null
													}
													{localStorage.getItem("Manage Access")==="True" ?
														<li id="ManageAccess">
															<a className="waves-effect" href="ManageAccess"><i className="menu-icon fa fa-clone"></i><span>Manage Access</span></a>
														</li> : null
													}
													{localStorage.getItem("Manage Leaves")==="True" ?						
														<li id="ManageLeaves">
															<a className="waves-effect" href="ManageLeaves"><i className="menu-icon fa fa-clone"></i><span>Manage Leaves</span></a>
														</li>: null
													}
			
												</ul>
			
										</div>
									</div>
								</div>
				
			
								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Change Password</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a  href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
					<div id="wrapper">
						<div className="main-content">
			                <div className="col-xs-12">
                                <div className="box-content bordered primary js__card">
                                    <div id="single-wrapper">
                                        <form action="#" className="frm-single">
                                            <div className="inside">
                                            <div className="title">
                                                <img src={logo} alt="Chanakya Mandal" title="Chanakya Mandal User Login" style={IconSize} />
                                            </div>
                                            <div className="frm-title">Change Password</div>
                                            
                                            <div className="frm-input"><input type="password" ref="oldpassword" placeholder="Old Password" className="frm-inp" value={this.state.oldpassword} onChange={this.onChangePassword} /><i className="fa fa-lock frm-ico"></i></div>
                                            <div className="frm-input"><input type="password" ref="newpassword" placeholder=" New Password" className="frm-inp" value={this.state.newpassword} onChange={this.onChangeNewPassword} /><i className="fa fa-lock frm-ico"></i></div>
                                            <div className="frm-input"><input type="password" ref="confirmedpassword" placeholder=" Confirmed Password" className="frm-inp" value={this.state.confirmedpassword} onChange={this.onChangeConfirmedpassword} /><i className="fa fa-lock frm-ico"></i></div>
                                            
                                            
                                            
                                        <input type="submit" value="Change Password" className="frm-submit"  onClick={this.onSubmit}/>
                                        
                                            
                                            <div className="frm-footer">© Chanakya Mandal Pariwar <script type="text/JavaScript"> document.write(new Date().getFullYear()); </script>.</div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
										
                            <footer id="footer" className="footer">
                                <ul className="list-inline">
                                    <li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Coockies</a></li>
                                    <li><a href="#">Legal Notice</a></li>
                                </ul>
                            </footer>
						</div>
					</div>
				</div>
				);
			}
		}
	}
	
		

	export default ChangePassword;