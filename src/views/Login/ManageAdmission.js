import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import {toast} from 'react-toastify';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');


	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
    }

	class ManageAdmission extends React.Component {
		constructor(props){
			super(props)
			this.state = {
				offset: 0,
				perPage: 10,
				currentPage: 0,
				addadmission :false,
				editadmission :false,
				admissionList: [],
				courseList : [],
				admissionId : '',
				studentName : '',
				emailId : '',
				mobileNumber : '',
				alterMobileNumber : '',
				selectcourse : '',
				admissionStatus : '',
			}
			this.logout = this.logout.bind(this);
			this.handlePageClick = this.handlePageClick.bind(this);
		}
		ShowDivfunction = () =>{
			const {addadmission} = this.state;
			this.setState({
				addadmission : !addadmission
			}
			)
		}
		
		ShowEditDivFunction = (admissionData) =>{
			const {editadmission} = this.state;
			this.setState({
				editadmission : !editadmission,
				admissionId : admissionData.admission_id,
				studentName : admissionData.student_name,
				emailId : admissionData.email_id,
				mobileNumber : admissionData.mobile_number,
				alterMobileNumber : admissionData.alter_mobile_number,
				selectcourse :admissionData.selected_course,
				admissionStatus: admissionData.admission_status,
				
			}
			)
			
		}
		getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				const apiUrl = apiConfig.API_Path +'ManageAdmission/getAccessPermissions';
				const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_role'));
				formData.append('ManageUser', "Manage Admission");
				const options = {
					method: 'POST',
					body: formData
				}
				fetch(apiUrl,options)
				.then(res => res.json() )
				.then(
					(result) => {
						
						localStorage.setItem("editPermission",result[0]['edit_permission']);
						localStorage.setItem('deletePermission',result[0]['delete_permission']);
						localStorage.setItem('viewPermission',result[0]['view_permission']);
						console.log(result)
					}
					
				)
			}
			
		}
		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};
		componentDidMount() {
			$(".commonRemove").removeClass('current');
			$("#ManageAdmission").addClass('current');
			this.getDetails();
			this.getCourseDeatils();
			this.getAccessPermissions();
		}

		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}

		getDetails() {
			const apiUrl = apiConfig.API_Path +'ManageAdmission/getDetails';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	admissionList: result
					// });
					const admissionList = result;
					const slice = admissionList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(admissionList.length / this.state.perPage),
						admissionList : slice
					})
				}
				
			  )
			  
			  
		  }

		  getCourseDeatils(){
			const apiUrl = apiConfig.API_Path +'ManageAdmission/getCourseDeatils';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(
				(result) => {
					this.setState({
						courseList: result
					});
				}
				
			  )
			  
		  }
		  
		  
		  deleteadmission(admissionId) {
			const formData = new FormData();
			formData.append('admissionId', admissionId);
		
			const options = {
			  method: 'POST',
			  body: formData
			}
		
			fetch(apiConfig.API_Path +'ManageAdmission/deleteDetails', options)
			
			.then(res => res.json())
			.then(
				(result) => {
					toast.success(result.message)
					this.getDetails();
				},
				(error) => {
					toast.error("Opps! Something went Wrong.")
				}
			)
			}
		
			
			onChangeSearch(e) {
				const formData = new FormData();
				formData.append('searchData', e.target.value);
			
				const options = {
				  method: 'POST',
				  body: formData
				}
				fetch(apiConfig.API_Path +'ManageAdmission/onChangeSearch',options)
				  .then(res => res.json() )
				  .then(
					(result) => {
						this.setState({
							admissionList: result
						});
					}
					// ,
					// (error) => {
					//   this.setState({ error });
					// }
				  )
				  
				  
			  }
		
		render() {
			if(localStorage.getItem("userLoggedIn") ==="False" || localStorage.getItem("userLoggedIn") ===null){
				window.location = "login";
			}else{
				const { admissionList} = this.state;
				return (
						<div>
							<header id="header">
								<div className="main-menu">
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<Navigation />
									</div>
								</div>
				
			
								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Manage Admissions</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
					<div id="wrapper">
						<div className="main-content">
			
						<div className="row small-spacing">
							{this.state.addadmission? <ShowDiv /> :null}
			
							{this.state.editadmission? <ShowDiv 
								admissionId = {this.state.admissionId} 
								studentName = {this.state.studentName}
								emailId = {this.state.emailId}
								mobileNumber  = {this.state.mobileNumber}
								alterMobileNumber = {this.state.alterMobileNumber}
								selectcourse = {this.state.selectcourse}
								admissionStatus = {this.state.admissionStatus}
								
								
							/> :null}
						</div>
						<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">Admissions List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-admission-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add Admission</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch"placeholder="Search by Student name, course name, admission status" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
										<table id="admission-list" className="table table-striped table-bordered display" >
										<thead>
												<tr>
													<th>Sr. No.</th>
													<th>student's Name</th>
													<th>Email ID</th>
													<th>Mobile Number</th>
													<th>Alter Mobile Number</th>
													<th>Selected course</th>
													<th>Admission Status</th>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? <th>Action</th> : null}
												</tr>
											</thead>
											{/* <tfoot>
												<tr>
													<th>Sr. No.</th>
													<th>admission Name</th>
													<th>Action</th>
												</tr>
											</tfoot> */}
											{localStorage.getItem('viewPermission') === 'true' ?
											<tbody>
											{admissionList.map((admissionData,index) => (
												<tr key={admissionData.admission_id}>
													<React.Fragment> 
													<td>{index+1}</td>
													<td>{admissionData.student_name}</td>
													<td>{admissionData.email_id}</td>
													<td>{admissionData.mobile_number}</td>
													<td>{admissionData.alter_mobile_number}</td>
													<td>{admissionData.course_name}</td>
													<td>{admissionData.admission_status}</td>
													
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ?  
													<td>
														{localStorage.getItem('editPermission') === 'true' ?
														<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light"  title="Edit admission Details" onClick={() => this.ShowEditDivFunction(admissionData)}>
															<i className="ico fa fa-edit"></i>
														</button> : null}
														{localStorage.getItem('deletePermission') === 'true' ?
														<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete admission" onClick={() => this.deleteadmission(admissionData.admission_id)}>
															<i className="ico fa fa-trash"></i>
														</button> : null }

													</td>
													: null}
													</React.Fragment> 
												</tr>
											))}
												
											</tbody>
											:null}
										</table>
										<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
									</div>
								</div>
								
							</div>
										
								<footer id="footer" className="footer">
									<ul className="list-inline">
										<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
										<li><a href="#">Privacy</a></li>
										<li><a href="#">Coockies</a></li>
										<li><a href="#">Legal Notice</a></li>
									</ul>
								</footer>
							</div>
						</div>
					</div>
				);
			}
		}
	}
	
		class ShowDiv extends Component{
			constructor(props){
				super(props)
				this.state = {
					hideDiv :true,
					courseList : [],
					studentName : '',
					emailId : '',
					mobileNumber : '',
					alterMobileNumber : '',
					selectcourse : '',
					admissionStatus : '',
					admissionExcel : null,
				}
				this.getCourseDeatils();
				this.uploadExcel = this.uploadExcel.bind(this);
				this.onChangestudentName = this.onChangestudentName.bind(this);
				this.onChangeemailId = this.onChangeemailId.bind(this);
				this.onChangemobileNumber = this.onChangemobileNumber.bind(this);
				this.onChangealterMobileNumber = this.onChangealterMobileNumber.bind(this);
				this.onChangeselectcourse = this.onChangeselectcourse.bind(this);
				this.onChangeadmissionStatus = this.onChangeadmissionStatus.bind(this);
				this.onSubmit = this.onSubmit.bind(this);
				this.onUploadExcel = this.onUploadExcel.bind(this);
				
			}
			getCourseDeatils(){
				const apiUrl = apiConfig.API_Path +'ManageAdmission/getCourseDeatils';
			
				fetch(apiUrl)
				  .then(res => res.json() )
				  .then(response => {
					let coursedetails = response.map(courseData => {
					  return {value: courseData.course_id, display: courseData.course_name, Selected : false}
					}); 
					
						// this.setState({
						// courseList: [{value: '', display: 'Select Course',Selected: true}].concat(coursedetails)
						// });
						const tempArray = [];
						coursedetails.forEach(element => {
							
							if(element.value ===this.props.selectcourse){
								tempArray.push({value: element.value, display: element.display,Selected: true})
							}else{
								tempArray.push({value: element.value, display: element.display,Selected: false})
							}
						});
						
						this.setState({
							courseList: [{value: '', display: 'Select Course',Selected: true}].concat(tempArray)
							});
						// this.setState({
						// 	courseList: response
						// 	});
						
					}
					
				  )
				 
			  }
			onChangestudentName(e) {
				this.setState({
					studentName :  e.target.value
				});
			}
			onChangeemailId(e) {
				this.setState({
					emailId :  e.target.value
				});
			}
			onChangemobileNumber(e) {
				this.setState({
					mobileNumber :  e.target.value
				});
			}
			onChangealterMobileNumber(e) {
				this.setState({
					alterMobileNumber :  e.target.value
				});
			}
			onChangeselectcourse(e) {
				this.setState({
					selectcourse :  e.target.value
				});
			}
			onChangeadmissionStatus(e) {
				this.setState({
					admissionStatus :  e.target.value
				});
			}
			
			uploadExcel = e => {
			
				//const files = Array.from(e.target.files)
				var file = e.target.files[0];
					
					var fileSize = e.target.files[0].size / 1024 / 1024;
					if(fileSize > 2){
						toast.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 2MB.");
						return false;
					}
					else { 
						if (file.name.split('.').pop()==='xlsx' || file.name.split('.').pop()==='xls') {
							this.setState({ 
								admissionExcel: e.target.files[0]})
						} else {
							toast.error('Please upload correct File , File extension should be (.xls|.xlsx)');
						   // $scope.ShowSpinnerStatus = false;
							return false;
						}
					}
					console.log(e.target.files[0])
				// this.setState({ uploading: true,
				// 	userPhoto : e.target.files[0]})
				
			};
			
			onUploadExcel(e){
				e.preventDefault();
				if(this.state.admissionExcel){
					
						const formData = new FormData();
						formData.append("admissionExcelFile", this.state.admissionExcel);
						
						axios.post(apiConfig.API_Path +'ManageAdmission/uploadExcel',formData,{
							// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
							headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
						})
						.then(res => 
							{
							if(res.data.status.status === '1'){
								toast.success(res.data.status.message)
								this.setState(
									{ 
										admissionExcel : null
					
									})
									
									window.location.reload(false);
								} else{
									toast.error(res.data.status.message)
									this.setState(
									{ 
										admissionExcel : null
									})
								} 
								}
								);
					
				}else{
					toast.error("upload student excel is required.")
					}
			}
			
			onSubmit(e){
			e.preventDefault();
			
			if(this.state.studentName  || this.props.studentName){
				if(this.state.studentName){
					if (!(this.state.studentName.match(/^([a-zA-Z-',.\s]{1,350})$/))) {
						this.refs.studentName.focus();
						toast.error("Valid student name is required.")
						return false;
					}
				}
				if(this.props.studentName){
					if (!(this.props.studentName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.studentName.focus();
						toast.error("Valid student name is required.")
						return false;
					}
				}
	
				if(this.state.emailId || this.props.emailId){
					if(this.state.emailId){
						if (!(this.state.emailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/))) {
							this.refs.emailId.focus();
							toast.error("Valid email id is required.")
							return false;
						}
					}
					if(this.props.emailId){
						if (!(this.props.emailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/))) {
							this.refs.emailId.focus();
							toast.error("Valid email id is required.")
							return false;
						}
					}
					if(this.state.mobileNumber || this.props.mobileNumber){
						if(this.state.mobileNumber){
							if (!(this.state.mobileNumber.match(/^[7-9][0-9]{9}$/))) {
								this.refs.mobileNumber.focus();
								toast.error("Valid mobile number is required.")
								return false;
							}
						}
						if(this.props.mobileNumber){
							if (!(this.props.mobileNumber.match(/^[7-9][0-9]{9}$/))) {
								this.refs.mobileNumber.focus();
								toast.error("Valid mobile number is required.")
								return false;
							}
						}
						if(this.state.alterMobileNumber || this.props.alterMobileNumber){
							if(this.state.alterMobileNumber ){
								if (!(this.state.alterMobileNumber.match(/^[7-9][0-9]{9}$/))) {
									this.refs.alterMobileNumber.focus();
									toast.error("Valid mobile number is required.")
									return false;
								}
							}
							if(this.props.alterMobileNumber ){
								if (!(this.props.alterMobileNumber.match(/^[7-9][0-9]{9}$/))) {
									this.refs.alterMobileNumber.focus();
									toast.error("Valid mobile number is required.")
									return false;
								}
							}

						}
							if(this.state.selectcourse  || this.props.selectcourse){
								
								if(this.state.admissionStatus  || this.props.admissionStatus){
									
				
											const formData = new FormData();
											//coureName
											if(this.state.studentName){
												formData.append("studentName", this.state.studentName);
											}else{
												formData.append("studentName", this.props.studentName);
											}
	
											//admissionType
											if(this.state.emailId){
												formData.append("emailId", this.state.emailId);
											}else{
												formData.append("emailId", this.props.emailId);
											}
	
											//admissionDuration
											if(this.state.mobileNumber){
												formData.append("mobileNumber", this.state.mobileNumber);
											}else{
												formData.append("mobileNumber", this.props.mobileNumber);
											}
	
											//startDate
											
											if(this.state.alterMobileNumber ){
												formData.append("alterMobileNumber", this.state.alterMobileNumber);
											}else{
												formData.append("alterMobileNumber", this.props.alterMobileNumber);
											}
											if((this.state.alterMobileNumber ==='' || this.state.alterMobileNumber ===undefined) && (this.props.alterMobileNumber ==='' || this.props.alterMobileNumber ===undefined)){
												formData.append("alterMobileNumber", '');
												
											}
	
											//endDate
											if(this.state.selectcourse){
												formData.append("selectcourse", this.state.selectcourse);
											}else{
												formData.append("selectcourse", this.props.selectcourse);
											}
											//subjectName
											if(this.state.admissionStatus){
												formData.append("admissionStatus", this.state.admissionStatus);
											}else{
												formData.append("admissionStatus", this.props.admissionStatus);
											}
	
											
											formData.append("admissionId", this.props.admissionId);
											
											if(this.props.admissionId){
												axios.post(apiConfig.API_Path +'ManageAdmission/updateDetails',formData,{
													// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
													headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
												})
												.then(res => 
													{
													if(res.data.status.status ==='1'){
														toast.success(res.data.status.message)
														this.setState(
															{ 
																admissionId : '',
																studentName : '',
																emailId : '',
																mobileNumber : '',
																alterMobileNumber : '',
																selectcourse : '',
																admissionStatus : '',
																hideDiv : false
											
															})
															
															window.location.reload(false);
													} else{
														toast.error(res.data.status.message)
														this.setState(
														{ 
															studentName : '',
															emailId : '',
															mobileNumber : '',
															alterMobileNumber : '',
															selectcourse : '',
															admissionStatus : '',
															admissionId : '',
														})
													} 
													}
													);
											}else{
												
												
											axios.post(apiConfig.API_Path +'ManageAdmission/insertDetails',formData,{
												// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
												headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
												})
												.then(res => 
													{
													if(res.data.status.status ==='1'){
														toast.success(res.data.status.message)
														this.setState(
															{ 
																studentName : '',
																emailId : '',
																mobileNumber : '',
																alterMobileNumber : '',
																selectcourse : '',
																admissionStatus : '',
																hideDiv : false
											
															})
															
															window.location.reload(false);
													} else{
														toast.error(res.data.status.message)
														this.setState(
														{ 
															studentName : '',
															emailId : '',
															mobileNumber : '',
															alterMobileNumber : '',
															selectcourse : '',
															admissionStatus : '',
															
														})
													} 
													}
													) ;
												}
										
								}else{
									this.refs.admissionStatus.focus();
									toast.error("Admission status is required.")
									}
							}else{
								this.refs.selectcourse.focus();
								toast.error("Select course is required.")
								}	
						// }else{
						// 	this.refs.startDate.focus();
						// 	toast.error("Start date is required.")
						// 	}	
					}else{
						this.refs.mobileNumber.focus();
						toast.error("Mobile number is required.")
						}						
				}else{
					this.refs.emailId.focus();
					toast.error("Email Id is required.")
					}				
			}else{
				this.refs.studentName.focus();
				toast.error("Student name is required.")
				}
		}
	
			HideDivFunction = () =>{
				const {hideDiv} = this.state;
				this.setState({
					hideDiv : !hideDiv
					
				}
				)
			
			}
		
			render(){
				
				return(
					<div>
						{this.state.hideDiv?
						<div className="col-xs-12">
							<div className="add-admission">
								<div className="col-xs-12">
									<div className="box-content card white">
										{this.props.admissionId? <h4 className="box-title">Update Student Admission Details</h4> : <h4 className="box-title">Add Student Admission Details </h4> }
										<form action="#" method="post">
                                        <div className="card-content text-center">
                                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div className="form-group">
												{/* <label className="control-label">Upload File</label> */}
												
                                                    <div className="preview-zone hidden">
                                                        <div className="box box-solid">
                                                            <div className="box-header with-border">
                                                                <div><b>Preview</b></div>
                                                                <div className="box-tools pull-right">
                                                                    <button type="button" className="btn btn-danger btn-xs remove-preview">
                                                                        <i className="fa fa-times"></i> Reset This Form
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div className="box-body"></div>
                                                        </div>
                                                    </div>
                                                    <div className="dropzone-wrapper">
                                                        <div className="dropzone-desc">
                                                            <i className="glyphicon glyphicon-download-alt"></i>
                                                            <p>Excel upload will replace all existing leads.</p>
															
                                                        </div>
														
                                                        <input type="file" name="img_logo" className="dropzone" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="exampleInputFile"  onChange={this.uploadExcel}/>
														{/* <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" name="FormFeesList"  id="exampleInputFile" onchange="angular.element(this).scope().uploadedFile(this)" required /> */}
                                                    </div>
                                                 </div>
                                            </div>

                                            
                                            <div className="col-lg-12">
                                                <div className="input-group">
                                                    <div className="col-lg-6 col-md-6">
                                                        <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light" onClick={this.onUploadExcel}>Assign & Submit</button>
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <button id="HideAddLeads" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </form>

                                    
										
										<div className="card-content text-center">
											<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<button type="button" className="btn btn-info btn-circle btn-lg waves-effect waves-light">
													<span>OR</span> 
												</button>
											</div>
										</div>
									</div>
								</div>
								<div className="col-xs-12">
									<div className="box-content card white">
										{this.props.admissionId? <h4 className="box-title">Update Student Admission Details</h4> : <h4 className="box-title">Add Student Admission Details </h4> }
										<form action="#" method="post">
											<div className="card-content">
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-user"></i></label></div>
														<input id="ig-1" type="text" className="form-control" placeholder="Student's Name" ref="studentName" defaultValue={this.state.studentName ||this.props.studentName } onChange={(e)=>this.onChangestudentName(e)}/>
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-2" className="btn btn-default"><i className="fa fa-envelope"></i></label></div>
														<input id="ig-2" type="text" className="form-control" placeholder="Email address" ref="emailId" defaultValue={this.state.emailId ||this.props.emailId } onChange={(e)=>this.onChangeemailId(e)}/>
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-mobile"></i></label></div>
														<input id="ig-3" type="text" className="form-control" placeholder="Mobile Number" ref="mobileNumber" defaultValue={this.state.mobileNumber ||this.props.mobileNumber } onChange={(e)=>this.onChangemobileNumber(e)} maxLength="10" />
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-mobile"></i></label></div>
														<input id="ig-4" type="text" className="form-control" placeholder="Alternate Mobile Number" ref="alterMobileNumber" defaultValue={this.state.alterMobileNumber ||this.props.alterMobileNumber } onChange={(e)=>this.onChangealterMobileNumber(e)} maxLength="10" />
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-user"></i></label></div>
														{/* <select id="ig-4" className="form-control" ref="selectadmission" defaultValue={this.state.selectadmission ||this.props.selectadmission } onChange={(e)=>this.onChangeselectadmission(e)}> */}
														<select id="ig-4" className="form-control" ref="selectcourse" defaultValue={this.state.selectcourse ||this.props.selectcourse } onChange={(e)=>this.onChangeselectcourse(e)}>
															{this.state.courseList.map((courseData) => <option key={courseData.value} value={courseData.value} selected={courseData.Selected}>{courseData.display}</option>)}
															{/* <option value=''>Select Course</option>
															{this.state.courseList.map((courseData) => <option key={courseData.course_id} value={courseData.course_id }  >{courseData.course_name}</option>)} */}
					
														</select>
													</div>
												</div>
												
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-33" className="btn btn-default"><i className="fa fa-user"></i></label></div>
														<select id="ig-33" className="form-control" ref="admissionStatus" defaultValue={this.state.admissionStatus ||this.props.admissionStatus } onChange={(e)=>this.onChangeadmissionStatus(e)}>
															<option>Admission Status</option>
															<option value="Pending">Pending</option>
															<option value="Hold">Hold</option>
															<option value="Approved">Approved</option>
															<option value="Cancel">Canceled</option>
														</select>
													</div>
												</div>
												<div className="input-group">
													<div className="col-lg-6 col-md-6">
													{this.props.admissionId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
														
													</div>
													<div className="col-lg-6 col-md-6">
														<button id="hide-add-role" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>: null}
					</div>
				
				)
			}
		}
	
		
	
	


	export default ManageAdmission;