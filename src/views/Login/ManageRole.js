import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import { toast } from 'react-toastify';
import ReactPaginate from 'react-paginate';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');


const IconColor = {
	color: 'blue',

}
const IconSize = {
	width: '170px',
	height: '80px',

}
class ManageRole extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			offset: 0,
			perPage: 10,
			currentPage: 0,
			addRole: false,
			editRole: false,
			roleList: [],
			roleId: '',
			roleName: '',
		}
		this.logout = this.logout.bind(this);
		this.handlePageClick = this.handlePageClick.bind(this);

	}
	ShowDivfunction = () => {
		const { addRole } = this.state;
		this.setState({
			addRole: !addRole
		}
		)
	}

	ShowEditDivFunction = (roleData) => {
		const { editRole } = this.state;
		this.setState({
			editRole: !editRole,
			roleId: roleData.role_id,
			roleName: roleData.role_name,

		}
		)
	}

	logout() {
		localStorage.setItem("userLoggedIn", "False");
		localStorage.setItem("user_id", "");
		localStorage.setItem("user_role", "");
		localStorage.clear();
		setTimeout(() => {
			window.location = "Login";
		}, 1000)
	}

	handlePageClick = (e) => {
		const selectedPage = e.selected;
		const offset = selectedPage * this.state.perPage;

		this.setState({
			currentPage: selectedPage,
			offset: offset
		}, () => {
			this.getDetails()
		});

	};
	getDetails() {
		const apiUrl = apiConfig.API_Path +'ManageRole/getDetails';

		fetch(apiUrl)
			.then(res => res.json())
			.then(
				(result) => {
					// this.setState({
					// 	roleList: result
					// });
					const roleList = result;
					const slice = roleList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
						pageCount: Math.ceil(roleList.length / this.state.perPage),
						roleList: slice
					})
				}

			)


	}

	getAccessPermissions() {
		var role = localStorage.getItem('role_name');
		if (role === 'Master Admin') {
			localStorage.setItem("editPermission", 'true');
			localStorage.setItem('deletePermission', 'true');
			localStorage.setItem('viewPermission', 'true');
		} else {
			const apiUrl = apiConfig.API_Path + 'ManageRole/getAccessPermissions';
			const formData = new FormData();
			formData.append('userId', localStorage.getItem('user_role'));
			formData.append('ManageUser', "Manage Role");
			const options = {
				method: 'POST',
				body: formData
			}
			fetch(apiUrl, options)
				.then(res => res.json())
				.then(
					(result) => {
						localStorage.setItem("editPermission", result[0]['edit_permission']);
						localStorage.setItem('deletePermission', result[0]['delete_permission']);
						localStorage.setItem('viewPermission', result[0]['view_permission']);
						console.log(result)
					}

				)
		}

	}
	componentDidMount() {
		$(".commonRemove").removeClass('current');
		$("#ManageRole").addClass('current');
		this.getDetails();
		this.getAccessPermissions();
	}
	deleterole(roleId) {
		const { roles } = this.state;

		const formData = new FormData();
		formData.append('roleId', roleId);

		const options = {
			method: 'POST',
			body: formData
		}

		fetch(apiConfig.API_Path + 'ManageRole/deleteRole', options)

			.then(res => res.json())
			.then(
				(result) => {
					toast.success(result.message)
					this.getDetails();
				},
				(error) => {
					toast.error("Opps! Something went Wrong.")
				}
			)
	}


	onChangeSearch(e) {
		//const searchData = e.target.value
		const formData = new FormData();
		formData.append('searchData', e.target.value);

		const options = {
			method: 'POST',
			body: formData
		}
		fetch(apiConfig.API_Path + 'ManageRole/onChangeSearch', options)
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						roleList: result
					});
				}
			)
	}

	render() {
		if (localStorage.getItem("userLoggedIn") === "False" || localStorage.getItem("userLoggedIn") === null) {
			window.location = "login";
		} else {
			const { roleList } = this.state;
			return (
				<div>
					<header id="header">
						<div className="main-menu">
							<div className="content">
								<a href="AdminDashboard" className="logo">
									<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize} />
								</a>
								<Navigation />
							</div>
						</div>


						<div className="fixed-navbar ">
							<div className="pull-left">
								<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
								<h1 className="page-title"> Call Management System - Manage Role</h1>
							</div>

							<div className="pull-right">
								<div className="ico-item">
									<i className="fa fa-user" style={IconColor}></i>
									<ul className="sub-ico-item">
										<li><a onClick={this.logout} >Log Out</a></li>
										<li><a href="ChangePassword">Change Password</a></li>
									</ul>
								</div>
							</div>
						</div>
					</header>
					<div id="wrapper">
						<div className="main-content">

							<div className="row small-spacing">
								{this.state.addRole ? <ShowDiv /> : null}

								{this.state.editRole ? <ShowDiv
									roleId={this.state.roleId}
									roleName={this.state.roleName}

								/> : null}
							</div>
							<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">Role List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-role-btn" className="btn btn-primary btn-sm" onClick={() => this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add Role</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch" placeholder="Search by Role" className="form-control" onChange={(e) => this.onChangeSearch(e)} />
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
										<table id="role-list" className="table table-striped table-bordered display" >
											<thead>
												<tr>
													<th>Sr. No.</th>
													<th>Role Name</th>
													{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? <th>Action</th> : null}
												</tr>
											</thead>

											{localStorage.getItem('viewPermission') === 'true' ?
												<tbody>
													{roleList.map((roledata, index) => (
														<tr key={roledata.role_id}>
															<React.Fragment>
																<td>{index + 1}</td>
																<td>{roledata.role_name}</td>
																{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ?
																	<td>
																		{localStorage.getItem('editPermission') === 'true' ?
																			<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light" title="Edit role Details" onClick={() => this.ShowEditDivFunction(roledata)}>
																				<i className="ico fa fa-edit"></i>
																			</button> : null}
																		{localStorage.getItem('deletePermission') === 'true' ?
																			<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete role" onClick={() => this.deleterole(roledata.role_id)}>
																				<i className="ico fa fa-trash"></i>
																			</button> : null}

																	</td>
																	: null}
															</React.Fragment>
														</tr>
													))}

												</tbody>
												: null}
										</table>
										<ReactPaginate
											previousLabel={"prev"}
											nextLabel={"next"}
											breakLabel={"..."}
											breakClassName={"break-me"}
											pageCount={this.state.pageCount}
											marginPagesDisplayed={2}
											pageRangeDisplayed={5}
											onPageChange={this.handlePageClick}
											containerClassName={"pagination"}
											subContainerClassName={"pages pagination"}
											activeClassName={"active"} />
									</div>
								</div>

							</div>

							<footer id="footer" className="footer">
								<ul className="list-inline">
									<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
									<li><a href="#">Privacy</a></li>
									<li><a href="#">Coockies</a></li>
									<li><a href="#">Legal Notice</a></li>
								</ul>
							</footer>
						</div>
					</div>
				</div>
			);
		}
	}
}

class ShowDiv extends Component {
	constructor(props) {
		super(props)
		this.state = {
			hideDiv: true,
			roleName: '',

		}

		this.onChangeroleName = this.onChangeroleName.bind(this);
		this.onSubmit = this.onSubmit.bind(this);

	}
	onChangeroleName(e) {
		this.setState({
			roleName: e.target.value
		});
	}

	onSubmit(e) {
		e.preventDefault();

		if (this.state.roleName || this.props.roleName) {
			if (this.state.roleName) {
				if (!(this.state.roleName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
					this.refs.roleName.focus();
					toast.error("Valid role name is required.")
					return false;
				}
			}
			if (this.props.roleName) {
				if (!(this.props.roleName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
					this.refs.roleName.focus();
					toast.error("Valid role name is required.")
					return false;
				}
			}

			const formData = new FormData();

			if (this.state.roleName) {
				formData.append("roleName", this.state.roleName);
			} else {
				formData.append("roleName", this.props.roleName);
			}

			formData.append("roleId", this.props.roleId);

			if (this.props.roleId) {
				axios.post(apiConfig.API_Path + 'ManageRole/updateDetails', formData, {
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
					.then(res => {
						if (res.data.status.status === '1') {
							toast.success(res.data.status.message)
							this.setState(
								{
									roleId: '',
									roleName: '',
									hideDiv: false

								})

							window.location.reload(false);
						} else {
							toast.error(res.data.status.message)
							this.setState(
								{
									roleName: '',
									roleId: '',
								})
						}
					}
					);
			} else {


				axios.post(apiConfig.API_Path + 'ManageRole/insertDetails', formData, {
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
					.then(res => {
						if (res.data.status.status === '1') {
							toast.success(res.data.status.message)
							this.setState(
								{
									roleName: '',
									hideDiv: false

								})

							window.location.reload(false);
						} else {
							toast.error(res.data.status.message)
							this.setState(
								{
									roleName: '',

								})
						}
					}
					);
			}

		} else {
			this.refs.roleName.focus();
			toast.error("Role name is required.")
		}
	}

	HideDivFunction = () => {
		const { hideDiv } = this.state;
		console.log(hideDiv);
		this.setState({
			hideDiv: !hideDiv

		}
		)

	}

	render() {
		return (
			<div>
				{this.state.hideDiv ?
					<div className="col-xs-12">
						<div className="add-role">
							<div className="col-xs-12">
								<div className="box-content card white">
									{this.props.roleId ? <h4 className="box-title">Update Role </h4> : <h4 className="box-title">Add Role </h4>}
									<form action="#" method="post">
										<div className="card-content">
											<div className="col-lg-6 col-md-6">
												<div className="input-group margin-bottom-20">
													<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-user"></i></label></div>
													<input id="ig-1" type="text" ref="roleName" className="form-control" placeholder="Role Name" defaultValue={this.state.roleName || this.props.roleName} onChange={(e) => this.onChangeroleName(e)} />
												</div>

											</div>


											<div className="input-group">
												<div className="col-lg-6 col-md-6">
													{this.props.roleId ? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light" onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light" onClick={this.onSubmit}>Submit</button>}

												</div>
												<div className="col-lg-6 col-md-6">
													<button id="hide-add-role" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={() => this.HideDivFunction()}>Cancel</button>
												</div>
											</div>

										</div>
									</form>
								</div>
							</div>
						</div>
					</div> : null}
			</div>

		)
	}
}




export default ManageRole;