import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import {toast} from 'react-toastify';
import DatePicker from 'react-date-picker';
import ReactPaginate from 'react-paginate';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');


	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
    }

	class ManageLead extends React.Component {
		constructor(props){
			super(props)
			this.state = {
				offset: 0,
				perPage: 10,
				currentPage: 0,
				addleads :false,
				editleads :false,
				leadsList: [],
				userList : [],
				leadsId : '',
				selectUser : '',
				studentName : '',
				emailId : '',
				mobileNumber : '', 
				campaignName : '',
				receivedDate : '',
				alterMobileNumber:'', 
				selectedValue : false,
				selectedMultileadsValue : [],
				selected : {},
				selectResponse : '',
			}
			
			this.onChangeselectUser = this.onChangeselectUser.bind(this);
			this.logout = this.logout.bind(this);
			this.handlePageClick = this.handlePageClick.bind(this);
			this.indivisualAssignLeadsFunction = this.indivisualAssignLeadsFunction.bind(this);
			this.assignLeadsToSeniorCounellor = this.assignLeadsToSeniorCounellor.bind(this);
			this.onChangeselectResponse = this.onChangeselectResponse.bind(this);
			this.selectedMultileLeads = this.selectedMultileLeads.bind(this);
			//this.selectedLeadsData = this.selectedLeadsData.bind(this);
			this.assignMultipleLeads = this.assignMultipleLeads.bind(this);
		}
		ShowDivfunction = () =>{
			const {addleads} = this.state;
			this.setState({
				addleads : !addleads
			}
			)
		}
		
		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};
		
		
		selectedMultileLeads = (leadsData,e) => {
			if(e.target.checked) {
				let arr = this.state.selectedMultileadsValue;
				arr.push(leadsData.student_name);
				this.setState({ selectedMultileadsValue: arr})
			}else {			
				let items = this.state.selectedMultileadsValue.splice(this.state.selectedMultileadsValue.indexOf(leadsData.student_id), 1);
				
				this.setState({
					selectedMultileadsValue: items
				})
				
			}
			
		}
		

		
		ShowEditDivFunction = (leadsData) =>{
			const {editleads} = this.state;
			this.setState({
				editleads : !editleads,
				leadsId : leadsData.leads_id,
				selectUser : leadsData.selected_user,
				studentName : leadsData.leads_type,
				emailId : leadsData.leads_reason,
				mobileNumber : leadsData.start_date,
				receivedDate :leadsData.end_date,
				alterMobileNumber: leadsData.no_of_days,
				
			}
			)
			console.log(leadsData)
		}
		getUserDeatils(){
			const apiUrl = apiConfig.API_Path +'ManageLead/getUserDeatils';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(response => {
				let userdetails = response.map(userData => {
				  return {value: userData.user_id, display: userData.user_name, Selected : false}
				}); 
				
					const tempArray = [];
					userdetails.forEach(element => {
						
						if(element.value === this.props.selectUser){
							tempArray.push({value: element.value, display: element.display,Selected: true})
						}else{
							tempArray.push({value: element.value, display: element.display,Selected: false})
						}
					});
					
					this.setState({
						telecalleruserList: [{value: '', display: 'Select User',Selected: true}].concat(tempArray)
						});
				}
				
			  )
			 
		  }

		onChangeselectUser(e) {
		this.setState({
			selectUser :  e.target.value
		});
		}
		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}

		checkRole(){
			var roledata = localStorage.getItem('role_name');
			if(roledata === 'Master Admin'){
				this.getDetails();
				this.getUserDeatils();
			}else{
				this.getIndividualDetails();
				this.getRolewiseDeatils();
			}
			// }else if(roledata === 'Telecaller'){
			// 	this.getIndividualDetails();
			// 	this.getRolewiseDeatils();
			// }else if(roledata === 'Senior Counsellor'){
			// 	this.getSeniorCounsellorIndividualDetails();
			// 	//this.getRolewiseDeatils();
			// }else{
			// 	this.getFacultyIndividualDetails();
			// 	//this.getRolewiseDeatils();
			// }
		}

		getDetails() {
			const apiUrl = apiConfig.API_Path +'ManageLead/getDetails';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	leadsList: result
					// });
					const leadsList = result;
					const slice = leadsList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(leadsList.length / this.state.perPage),
						leadsList : slice
					})
				}
				
			  )
			}

		getIndividualDetails(){
			const apiUrl = apiConfig.API_Path +'ManageLead/getIndividualDetails';
		
			const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_id'));
				const options = {
					method: 'POST',
					body: formData
				}

			fetch(apiUrl,options)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	leadsList: result
					// });
					const leadsList = result;
					const slice = leadsList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(leadsList.length / this.state.perPage),
						leadsList : slice
					})
				}
				
			  )
		}

		
		getSeniorCounsellorIndividualDetails(){
			const apiUrl = apiConfig.API_Path +'ManageLead/getSeniorCounsellorIndividualDetails';
		
			const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_id'));
				const options = {
					method: 'POST',
					body: formData
				}

			fetch(apiUrl,options)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	leadsList: result
					// });
					const leadsList = result;
					const slice = leadsList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(leadsList.length / this.state.perPage),
						leadsList : slice
					})
				}
				
			  )
		}
		getUserDeatils(){
		const apiUrl = apiConfig.API_Path +'ManageLead/getUserDeatils';
	
		fetch(apiUrl)
			.then(res => res.json() )
			.then(response => {
			let coursedetails = response.map(userData => {
				return {value: userData.user_id, display: userData.user_name, Selected : false}
			}); 
			
				// this.setState({
				// userList: [{value: '', display: 'Select Course',Selected: true}].concat(coursedetails)
				// });
				const tempArray = [];
				coursedetails.forEach(element => {
					
					if(element.value === this.props.selectUser){
						tempArray.push({value: element.value, display: element.display,Selected: true})
					}else{
						tempArray.push({value: element.value, display: element.display,Selected: false})
					}
				});
				
				this.setState({
					userList: [{value: '', display: 'Assign Lead',Selected: true},{value: 'Distribute Equally', display: 'Distribute Equally',Selected: false},{value: 'Distribute Randomly', display: 'Distribute Randomly',Selected: false}].concat(tempArray)
					});
				// this.setState({
				// 	userList: response
				// 	});
				
			}
			
			)
			
		}
		
		getRolewiseDeatils(){
			const apiUrl = apiConfig.API_Path +'ManageLead/getRolewiseDeatils';
	
		fetch(apiUrl)
			.then(res => res.json() )
			.then(response => {
			let coursedetails = response.map(userData => {
				return {value: userData.user_id, display: userData.user_name, Selected : false}
			}); 
			
				// this.setState({
				// userList: [{value: '', display: 'Select Course',Selected: true}].concat(coursedetails)
				// });
				const tempArray = [];
				coursedetails.forEach(element => {
					
					if(element.value === this.props.selectUser){
						tempArray.push({value: element.value, display: element.display,Selected: true})
					}else{
						tempArray.push({value: element.value, display: element.display,Selected: false})
					}
				});
				
				this.setState({
					userList: [{value: '', display: 'Assign Lead',Selected: true}].concat(tempArray)
					});
				// this.setState({
				// 	userList: response
				// 	});
				
			}
			
			)
		}

		getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				const apiUrl = apiConfig.API_Path +'ManageLead/getAccessPermissions';
				const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_role'));
				formData.append('ManageUser', "Manage Leads");
				const options = {
					method: 'POST',
					body: formData
				}
				fetch(apiUrl,options)
				.then(res => res.json() )
				.then(
					(result) => {
						
						localStorage.setItem("editPermission",result[0]['edit_permission']);
						localStorage.setItem('deletePermission',result[0]['delete_permission']);
						localStorage.setItem('viewPermission',result[0]['view_permission']);
						console.log(result)
					}
					
				)
			}
			
		}

		componentDidMount() {
			$(".commonRemove").removeClass('current');
			$("#ManageLead").addClass('current');
				this.checkRole();
			// this.getDetails();
			// this.getUserDeatils();
			this.getAccessPermissions();
		}

		assignMultipleLeads = (e) =>{
			
			const formData = new FormData();
				formData.append("selectedMultileadsValue", this.state.selectedMultileadsValue);
				formData.append("selectUser", this.state.selectUser);
				axios.post(apiConfig.API_Path +'ManageLead/assignMultipleLeads',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
				.then(res => 
					{
					if(res.data.status.status === '1'){
						toast.success(res.data.status.message)
						this.setState(
							{ 
								leadsId : '',
								selectUser : '',
							})
							
							window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								selectUser : '',
								leadsId : '',
							})
						} 
						}
						);
		}
		onChangeselectResponse = (leadsData,e) => {
			this.setState({
				selectResponse :  e.target.value
			});
			if(this.state.selectResponse != ''){
			const formData = new FormData();
				formData.append("leadId", leadsData.lead_id);
				formData.append("selectResopnse", e.target.value);
				axios.post(apiConfig.API_Path +'ManageLead/onChangeselectResponse',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
				.then(res => 
					{
					if(res.data.status.status === '1'){
						toast.success(res.data.status.message)
						this.setState(
							{ 
								leadsId : '',
								selectResponse : '',
							})
							
							window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								selectResponse : '',
								leadsId : '',
							})
						} 
						}
						);
					}
		}

		indivisualAssignLeadsFunction(leadsData){
			if(this.state.selectUser){
				const formData = new FormData();
				formData.append("leadId", leadsData.lead_id);
				formData.append("selectUser", this.state.selectUser);
				axios.post(apiConfig.API_Path +'ManageLead/indivisualAssignLeadsFunction',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
				.then(res => 
					{
					if(res.data.status.status === '1'){
						toast.success(res.data.status.message)
						this.setState(
							{ 
								leadsId : '',
								selectUser : '',
							})
							
							window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								selectUser : '',
								leadsId : '',
							})
						} 
						}
						);
			}else{
			this.refs.selectUser.focus();
			toast.error("assign lead is required.")
			}
		}

		
		assignLeadsToSeniorCounellor(leadsData){
			if(this.state.selectUser){
				const formData = new FormData();
				formData.append("leadId", leadsData.lead_id);
				formData.append("selectUser", this.state.selectUser);
				axios.post(apiConfig.API_Path +'ManageLead/assignLeadsToSeniorCounellor',formData,{
					// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
				})
				.then(res => 
					{
					if(res.data.status.status === '1'){
						toast.success(res.data.status.message)
						this.setState(
							{ 
								leadsId : '',
								selectUser : '',
							})
							
							window.location.reload(false);
						} else{
							toast.error(res.data.status.message)
							this.setState(
							{ 
								selectUser : '',
								leadsId : '',
							})
						} 
						}
						);
			}else{
			this.refs.selectUser.focus();
			toast.error("assign lead is required.")
			}
		}

		deleteleads(leadsId) {
		const { leadss } = this.state;
	
		const formData = new FormData();
		formData.append('leadsId', leadsId);
	
		const options = {
			method: 'POST',
			body: formData
		}
	
		fetch(apiConfig.API_Path +'ManageLead/deleteDetails', options)
		
		.then(res => res.json())
		.then(
			(result) => {
				toast.success(result.message)
				this.getDetails();
			},
			(error) => {
				toast.error("Opps! Something went Wrong.")
			}
		)
		}
		
			
		onChangeSearch(e) {
			//const searchData = e.target.value
			const formData = new FormData();
			formData.append('searchData', e.target.value);
		
			const options = {
				method: 'POST',
				body: formData
			}
			fetch(apiConfig.API_Path +'ManageLead/onChangeSearch',options)
				.then(res => res.json() )
				.then(
				(result) => {
					this.setState({
						leadsList: result
					});
				}
				// ,
				// (error) => {
				//   this.setState({ error });
				// }
				)
				
				
			}
		
		render() {
			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				const { leadsList} = this.state;
				return (
						<div>
							 
							<header id="header">
							
								<div className="main-menu">
									 
									<div className="content">
										<a href="AdminDashboard" className="logo">
											<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
										</a>
										<Navigation />
									
									</div>
								</div>
							
			
								<div className="fixed-navbar ">
									<div className="pull-left">
										<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
										<h1 className="page-title"> Call Management System - Manage leads</h1>
									
									</div>
						
									<div className="pull-right">
											{/* <div className="ico-item">
												<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
												<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
												
											</div> */}
							
									<div className="ico-item">
										<i className="fa fa-user" style={IconColor}></i>
										<ul className="sub-ico-item">
											<li><a onClick= {this.logout} >Log Out</a></li>
											<li><a href="ChangePassword">Change Password</a></li>
										</ul>
										
									</div>
							</div>
								</div>
							</header>
					<div id="wrapper">
						<div className="main-content">
			
						<div className="row small-spacing">
							{this.state.addleads? <ShowDiv /> :null}
			
							{this.state.editleads? <ShowDiv 
								leadsId = {this.state.leadsId} 
								selectUser = {this.state.selectUser}
								studentName = {this.state.studentName}
								emailId  = {this.state.emailId}
								mobileNumber = {this.state.mobileNumber}
								receivedDate = {this.state.receivedDate}
								alterMobileNumber = {this.state.alterMobileNumber}
								
								
							/> :null}
						</div>
						<div className="col-xs-12">
								<div className="box-content bordered primary js__card">
									<div className="col-lg-6 col-md-6">
										<h4 className="box-title pull-left">Leads List</h4>
									</div>
									<div className="col-lg-6 col-md-6">
										<div className="pull-right">
											<a id="add-leads-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add leads</a>
											<a id="assign-multiple-btn" className="btn btn-success btn-sm"><i className="fa fa-plus"></i> Assign Multiple</a>
										</div>
									</div>
									<div className="clearfix">&nbsp;</div>
									<div className="col-lg-12">
										<div className="col-lg-4 pull-right">
											<div className="form-group">
												<div className="input-group">
													<input type="text" name="inputSearch"placeholder="Search by details" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
													<span className="input-group-addon"><i className="fa fa-search"></i></span>
												</div>
												<div className="input-group-btn"> </div>
											</div>
										</div>
									</div>
									<div className="table table-responsive">
										<table id="leads-list" className="table table-striped table-bordered display" >
										<thead>
												<tr>
													<th>Sr. No.</th>
													{/* <th><input type="checkbox" onClick={this.handleAllChecked}  value="checkedall" /> </th> */}
													<th>Select</th>
													<th>Name</th>
													<th>Email</th>
													<th>Mobile No.</th>
													<th>Alternate No.</th>
													<th>Course</th>
													<th>Source</th>
													<th>Received Date</th>
													{localStorage.getItem('role_name') === 'Master Admin' ?
														<th>Assign To</th> 
													:null}
													{localStorage.getItem('role_name') !== 'Master Admin' ?
														<th>Select Basic Resopnse</th> 
													:null}
													{localStorage.getItem('role_name') !== 'Master Admin' ?
														<th>Resopnse</th> 
													:null}
													{localStorage.getItem('role_name') !== 'Master Admin' ?
														<th>Assign To</th> 
													:null}
													{localStorage.getItem('role_name') !== 'Master Admin' ?
														<th>Assign To</th> 
													:null}
													{/* <th>User ID</th> */}
												</tr>
											</thead>
											{/* <tfoot>
												<tr>
													<th>Sr. No.</th>
													<th>leads Name</th>
													<th>Action</th>
												</tr>
											</tfoot> */}
											{localStorage.getItem('viewPermission') === 'true' ?
											<tbody>
											{leadsList.map((leadsData,index) => (
												<tr key={leadsData.lead_id}>
													<React.Fragment> 
													<td>{index+1}</td>
													
													{/* <td>
																<form>
																	<div className="checkbox">
																		<input type="checkbox" id="chk-1"/><label htmlFor="chk-1"></label> 
																	</div>
																</form>
															</td> */}
															<td ><input id="chkCustomer_{{leadsData.lead_id}}" type="checkbox" checked={this.state.selectedMultileadsValue.find((p) => p.lead_id === leadsData.lead_id)} defaultValue={this.state.selectedMultileadsValue} onClick={(e) => this.selectedMultileLeads(leadsData,e)}  /></td>
															<td>{leadsData.student_name}</td>
															<td>{leadsData.email_id}</td>
															<td>{leadsData.mobile_number}</td>
															<td>{leadsData.alter_mobile_number}</td>
															<td>{leadsData.selected_course}</td>
															<td>{leadsData.selected_source}</td>
															<td>{leadsData.date}</td>
															
															{localStorage.getItem('role_name') === 'Master Admin' ?
																<td>
																	<form>
																		<div className="form-group">
																			<select id="ig-4" className="form-control" ref="selectUser" defaultValue={this.state.selectUser ||this.props.selectUser } onChange={(e)=>this.onChangeselectUser(e)}>
																				{this.state.userList.map((userData) => <option key={userData.value} value={userData.value} selected={userData.Selected}>{userData.display}</option>)}
																			</select>
																		</div>	
																		<button type="button" className="btn btn-primary  btn-xs waves-effect waves-light"  title="Edit leads Details" onClick={() => this.indivisualAssignLeadsFunction(leadsData)}>
																		Assign Lead
																	</button>
																	</form>
																</td>
															:null}
															{localStorage.getItem('role_name') !== 'Master Admin' ?
																<td>
																	<form>
																	<select id="ig-33"  className="form-control" ref="selectResponse" defaultValue={this.state.selectResponse ||this.props.selectResponse }  onClick={(e) => this.onChangeselectResponse(leadsData,e)}>
																		<option value=" ">Select Resopnse</option>
																		<option value="Very Positive" >Very Positive</option>
																		<option value="Positive" >Positive</option>
																		<option value="Negative">Negative</option>
																		<option value="No Answer">No Answer</option>
																		<option value="Not Reachable">Not Reachable</option>
																		
																	</select>   
																	</form>
																</td>
															:null}
															{localStorage.getItem('role_name') !== 'Master Admin' ?
																<td>{leadsData.response}</td>
															:null}
															{localStorage.getItem('role_name') !== 'Master Admin' ?
																<td>
																	<form>
																		<div className="form-group">
																			<select id="ig-4" className="form-control" ref="selectUser" defaultValue={this.state.selectUser ||this.props.selectUser } onChange={(e)=>this.onChangeselectUser(e)}>
																				{this.state.userList.map((userData) => <option key={userData.value} value={userData.value} selected={userData.Selected}>{userData.display}</option>)}
																			</select>
																		</div>	
																		<button type="button" className="btn btn-primary  btn-xs waves-effect waves-light"  title="Edit leads Details" onClick={() => this.assignLeadsToSeniorCounellor(leadsData)}>
																		Assign Lead
																	</button>
																	</form>
																</td>
															:null}
															
															{/* <td>{leadsData.assign_lead}</td> */}
													
													
													{/* <td>
														<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light"  title="Edit leads Details" onClick={() => this.ShowEditDivFunction(leadsData)}>
															<i className="ico fa fa-edit"></i>
														</button>
														
														<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete leads" onClick={() => this.deleteleads(leadsData.leads_id)}>
															<i className="ico fa fa-trash"></i>
														</button>
													</td> */}
												</React.Fragment> 
												</tr>
											))}
												
											</tbody>
											:null}
											
										</table>
										<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
									</div>
									
									{/* <form>
										<div className="col-lg-12 col-md-12">
											<div className="input-group margin-bottom-20">
												<div className="input-group-btn"><label htmlFor="ig-11" className="btn btn-default"><i className="fa fa-user"></i></label></div>
												
												<input id="ig-11" type="text" className="form-control" placeholder="selected leads" ref="selectedMultileadsValue" defaultValue={this.state.selectedMultileadsValue ||this.props.selectedMultileadsValue } onChange={(e)=>this.selectedLeadsData(e)}/>
												
											</div>
											
										</div>
										<div className="col-lg-6 col-md-6">
										<div className="input-group margin-bottom-20">
											<div className="input-group-btn"><label htmlFor="ig-11" className="btn btn-default"><i className="fa fa-user"></i></label></div>
												<select id="ig-4" className="form-control" ref="selectUser" defaultValue={this.state.selectUser ||this.props.selectUser } onChange={(e)=>this.onChangeselectUser(e)}>
													{this.state.userList.map((userData) => <option key={userData.value} value={userData.value} selected={userData.Selected}>{userData.display}</option>)}
												</select>
											</div>	
										</div>
										<button type="button" className="btn btn-primary  btn-xs waves-effect waves-light"  title="Edit leads Details" onClick={() => this.assignMultipleLeads()}>
										Assign Multiple Leads
										</button>
									</form>		 */}
								</div>
								
							</div>
										
								<footer id="footer" className="footer">
									<ul className="list-inline">
										<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
										<li><a href="#">Privacy</a></li>
										<li><a href="#">Coockies</a></li>
										<li><a href="#">Legal Notice</a></li>
									</ul>
								</footer>
							</div>
						</div>
					</div>
				);
			}
		}
		  }
	
		class ShowDiv extends Component{
			constructor(props){
				super(props)
				this.state = {
					hideDiv :true,
					userList : [],
					campaignList : [],
					selectUser : '',
					selectCampaign : '',
					courseList : [],
					selectcourse : '',
					sourceList : [],
					selectsource : '',
					studentName : '',
					emailId : '',
					mobileNumber : '', 
					campaignName : '',
					receivedDate : '',
					alterMobileNumber:'', 
					campaignName : '',
					leadExcel : null
					
					
				}
				this.getUserDeatils();
				this.getCourseDeatils();
				this.getSourceDeatils();
				this.getCampaignDeatils();
				this.uploadExcel = this.uploadExcel.bind(this);
				this.onChangeselectUser = this.onChangeselectUser.bind(this);
				this.onChangeselectCampaign = this.onChangeselectCampaign.bind(this);
				this.onChangeselectcourse = this.onChangeselectcourse.bind(this);
				this.onChangeselectsource = this.onChangeselectsource.bind(this);
				this.onChangestudentName = this.onChangestudentName.bind(this);
				this.onChangeemailId = this.onChangeemailId.bind(this);
				this.onChangemobileNumber = this.onChangemobileNumber.bind(this);
				this.onChangereceivedDate = this.onChangereceivedDate.bind(this);
				this.onChangealterMobileNumber = this.onChangealterMobileNumber.bind(this);
				this.onChangecampaignName = this.onChangecampaignName.bind(this);
				this.onSubmit = this.onSubmit.bind(this);
				this.onUploadExcel = this.onUploadExcel.bind(this);
				
			}
			getUserDeatils(){
				const apiUrl = apiConfig.API_Path +'ManageLead/getUserDeatils';
			
				fetch(apiUrl)
				  .then(res => res.json() )
				  .then(response => {
					let userdetails = response.map(userData => {
					  return {value: userData.user_id, display: userData.user_name, Selected : false}
					}); 
					
						const tempArray = [];
						userdetails.forEach(element => {
							
							if(element.value === this.props.selectUser){
								tempArray.push({value: element.value, display: element.display,Selected: true})
							}else{
								tempArray.push({value: element.value, display: element.display,Selected: false})
							}
						});
						
						this.setState({
							userList: [{value: '', display: 'Select User',Selected: true},{value: 'Distribute Equally', display: 'Distribute Equally',Selected: false},{value: 'Distribute Randomly', display: 'Distribute Randomly',Selected: false}].concat(tempArray)
							});
					}
					
				  )
				 
			  }

			getCampaignDeatils(){
				const apiUrl = apiConfig.API_Path +'ManageLead/getCampaignDeatils';
			
				fetch(apiUrl)
				  .then(res => res.json() )
				  .then(response => {
					let camaigndetails = response.map(userData => {
					  return {value: userData.campaign_name, display: userData.campaign_name, Selected : false}
					}); 
					
						const tempArray = [];
						camaigndetails.forEach(element => {
							
							if(element.value === this.props.selectUser){
								tempArray.push({value: element.value, display: element.display,Selected: true})
							}else{
								tempArray.push({value: element.value, display: element.display,Selected: false})
							}
						});
						
						this.setState({
							campaignList: [{value: '', display: 'Select Campaign',Selected: true}].concat(tempArray)
							});
							
					}
					
				  )

			}

			getCourseDeatils(){
			const apiUrl1 = apiConfig.API_Path +'ManageLead/getCourseDeatils';
		
			fetch(apiUrl1)
				.then(res => res.json() )
				.then(response => {
				let courseList = response.map(courseData => {
					return {value: courseData.course_name, display: courseData.course_name, Selected : false}
				}); 
				
					const tempArray = [];
					courseList.forEach(element => {
						
						if(element.value === this.props.selectcourse){
							tempArray.push({value: element.value, display: element.display,Selected: true})
						}else{
							tempArray.push({value: element.value, display: element.display,Selected: false})
						}
					});
					
					this.setState({
						courseList: [{value: '', display: 'Select Course',Selected: true}].concat(tempArray)
						});
				}
				
				)
				
			}

			getSourceDeatils(){
			const apiUrl = apiConfig.API_Path +'ManageLead/getSourceDeatils';
		
			fetch(apiUrl)
				.then(res => res.json() )
				.then(response => {
				let sourceList = response.map(courseData => {
					return {value: courseData.source_name, display: courseData.source_name, Selected : false}
				}); 
				
					const tempArray = [];
					sourceList.forEach(element => {
						
						if(element.value === this.props.selectcourse){
							tempArray.push({value: element.value, display: element.display,Selected: true})
						}else{
							tempArray.push({value: element.value, display: element.display,Selected: false})
						}
					});
					
					this.setState({
						sourceList: [{value: '', display: 'Select Source',Selected: true}].concat(tempArray)
						});
					
					
				}
				
				)
				
			}

			uploadExcel = e => {
			
				//const files = Array.from(e.target.files)
				var file = e.target.files[0];
					
					var fileSize = e.target.files[0].size / 1024 / 1024;
					if(fileSize > 2){
						toast.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 2MB.");
						return false;
					}
					else { 
						if (file.name.split('.').pop()==='xlsx' || file.name.split('.').pop()==='xls') {
							this.setState({ 
								leadExcel: e.target.files[0]})
						} else {
							toast.error('Please upload correct File , File extension should be (.xls|.xlsx)');
						   // $scope.ShowSpinnerStatus = false;
							return false;
						}
					}
					console.log(e.target.files[0])
				// this.setState({ uploading: true,
				// 	userPhoto : e.target.files[0]})
				
			};
			
			onChangeselectUser(e) {
				this.setState({
					selectUser :  e.target.value
				});
			}
			onChangeselectCampaign(e){
				this.setState({
					selectCampaign : e.target.value
				});
			}
			onChangeselectcourse(e) {
				this.setState({
					selectcourse :  e.target.value
				});
			}
			onChangeselectsource(e) {
				this.setState({
					selectsource :  e.target.value
				});
			}
			onChangestudentName(e) {
				this.setState({
					studentName :  e.target.value
				});
			}
			onChangeemailId(e) {
				this.setState({
					emailId :  e.target.value
				});
			}
			onChangemobileNumber(e) {
				this.setState({
					mobileNumber :  e.target.value
				});
				
			}
			onChangereceivedDate(e) {
				this.setState({
					receivedDate :  e.target.value
				});
				
			}
			onChangecampaignName(e){
				this.setState({
					campaignName : e.target.value
				})
			}
			
			onChangealterMobileNumber(e) {
				this.setState({
					alterMobileNumber :  e.target.value
				});
			}
			
			onUploadExcel(e){
				e.preventDefault();
				if(this.state.leadExcel){
					if(this.state.selectUser){
						if(this.state.selectCampaign){

							const formData = new FormData();
							formData.append("leadExcelFile", this.state.leadExcel);
							formData.append("selectUser", this.state.selectUser);
							formData.append("selectCampaign", this.state.selectCampaign);
							axios.post(apiConfig.API_Path +'ManageLead/uploadExcel',formData,{
								// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
								headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
							})
							.then(res => 
								{
								if(res.data.status.status === '1'){
									toast.success(res.data.status.message)
									this.setState(
										{ 
											leadsId : '',
											selectUser : '',
											studentName : '',
											emailId : '',
											mobileNumber : '', campaignName : '',
											receivedDate : '',
											alterMobileNumber:'', 
											hideDiv : false
						
										})
										
										window.location.reload(false);
									} else{
										toast.error(res.data.status.message)
										this.setState(
										{ 
											selectUser : '',
											studentName : '',
											emailId : '',
											mobileNumber : '', campaignName : '',
											receivedDate : '',
											alterMobileNumber:'', 
											leadsId : '',
										})
									} 
									}
									);
						}else{
							this.refs.selectCampaign.focus();
							toast.error("Select campaign is required.")
							}
					}else{
					this.refs.selectUser.focus();
					toast.error("assign lead is required.")
					}
				}else{
					toast.error("upload lead excel is required.")
					}
			}
			onSubmit(e){
			e.preventDefault();
			
			if(this.state.studentName  || this.props.studentName){
				if(this.state.studentName){
					if (!(this.state.studentName.match(/^([a-zA-Z-',.\s]{1,350})$/))) {
						this.refs.studentName.focus();
						toast.error("Valid student name is required.")
						return false;
					}
				}
				if(this.props.studentName){
					if (!(this.props.studentName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.studentName.focus();
						toast.error("Valid student name is required.")
						return false;
					}
				}
	
				if(this.state.emailId || this.props.emailId){
					if(this.state.emailId){
						if (!(this.state.emailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/))) {
							this.refs.emailId.focus();
							toast.error("Valid email id is required.")
							return false;
						}
					}
					if(this.props.emailId){
						if (!(this.props.emailId.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/))) {
							this.refs.emailId.focus();
							toast.error("Valid email id is required.")
							return false;
						}
					}
					if(this.state.mobileNumber || this.props.mobileNumber){
						if(this.state.mobileNumber){
							if (!(this.state.mobileNumber.match(/^[7-9][0-9]{9}$/))) {
								this.refs.mobileNumber.focus();
								toast.error("Valid mobile number is required.")
								return false;
							}
						}
						if(this.props.mobileNumber){
							if (!(this.props.mobileNumber.match(/^[7-9][0-9]{9}$/))) {
								this.refs.mobileNumber.focus();
								toast.error("Valid mobile number is required.")
								return false;
							}
						}
						if(this.state.alterMobileNumber || this.props.alterMobileNumber){
							if(this.state.alterMobileNumber ){
								if (!(this.state.alterMobileNumber.match(/^[7-9][0-9]{9}$/))) {
									this.refs.alterMobileNumber.focus();
									toast.error("Valid mobile number is required.")
									return false;
								}
							}
							if(this.props.alterMobileNumber ){
								if (!(this.props.alterMobileNumber.match(/^[7-9][0-9]{9}$/))) {
									this.refs.alterMobileNumber.focus();
									toast.error("Valid mobile number is required.")
									return false;
								}
							}

						}
						if(this.state.campaignName  || this.props.campaignName){
							if(this.state.receivedDate  || this.props.receivedDate){
								if(this.state.selectcourse  || this.props.selectcourse){
									if(this.state.selectsource  || this.props.selectsource){
										
										
					
												const formData = new FormData();
												
												if(this.state.receivedDate){
													formData.append("receivedDate", this.state.receivedDate);
												}else{
													formData.append("receivedDate", this.props.receivedDate);
												}
												if(this.state.studentName){
													formData.append("studentName", this.state.studentName);
												}else{
													formData.append("studentName", this.props.studentName);
												}
		
												//admissionType
												if(this.state.emailId){
													formData.append("emailId", this.state.emailId);
												}else{
													formData.append("emailId", this.props.emailId);
												}
		
												//admissionDuration
												if(this.state.mobileNumber){
													formData.append("mobileNumber", this.state.mobileNumber);
												}else{
													formData.append("mobileNumber", this.props.mobileNumber);
												}
		
												//startDate
												
												if(this.state.alterMobileNumber ){
													formData.append("alterMobileNumber", this.state.alterMobileNumber);
												}else{
													formData.append("alterMobileNumber", this.props.alterMobileNumber);
												}
												if((this.state.alterMobileNumber==='' || this.state.alterMobileNumber===undefined) && (this.props.alterMobileNumber==='' || this.props.alterMobileNumber===undefined)){
													formData.append("alterMobileNumber", '');
													
												}
		
												//endDate
												if(this.state.selectcourse){
													formData.append("selectcourse", this.state.selectcourse);
												}else{
													formData.append("selectcourse", this.props.selectcourse);
												}

												if(this.state.selectsource){
													formData.append("selectsource", this.state.selectsource);
												}else{
													formData.append("selectsource", this.props.selectsource);
												}

												if(this.state.campaignName){
													formData.append("campaignName", this.state.campaignName);
												}else{
													formData.append("campaignName", this.props.campaignName);
												}
												
												formData.append("leadsId", this.props.leadsId);
												
												if(this.props.leadsId){
													axios.post(apiConfig.API_Path +'ManageLead/updateDetails',formData,{
														// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
														headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
													})
													.then(res => 
														{
														if(res.data.status.status === '1'){
															toast.success(res.data.status.message)
															this.setState(
																{ 
																	leadsId : '',
																	selectUser : '',
																	studentName : '',
																	emailId : '',
																	mobileNumber : '', campaignName : '',
																	receivedDate : '',
																	alterMobileNumber:'', 
																	hideDiv : false
												
																})
																
																window.location.reload(false);
														} else{
															toast.error(res.data.status.message)
															this.setState(
															{ 
																selectUser : '',
																studentName : '',
																emailId : '',
																mobileNumber : '', campaignName : '',
																receivedDate : '',
																alterMobileNumber:'', 
																leadsId : '',
															})
														} 
														}
														);
												}else{
													
													
												axios.post(apiConfig.API_Path +'ManageLead/insertDetails',formData,{
													// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
													headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
													})
													.then(res => 
														{
														if(res.data.status.status === '1'){
															toast.success(res.data.status.message)
															this.setState(
																{ 
																	selectUser : '',
																	studentName : '',
																	emailId : '',
																	mobileNumber : '', campaignName : '',
																	receivedDate : '',
																	alterMobileNumber:'', 
																	hideDiv : false
												
																})
																
																window.location.reload(false);
														} else{
															toast.error(res.data.status.message)
															this.setState(
															{ 
																selectUser : '',
																studentName : '',
																emailId : '',
																mobileNumber : '', campaignName : '',
																receivedDate : '',
																alterMobileNumber:'', 
															})
														} 
														}
														) ;
													}
									}else{
										this.refs.selectsource.focus();
										toast.error("Select source is required.")
										}	
								}else{
									this.refs.selectcourse.focus();
									toast.error("Select course is required.")
									}	
							}else{
								this.refs.receivedDate.focus();
								toast.error("Received date is required.")
								}						
						}else{
							this.refs.campaignName.focus();
							toast.error("Campaign name is required.")
							}
								
						// }else{
						// 	this.refs.startDate.focus();
						// 	toast.error("Start date is required.")
						// 	}	
					}else{
						this.refs.mobileNumber.focus();
						toast.error("Mobile number is required.")
						}						
				}else{
					this.refs.emailId.focus();
					toast.error("Email Id is required.")
					}				
			}else{
				this.refs.studentName.focus();
				toast.error("Student name is required.")
				}
		}
	
		HideDivFunction = () =>{
			const {hideDiv} = this.state;
			console.log(hideDiv);
			this.setState({
				hideDiv : !hideDiv
				
			}
			)
		}
		
			render(){
				
				return(
					<div>
						{this.state.hideDiv?
						<div className="col-xs-12">
							<div className="add-leads">
                            <div className="col-xs-12">
									<div className="box-content card white">
										{this.props.leadsId? <h4 className="box-title">Update multiple leads </h4> : <h4 className="box-title">Add multiple leads </h4> }
                                    
                                
                                    <form action="#" method="post">
										<div className="col-lg-12" >
											<div className="col-lg-6 col-md-6"></div>
											<div className="col-lg-6 col-md-6 text-right">
												<a href="sampleExcel/LeadsSampleExcel.xlsx" download>
													<b><i className="fa fa-download"> Download Sample Excel File </i></b>
												</a>
											</div>
										</div>
                                        <div className="card-content text-center">
                                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												
                                                <div className="form-group">
												{/* <label className="control-label">Upload File</label> */}
												
                                                    <div className="preview-zone hidden">
                                                        <div className="box box-solid">
                                                            <div className="box-header with-border">
                                                                <div><b>Preview</b></div>
                                                                <div className="box-tools pull-right">
                                                                    <button type="button" className="btn btn-danger btn-xs remove-preview">
                                                                        <i className="fa fa-times"></i> Reset This Form
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div className="box-body"></div>
                                                        </div>
                                                    </div>
													
                                                    <div className="dropzone-wrapper">
                                                        <div className="dropzone-desc">
                                                            <i className="glyphicon glyphicon-download-alt"></i>
                                                            <p>Excel upload.</p>
															
                                                        </div>
														
                                                        <input type="file" name="img_logo" className="dropzone" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="exampleInputFile"  onChange={this.uploadExcel}/>
														{/* <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" name="FormFeesList"  id="exampleInputFile" onchange="angular.element(this).scope().uploadedFile(this)" required /> */}
														
                                                    </div>
													
                                                
                                                    {/* <input type="file" id="exampleInputFile"/>
                                                    <p className="help-block">(Excel upload will replace all existing leads.)</p> */}
                                                </div>
                                            </div>

                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <select id="ig-4" className="form-control" ref="selectUser" defaultValue={this.state.selectUser ||this.props.selectUser } onChange={(e)=>this.onChangeselectUser(e)}>
														{this.state.userList.map((userData) => <option key={userData.value} value={userData.value} selected={userData.Selected}>{userData.display}</option>)}
													</select>
                                      
                                                </div>
                                            </div>
											<div className="col-lg-6">
                                                <div className="form-group">
                                                    <select id="ig-4" className="form-control" ref="selectCampaign" defaultValue={this.state.selectCampaign ||this.props.selectCampaign } onChange={(e)=>this.onChangeselectCampaign(e)}>
														{this.state.campaignList.map((campaginData) => <option key={campaginData.value} value={campaginData.value} selected={campaginData.Selected}>{campaginData.display}</option>)}
													</select>
                                      
                                                </div>
                                            </div>
											
                                            <div className="col-lg-4">
                                                <div className="input-group">
                                                    <div className="col-lg-6 col-md-6">
                                                        <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light" onClick={this.onUploadExcel}>Assign & Submit</button>
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <button id="HideAddLeads" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </form>

                                    <div className="card-content text-center">
                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <button type="button" className="btn btn-info btn-circle btn-lg waves-effect waves-light">
                                                <span>OR</span> 
                                            </button>
                                        </div>
                                    </div>
                                        <div className="col-xs-12">
                                            <div className="box-content card white">
                                                {this.props.leadsId? <h4 className="box-title">Update single leads </h4> : <h4 className="box-title">Add single leads </h4> }
                                                <form action="#" method="post">
                                                        <div className="card-content">
															<div className="col-lg-6 col-md-6">
																<div className="input-group margin-bottom-20">
																	<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-user"></i></label></div>
																	<input id="ig-1" type="text" className="form-control" placeholder="Student's Name" ref="studentName" defaultValue={this.state.studentName ||this.props.studentName } onChange={(e)=>this.onChangestudentName(e)}/>
																</div>
															</div>
															<div className="col-lg-6 col-md-6">
																<div className="input-group margin-bottom-20">
																	<div className="input-group-btn"><label htmlFor="ig-2" className="btn btn-default"><i className="fa fa-envelope"></i></label></div>
																	<input id="ig-2" type="text" className="form-control" placeholder="Email address" ref="emailId" defaultValue={this.state.emailId ||this.props.emailId } onChange={(e)=>this.onChangeemailId(e)}/>
																</div>
															</div>
															<div className="col-lg-6 col-md-6">
																<div className="input-group margin-bottom-20">
																	<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-mobile"></i></label></div>
																	<input id="ig-3" type="text" className="form-control" placeholder="Mobile Number" ref="mobileNumber" defaultValue={this.state.mobileNumber ||this.props.mobileNumber } onChange={(e)=>this.onChangemobileNumber(e)} maxLength="10" />
																</div>
															</div>
															<div className="col-lg-6 col-md-6">
																<div className="input-group margin-bottom-20">
																	<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-mobile"></i></label></div>
																	<input id="ig-4" type="text" className="form-control" placeholder="Alternate Mobile Number" ref="alterMobileNumber" defaultValue={this.state.alterMobileNumber ||this.props.alterMobileNumber } onChange={(e)=>this.onChangealterMobileNumber(e)} maxLength="10" />
																</div>
															</div>
                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="input-group margin-bottom-20">
                                                                    <div className="input-group-btn"><label htmlFor="ig-11" className="btn btn-default"><i className="fa fa-user"></i></label></div>
                                                                    
                                                                    {/* <input id="ig-11" type="text" className="form-control" placeholder="Campaign" ref="campaignName" defaultValue={this.state.campaignName ||this.props.campaignName } onChange={(e)=>this.onChangecampaignName(e)}/> */}
																	<div className="form-group">
																		<select id="ig-4" className="form-control" ref="campaignName" defaultValue={this.state.campaignName ||this.props.campaignName } onChange={(e)=>this.onChangecampaignName(e)}>
																			{this.state.campaignList.map((campaginData) => <option key={campaginData.value} value={campaginData.value} selected={campaginData.Selected}>{campaginData.display}</option>)}
																		</select>
                                      
                                                					</div>
                                                                </div>
                                                                
                                                            </div>			
                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="input-group margin-bottom-20">
                                                                    <div className="input-group-btn"><label htmlFor="ig-8" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
                                                                
                                                                    <input id="ig-8" type="date" className="form-control" placeholder="Received Date" ref="receivedDate" defaultValue={this.state.receivedDate ||this.props.receivedDate } onChange={(e)=>this.onChangereceivedDate(e)}/>
                                                                </div>
                                                            
                                                            </div>
                                                            <div className="col-lg-6 col-md-6">
																<div className="input-group margin-bottom-20">
																	<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-user"></i></label></div>
																	<select id="ig-4" className="form-control" ref="selectcourse" defaultValue={this.state.selectcourse ||this.props.selectcourse } onChange={(e)=>this.onChangeselectcourse(e)}>
																		{this.state.courseList.map((courseData) => <option key={courseData.value} value={courseData.value} selected={courseData.Selected}>{courseData.display}</option>)}
																	</select>
																</div>
															</div>
                                                            <div className="col-lg-6 col-md-6">
																<div className="input-group margin-bottom-20">
																	<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-user"></i></label></div>
																	{/* <select id="ig-4" className="form-control" ref="selectadmission" defaultValue={this.state.selectadmission ||this.props.selectadmission } onChange={(e)=>this.onChangeselectadmission(e)}> */}
																	<select id="ig-4" className="form-control" ref="selectsource" defaultValue={this.state.selectsource ||this.props.selectsource } onChange={(e)=>this.onChangeselectsource(e)}>
																		{this.state.sourceList.map((sourceData) => <option key={sourceData.value} value={sourceData.value} selected={sourceData.Selected}>{sourceData.display}</option>)}
																		{/* <option value=''>Select Course</option>
																		{this.state.courseList.map((courseData) => <option key={courseData.course_id} value={courseData.course_id }  >{courseData.course_name}</option>)} */}
								
																	</select>
																</div>
															</div>


                                                            <div className="input-group pull">
                                                                <div className="col-lg-6 col-md-6">
                                                                {this.props.leadId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
                                                                    
                                                                </div>
                                                                <div className="col-lg-6 col-md-6">
                                                                    <button id="hide-add-role" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
                                                                </div>
                                                            </div>
                                                        
                                                        </div>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
						</div>: null}
					</div>
				
				)
			}
		}
	
		
	
	


	export default ManageLead;