import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');

    const IconColor = {
        color:'blue',
      
    }
    const IconSize = {
		width: '170px',
        height : '80px',
        
    }
    class AdminDashboard extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            addLeads :false
        }
        // this.hideShowDiv() = this.hideShowDiv.bind(this)
        this.logout = this.logout.bind(this);
    }
    ShowDivfunction = () =>{
        const {addLeads} = this.state;
        this.setState({
            addLeads : !addLeads
        }
        )
    }
    logout(){
        localStorage.setItem("userLoggedIn", "False");
        localStorage.setItem("user_id", "");
        localStorage.setItem("user_role", "");
        localStorage.clear();
        setTimeout(() => {
            window.location = "Login";
        },1000)
    }

    componentDidMount() {
        $(".commonRemove").removeClass('current');
        $("#AdminDashboard").addClass('current');
       
    }
    render() {
      return (
            <div>
                <header id="header">
		            <div className="main-menu">
			            <div className="content">
                            <a href="AdminDashboard" className="logo">
                                <img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
                            </a>
                            <Navigation />
			            </div>
			        </div>
	

                    <div className="fixed-navbar ">
                        <div className="pull-left">
                            <button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
                            <h1 className="page-title"> Call Management System - Admin Dashboard</h1>
                        
                        </div>
			
                        <div className="pull-right">
                                {/* <div className="ico-item">
                                    <a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
                                    <form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit"></button></form>
                                    
                                </div> */}
				
                        <div className="ico-item">
                        <i className="fa fa-user" style={IconColor}></i>
                            <ul className="sub-ico-item">
                                
                                <li><a onClick= {this.logout} >Log Out</a></li>
                                <li><a href="ChangePassword">Change Password</a></li>
                            </ul>
                            
                        </div>
                </div>
                    </div>
                </header>
                <div id="wrapper">
                    <div className="main-content">
			
			
                        <div className="row small-spacing">
                        <div className="reports">
				<div className="col-lg-6 col-md-6 col-xs-12">
					<div className="box-content card white">
						<h4 className="box-title">Date Wise – No. of Calls</h4>
						<div id="bar-morris-chart" className="morris-chart" ></div>
						<div className="text-center">
							<ul className="list-inline morris-chart-detail-list">
								<li><i className="fa fa-circle"></i>Date wise number of Calls</li>
								{/* <!-- <li><i className="fa fa-circle"></i>Series B</li>
									<li><i className="fa fa-circle"></i>Series C</li> --> */}
								</ul>
							</div>
						</div>
					</div>
					<div className="col-lg-6 col-md-6 col-xs-12">
						<div className="box-content card white">
							<h4 className="box-title">Caller Wise</h4>
							<div id="pile-flot-chart" className="flot-chart" ></div>	
						</div>
					</div>
				</div>
			</div>

		<div className="row small-spacing">
			<div className="reports">
				<div className="col-lg-6 col-md-6 col-xs-12">
					<div className="box-content card white">
						<h4 className="box-title">Date Wise – No. of Calls</h4>
						<div id="bar-morris-chart" className="morris-chart" ></div>
						<div className="text-center">
							<ul className="list-inline morris-chart-detail-list">
								<li><i className="fa fa-circle"></i>Date wise number of Calls</li>
								{/* <li><i className="fa fa-circle"></i>Series B</li>
									<li><i className="fa fa-circle"></i>Series C</li> */}
								</ul>
							</div>
						</div>
					</div>
					<div className="col-lg-6 col-md-6 col-xs-12">
						<div className="box-content card white">
							<h4 className="box-title">Caller Wise</h4>
							<div id="pile-flot-chart" className="flot-chart" ></div>	
						</div>
					</div>
				</div>
			</div>

			<div className="row small-spacing">
				<div className="reports">
					<div className="col-lg-6 col-md-6 col-xs-12">
						<div className="box-content card white">
							<h4 className="box-title">Response Wise</h4>
						
							<div id="donut-morris-chart" className="morris-chart" ></div>
							<div className="text-center">
								<ul className="list-inline morris-chart-detail-list">
									<li><i className="fa fa-circle"></i>Very Positive</li>
									<li><i className="fa fa-circle"></i>Positive</li>
									<li><i className="fa fa-circle"></i>Negative</li>
									<li><i className="fa fa-circle"></i>No Answer</li>
									<li><i className="fa fa-circle"></i>Not Reachable</li>
								</ul>
							</div>
							
						</div>
					</div>

					<div className="col-lg-6 col-md-6 col-xs-12">
						<div className="box-content card white">
							<h4 className="box-title">Course Wise Response</h4>
							
							<div id="donut-flot-chart" className="flot-chart margin-bottom-20 margin-top-20" ></div>
							
						</div>
					</div>

				</div>
                            {this.state.addLeads? <ShowDiv /> :null}
                            
                        </div>
                            <div className="col-xs-12">
                                <div className="box-content bordered primary js__card AddLeadsCard">
                                    <div className="col-lg-4 col-md-4">
                                        <h4 className="box-title pull-left with-control">All Leads</h4>
                                    </div>
                                    <div className="col-lg-4 col-md-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <input type="text" name="inputSearch"placeholder="Search..." className="form-control"/>
                                                <span className="input-group-addon"><i className="fa fa-search"></i></span>
                                            </div>
                                            <div className="input-group-btn"> </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4">
                                        <div className="pull-right">
                                            <a id="AddLeadsBtn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus"></i> Add Leads</a>
                                            

                                            <a id="assign-multiple-btn" className="btn btn-success btn-sm"><i className="fa fa-plus"></i> Assign Multiple</a>
                                        
                                        </div>
                                    </div>
                                    <div className="clearfix">&nbsp;</div>

                                    
                                
                                    <div className="table table-responsive">
                                        <table id="leads-list" className="table table-striped table-bordered display assignTo" >
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile No.</th>
                                                    <th>Alternate No.</th>
                                                    <th>Course</th>
                                                    <th>Source</th>
                                                    <th>Received Date</th>
                                                    <th>Assign To</th>
                                                </tr>
                                            </thead>
                                            {/* <tfoot>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile No.</th>
                                                    <th>Alternate No.</th>
                                                    <th>Course</th>
                                                    <th>Source</th>
                                                    <th>Received Date</th>
                                                    <th>Assign To</th>
                                                </tr>
                                            </tfoot> */}
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <form>
                                                            <div className="checkbox">
                                                                <input type="checkbox" id="chk-1"/><label htmlFor="chk-1"></label> 
                                                            </div>
                                                        </form>
                                                    </td>
                                                    <td>Shridhar Honmore</td>
                                                    <td>shridhar@gmail.com</td>
                                                    <td>9922554478</td>
                                                    <td>-</td>
                                                    <td>GEE</td>
                                                    <td>Facebook</td>
                                                    <td>10-02-2020</td>
                                                    <td>
                                                        <form>
                                                            <div className="form-group">
                                                                <select className="form-control" id="sel1">
                                                                    <option>Distribute Equally</option>
                                                                    <option>Distribute Randomly</option>
                                                                    <option>Master Admin</option>
                                                                    <option>Faculty</option>
                                                                    <option>Senior Counsellor</option>
                                                                    <option>Telecaller</option>
                                                                </select>
                                                            </div>	
                                                            <a className="btn btn-info btn-assign" href="#">Assign</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <form>
                                                            <div className="checkbox">
                                                                <input type="checkbox" id="chk-2"/><label htmlFor="chk-2"></label> 
                                                            </div>
                                                        </form>
                                                    </td>
                                                    <td>Shridhar Honmore</td>
                                                    <td>shridhar@gmail.com</td>
                                                    <td>9922554478</td>
                                                    <td>-</td>
                                                    <td>GEE</td>
                                                    <td>Facebook</td>
                                                    <td>10-02-2020</td>
                                                    <td>
                                                        <form>
                                                            <div className="form-group">
                                                                <select className="form-control" id="sel1">
                                                                    <option>Distribute Equally</option>
                                                                    <option>Distribute Randomly</option>
                                                                    <option>Amol</option>
                                                                    <option>Shridhar</option>
                                                                    <option>Pratik</option>
                                                                    <option>Shruti</option>
                                                                </select>
                                                            </div>	
                                                            <a className="btn btn-info btn-assign" href="#">Assign</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <form>
                                                            <div className="checkbox">
                                                                <input type="checkbox" id="chk-3"/><label htmlFor="chk-3"></label> 
                                                            </div>
                                                        </form>
                                                    </td>
                                                    <td>Shridhar Honmore</td>
                                                    <td>shridhar@gmail.com</td>
                                                    <td>9922554478</td>
                                                    <td>-</td>
                                                    <td>GEE</td>
                                                    <td>Facebook</td>
                                                    <td>10-02-2020</td>
                                                    <td>
                                                        <form>
                                                            <div className="form-group">
                                                                <select className="form-control" id="sel1">
                                                                    <option>Distribute Equally</option>
                                                                    <option>Distribute Randomly</option>
                                                                    <option>Amol</option>
                                                                    <option>Shridhar</option>
                                                                    <option>Pratik</option>
                                                                    <option>Shruti</option>
                                                                </select>
                                                            </div>	
                                                            <a className="btn btn-info btn-assign" href="#">Assign</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <form>
                                                            <div className="checkbox">
                                                                <input type="checkbox" id="chk-4"/><label htmlFor="chk-4"></label> 
                                                            </div>
                                                        </form>
                                                    </td>
                                                    <td>Shridhar Honmore</td>
                                                    <td>shridhar@gmail.com</td>
                                                    <td>9922554478</td>
                                                    <td>-</td>
                                                    <td>GEE</td>
                                                    <td>Facebook</td>
                                                    <td>10-02-2020</td>
                                                    <td>
                                                        <form>
                                                            <div className="form-group">
                                                                <select className="form-control" id="sel1">
                                                                    <option>Distribute Equally</option>
                                                                    <option>Distribute Randomly</option>
                                                                    <option>Amol</option>
                                                                    <option>Shridhar</option>
                                                                    <option>Pratik</option>
                                                                    <option>Shruti</option>
                                                                </select>
                                                            </div>	
                                                            <a className="btn btn-info btn-assign" href="#">Assign</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <form>
                                                            <div className="checkbox">
                                                                <input type="checkbox" id="chk-5"/><label htmlFor="chk-5"></label> 
                                                            </div>
                                                        </form>
                                                    </td>
                                                    <td>Shridhar Honmore</td>
                                                    <td>shridhar@gmail.com</td>
                                                    <td>9922554478</td>
                                                    <td>-</td>
                                                    <td>GEE</td>
                                                    <td>Facebook</td>
                                                    <td>10-02-2020</td>
                                                    <td>
                                                        <form>
                                                            <div className="form-group">
                                                                <select className="form-control" id="sel1">
                                                                    <option>Distribute Equally</option>
                                                                    <option>Distribute Randomly</option>
                                                                    <option>Amol</option>
                                                                    <option>Shridhar</option>
                                                                    <option>Pratik</option>
                                                                    <option>Shruti</option>
                                                                </select>
                                                            </div>	
                                                            <a className="btn btn-info btn-assign" href="#">Assign</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <form>
                                                            <div className="checkbox">
                                                                <input type="checkbox" id="chk-6"/><label htmlFor="chk-6"></label> 
                                                            </div>
                                                        </form>
                                                    </td>
                                                    <td>Shridhar Honmore</td>
                                                    <td>shridhar@gmail.com</td>
                                                    <td>9922554478</td>
                                                    <td>-</td>
                                                    <td>GEE</td>
                                                    <td>Facebook</td>
                                                    <td>10-02-2020</td>
                                                    <td>
                                                        <form>
                                                            <div className="form-group">
                                                                <select className="form-control" id="sel1">
                                                                    <option>Distribute Equally</option>
                                                                    <option>Distribute Randomly</option>
                                                                    <option>Amol</option>
                                                                    <option>Shridhar</option>
                                                                    <option>Pratik</option>
                                                                    <option>Shruti</option>
                                                                </select>
                                                            </div>	
                                                            <a className="btn btn-info btn-assign" href="#">Assign</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <form>
                                                            <div className="checkbox">
                                                                <input type="checkbox" id="chk-7"/><label htmlFor="chk-7"></label> 
                                                            </div>
                                                        </form>
                                                    </td>
                                                    <td>Shridhar Honmore</td>
                                                    <td>shridhar@gmail.com</td>
                                                    <td>9922554478</td>
                                                    <td>-</td>
                                                    <td>GEE</td>
                                                    <td>Facebook</td>
                                                    <td>10-02-2020</td>
                                                    <td>
                                                        <form>
                                                            <div className="form-group">
                                                                <select className="form-control" id="sel1">
                                                                    <option>Distribute Equally</option>
                                                                    <option>Distribute Randomly</option>
                                                                    <option>Amol</option>
                                                                    <option>Shridhar</option>
                                                                    <option>Pratik</option>
                                                                    <option>Shruti</option>
                                                                </select>
                                                            </div>	
                                                            <a className="btn btn-info btn-assign" href="#">Assign</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                
                                </div>

                            </div>
                            <div className="col-xs-12">
                                <div className="box-content bordered info js__card">
                                    <h4 className="box-title">User List</h4>
                                    <div className="col-lg-12">
                                        <div className="col-lg-4 pull-right">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <input type="text" name="inputSearch"placeholder="Search..." className="form-control"/>
                                                    <span className="input-group-addon"><i className="fa fa-search"></i></span>
                                                </div>
                                                <div className="input-group-btn"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div className="table table-responsive">
                                        <table id="user-list" className="table table-striped table-bordered display">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No.</th>
                                                    <th>User Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile Number</th>
                                                    <th>User Role</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            {/* <tfoot>
                                                <tr>
                                                    <th>Sr. No.</th>
                                                    <th>User Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile Number</th>
                                                    <th>User Role</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot> */}
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Shreekant</td>
                                                    <td>shreekant@whitecode.co.in</td>
                                                    <td>9766453956</td>
                                                    <td>Master Admin</td>
                                                    <td>
                                                        <button type="button" className="btn btn-warning btn-circle btn-lg waves-effect waves-light" data-toggle="modal" data-target="#editUser" title="Edit User Details">
                                                            <i className="ico fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" className="btn btn-danger btn-circle btn-lg waves-effect waves-light" title="Delete User">
                                                            <i className="ico fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Akshay Bhingarkar</td>
                                                    <td>akshayb@gmail.com</td>
                                                    <td>9921869522</td>
                                                    <td>Faculty</td>
                                                    <td>
                                                        <button type="button" className="btn btn-warning btn-circle btn-lg waves-effect waves-light" data-toggle="modal" data-target="#editUser" title="Edit User Details">
                                                            <i className="ico fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" className="btn btn-danger btn-circle btn-lg waves-effect waves-light" title="Delete User">
                                                            <i className="ico fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Pratik Chaudhari</td>
                                                    <td>pratik@whitecode.co.in</td>
                                                    <td>9762433636</td>
                                                    <td>Senior Counsellor</td>
                                                    <td>
                                                        <button type="button" className="btn btn-warning btn-circle btn-lg waves-effect waves-light" data-toggle="modal" data-target="#editUser" title="Edit User Details">
                                                            <i className="ico fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" className="btn btn-danger btn-circle btn-lg waves-effect waves-light" title="Delete User">
                                                            <i className="ico fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Amit Patil</td>
                                                    <td>amit@gmail.com</td>
                                                    <td>9921869522</td>
                                                    <td>Telecaller</td>
                                                    <td>
                                                        <button type="button" className="btn btn-warning btn-circle btn-lg waves-effect waves-light" data-toggle="modal" data-target="#editUser" title="Edit User Details">
                                                            <i className="ico fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" className="btn btn-danger btn-circle btn-lg waves-effect waves-light" title="Delete User">
                                                            <i className="ico fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>Sakita </td>
                                                    <td>sankita@gmail.com</td>
                                                    <td>9762433636</td>
                                                    <td>Telecaller</td>
                                                    <td>
                                                        <button type="button" className="btn btn-warning btn-circle btn-lg waves-effect waves-light" data-toggle="modal" data-target="#editUser" title="Edit User Details">
                                                            <i className="ico fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" className="btn btn-danger btn-circle btn-lg waves-effect waves-light" title="Delete User">
                                                            <i className="ico fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                            </div>
                            <footer id="footer" className="footer">
                                <ul className="list-inline">
                                    <li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Coockies</a></li>
                                    <li><a href="#">Legal Notice</a></li>
                                </ul>
                            </footer>
                        </div>
                    </div>
                </div>
      );
    }
    }

    class ShowDiv extends Component{
        constructor(props){
            super(props)
            this.state = {
                hideDiv :true
            }
        
        }
        HideDivFunction = () =>{
            const {hideDiv} = this.state;
            console.log(hideDiv);
            this.setState({
                hideDiv : !hideDiv
                
            }
            )
        
        }
        render(){
            return(
                <div>
                    {this.state.hideDiv?
                    <div className="col-xs-12">
                        <div className="box-content card white">
                            <h4 className="box-title">Add Leads</h4>
                            <form className="AddLeads" action="#" method="post">
                                <div className="card-content">
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label className="control-label">Upload File</label>
                                            <div className="preview-zone hidden">
                                                <div className="box box-solid">
                                                    <div className="box-header with-border">
                                                        <div><b>Preview</b></div>
                                                        <div className="box-tools pull-right">
                                                            <button type="button" className="btn btn-danger btn-xs remove-preview">
                                                                <i className="fa fa-times"></i> Reset This Form
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="box-body"></div>
                                                </div>
                                            </div>
                                            <div className="dropzone-wrapper">
                                                <div className="dropzone-desc">
                                                    <i className="glyphicon glyphicon-download-alt"></i>
                                                    <p>Choose an image file or drag it here.</p>
                                                </div>
                                                <input type="file" name="img_logo" className="dropzone"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div className="card-content text-center">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button type="button" className="btn btn-info btn-circle btn-lg waves-effect waves-light">
                                        <span>OR</span> 
                                    </button>
                                </div>
                            </div>
                        
                            <form action="#" method="post">
                                <div className="card-content text-center">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div className="form-group">
                                            <input type="file" id="exampleInputFile"/>
                                            <p className="help-block">(Excel upload will replace all existing leads.)</p>
                                        </div>
                                    </div>

                                    <div className="col-lg-8">
                                        <div className="form-group">
                                            <select className="form-control" id="sel1">
                                                <option>Distribute Equally</option>
                                                <option>Distribute Randomly</option>
                                                <option>Amol</option>
                                                <option>Shridhar</option>
                                                <option>Pratik</option>
                                                <option>Shruti</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="input-group">
                                            <div className="col-lg-6 col-md-6">
                                                <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light">Assign & Submit</button>
                                            </div>
                                            <div className="col-lg-6 col-md-6">
                                                <button id="HideAddLeads" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </form>

                            <div className="card-content text-center">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button type="button" className="btn btn-info btn-circle btn-lg waves-effect waves-light">
                                        <span>OR</span> 
                                    </button>
                                </div>
                            </div>
                        
                            <div className="row small-spacing">
                                <div className="add-user">
                                    <div className="col-xs-12">
                                        <div className="box-content card white">
                                            <h4 className="box-title">Add Single Lead</h4>
                                        
                                            <form action="#" method="post">
                                                <div className="card-content">
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-user"></i></label></div>
                                                            
                                                            <input id="ig-1" type="text" className="form-control" placeholder="Username"/>
                                                        </div>
                                                        
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-2" className="btn btn-default"><i className="fa fa-envelope"></i></label></div>
                                                        
                                                            <input id="ig-2" type="email" className="form-control" placeholder="Email address"/>
                                                        </div>
                                                    
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-mobile"></i></label></div>
                                                            
                                                            <input id="ig-3" type="text" className="form-control" placeholder="Mobile Number"/>
                                                        </div>
                                                    
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-mobile"></i></label></div>
                                                            
                                                            <input id="ig-4" type="text" className="form-control" placeholder="Alternate Mobile Number"/>
                                                        </div>
                                                    
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-11" className="btn btn-default"><i className="fa fa-setting"></i></label></div>
                                                            
                                                            <input id="ig-11" type="text" className="form-control" placeholder="Campaign"/>
                                                        </div>
                                                        
                                                    </div>			
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-8" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
                                                        
                                                            <input id="ig-8" type="date" className="form-control" placeholder="Received Date"/>
                                                        </div>
                                                    
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-user"></i></label></div>
                                                            <select id="ig-4" className="form-control">
                                                                <option>Select Course</option>
                                                                <option value="MPSC">MPSC</option>
                                                                <option value="UPSC">UPSC</option>
                                                                <option value="JEE">JEE</option>
                                                                <option value="AICET">AICET</option>
                                                            </select>
                                                        </div>
                                                    
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="input-group margin-bottom-20">
                                                            <div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-user"></i></label></div>
                                                            <select id="ig-4" className="form-control">
                                                                <option>Select Source</option>
                                                                <option value="Friend">Friend</option>
                                                                <option value="Faculty">Faculty</option>
                                                                <option value="Facebook">Facebook</option>
                                                                <option value="News Paper">News Paper</option>
                                                            </select>
                                                        </div>
                                                        
                                                    </div>


                                                    <div className="input-group">
                                                        <div className="col-lg-6 col-md-6">
                                                            <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light">Create User</button>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6">
                                                            <button id="hide-add-user" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </form>
                                        
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                    </div>: null}
                </div>
            
            )
        }
    }

    export default AdminDashboard;