import React, { Component } from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import {toast} from 'react-toastify';
import ReactPaginate from 'react-paginate';
import Navigation from './Navigation';
import $ from 'jquery';
const apiConfig = require('../../api_config/apiConfig.json');

	const IconColor = {
		color:'blue',
		
	}
	const IconSize = {
		width: '170px',
        height : '80px',
        
    }

	class ManageCourse extends React.Component {
		constructor(props){
			super(props)
			this.state = {
				offset: 0,
				perPage: 10,
				currentPage: 0,
				addcourse :false,
				editcourse :false,
				courseList: [],
				courseId : '',
				courseName : '',
				courseType : '',
				courseDuration : '',
				startDate : '',
				endDate : '',
				subjectName: '',
				courseFees : '',
				courseDescription : ''
			}
		
			this.logout = this.logout.bind(this);
			this.handlePageClick = this.handlePageClick.bind(this);
		}
		ShowDivfunction = () =>{
			const {addcourse} = this.state;
			this.setState({
				addcourse : !addcourse
			}
			)
		}
		
		ShowEditDivFunction = (courseData) =>{
			const {editcourse} = this.state;
			this.setState({
				editcourse : !editcourse,
				courseId : courseData.course_id,
				courseName : courseData.course_name,
				courseType : courseData.course_type,
				courseDuration : courseData.course_duration,
				startDate : courseData.start_date,
				endDate :courseData.end_date,
				subjectName: courseData.subject_name,
				courseFees : courseData.course_fees,
				courseDescription : courseData.course_description
			}
			)
			console.log(courseData)
		}

		logout(){
			localStorage.setItem("userLoggedIn", "False");
			localStorage.setItem("user_id", "");
			localStorage.setItem("user_role", "");
			localStorage.clear();
			setTimeout(() => {
				window.location = "Login";
			},1000)
		}
		handlePageClick = (e) => {
			const selectedPage = e.selected;
			const offset = selectedPage * this.state.perPage;
	  
			this.setState({
				currentPage: selectedPage,
				offset: offset
			}, () => {
				this.getDetails()
			});
	  
		};

		getDetails() {
			const apiUrl = apiConfig.API_Path +'ManageCourse/getDetails';
		
			fetch(apiUrl)
			  .then(res => res.json() )
			  .then(
				(result) => {
					// this.setState({
					// 	courseList: result
					// });
					const courseList = result;
					const slice = courseList.slice(this.state.offset, this.state.offset + this.state.perPage)
					this.setState({
					    pageCount: Math.ceil(courseList.length / this.state.perPage),
						courseList : slice
					})
				}
				
			  )
			  
			  
		  }
		  
		  getAccessPermissions(){
			var role = localStorage.getItem('role_name');
			if(role=== 'Master Admin'){
				localStorage.setItem("editPermission",'true');
				localStorage.setItem('deletePermission','true');
				localStorage.setItem('viewPermission','true');
			}else{
				const apiUrl = apiConfig.API_Path +'ManageCourse/getAccessPermissions';
				const formData = new FormData();
				formData.append('userId', localStorage.getItem('user_role'));
				formData.append('ManageUser', "Manage Course");
				const options = {
					method: 'POST',
					body: formData
				}
				fetch(apiUrl,options)
				.then(res => res.json() )
				.then(
					(result) => {
						
						localStorage.setItem("editPermission",result[0]['edit_permission']);
						localStorage.setItem('deletePermission',result[0]['delete_permission']);
						localStorage.setItem('viewPermission',result[0]['view_permission']);
						console.log(result)
					}
					
				)
			}
		}
		componentDidMount() {
		$(".commonRemove").removeClass('current');
		$("#ManageCourse").addClass('current');
		this.getDetails();
		this.getAccessPermissions();
		}
		  deletecourse(courseId) {
			const { courses } = this.state;
		
			const formData = new FormData();
			formData.append('courseId', courseId);
		
			const options = {
			  method: 'POST',
			  body: formData
			}
		
			fetch(apiConfig.API_Path +'ManageCourse/deleteDetails', options)
			
			.then(res => res.json())
			.then(
				(result) => {
					toast.success(result.message)
					this.getDetails();
				},
				(error) => {
					toast.error("Opps! Something went Wrong.")
				}
			)
			}
		
			
			onChangeSearch(e) {
				const formData = new FormData();
				formData.append('searchData', e.target.value);
			
				const options = {
				  method: 'POST',
				  body: formData
				}
				fetch(apiConfig.API_Path +'ManageCourse/onChangeSearch',options)
				  .then(res => res.json() )
				  .then(
					(result) => {
						this.setState({
							courseList: result
						});
					}
					// ,
					// (error) => {
					//   this.setState({ error });
					// }
				  )
				  
				  
			  }
		
		render() {
			if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
				window.location = "login";
			}else{
				const { courseList} = this.state;
				return (
					<div>
						<header id="header">
							<div className="main-menu">
								<div className="content">
									<a href="AdminDashboard" className="logo">
										<img src={logo} alt="Chanakya Mandal Pariwar" title="Chanakya Mandal Pariwar" style={IconSize}/>
									</a>
									<Navigation />
								</div>
							</div>
			
		
							<div className="fixed-navbar ">
								<div className="pull-left">
									<button type="button" className="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
									<h1 className="page-title"> Call Management System - Manage Course</h1>
								
								</div>
					
								<div className="pull-right">
										{/* <div className="ico-item">
											<a href="#" className="ico-item ti-search js__toggle_open" data-target="#searchform-header"></a>
											<form action="#" id="searchform-header" className="searchform js__toggle"><input type="search" placeholder="Search..." className="input-search"/><button className="ti-search button-search" type="submit" ></button></form>
											
										</div> */}
						
								<div className="ico-item">
									<i className="fa fa-user" style={IconColor}></i>
									<ul className="sub-ico-item">
										<li><a onClick= {this.logout} >Log Out</a></li>
										<li><a href="ChangePassword">Change Password</a></li>
									</ul>
									
								</div>
						</div>
							</div>
						</header>
				<div id="wrapper">
					<div className="main-content">
		
					<div className="row small-spacing">
						{this.state.addcourse? <ShowDiv /> :null}
		
						{this.state.editcourse? <ShowDiv 
							courseId = {this.state.courseId} 
							courseName = {this.state.courseName}
							courseType = {this.state.courseType}
							courseDuration  = {this.state.courseDuration}
							startDate = {this.state.startDate}
							endDate = {this.state.endDate}
							subjectName = {this.state.subjectName}
							courseFees  = {this.state.courseFees}
							courseDescription  = {this.state.courseDescription}
							
						/> :null}
					</div>
					<div className="col-xs-12">
							<div className="box-content bordered primary js__card">
								<div className="col-lg-6 col-md-6">
									<h4 className="box-title pull-left">Courses List</h4>
								</div>
								<div className="col-lg-6 col-md-6">
									<div className="pull-right">
										<a id="add-course-btn" className="btn btn-primary btn-sm" onClick={()=>this.ShowDivfunction()}><i className="fa fa-plus" ></i> Add Course</a>
									</div>
								</div>
								<div className="clearfix">&nbsp;</div>
								<div className="col-lg-12">
									<div className="col-lg-4 pull-right">
										<div className="form-group">
											<div className="input-group">
												<input type="text" name="inputSearch"placeholder="Search by course name, type, description" className="form-control" onChange={(e)=>this.onChangeSearch(e)}/>
												<span className="input-group-addon"><i className="fa fa-search"></i></span>
											</div>
											<div className="input-group-btn"> </div>
										</div>
									</div>
								</div>
								<div className="table table-responsive">
									<table id="course-list" className="table table-striped table-bordered display" >
									<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Course Name</th>
												<th>Description</th>
												<th>Type</th>
												<th>Duratipn</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Subject</th>
												<th>Fees</th>
												{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? <th>Action</th> : null}
											</tr>
										</thead>
										{/* <tfoot>
											<tr>
												<th>Sr. No.</th>
												<th>course Name</th>
												<th>Action</th>
											</tr>
										</tfoot> */}
										{localStorage.getItem('viewPermission') === 'true' ?
										<tbody>
										{courseList.map((courseData,index) => (
											<tr key={courseData.course_id}>
												<React.Fragment> 
												<td>{index+1}</td>
												<td>{courseData.course_name}</td>
												<td>{courseData.course_description}</td>
												<td>{courseData.course_type}</td>
												<td>{courseData.course_duration}</td>
												<td>{courseData.start_date}</td>
												<td>{courseData.end_date}</td>
												<td>{courseData.subject_name}</td>
												<td>{courseData.course_fees}</td>
												{localStorage.getItem('editPermission') === 'true' || localStorage.getItem('deletePermission') === 'true' ? 
												<td>
													{localStorage.getItem('editPermission') === 'true' ?
													<button type="button" className="btn btn-warning btn-circle btn-sm waves-effect waves-light"  title="Edit course Details" onClick={() => this.ShowEditDivFunction(courseData)}>
														<i className="ico fa fa-edit"></i>
													</button> : null }
													{localStorage.getItem('deletePermission') === 'true' ?
													<button type="button" className="btn btn-danger btn-circle btn-sm waves-effect waves-light" title="Delete course" onClick={() => this.deletecourse(courseData.course_id)}>
														<i className="ico fa fa-trash"></i>
													</button> : null}
												</td>
												: null}
												</React.Fragment> 
											</tr>
										))}
											
										</tbody>
										:null}
									</table>
									<ReactPaginate
										previousLabel={"prev"}
										nextLabel={"next"}
										breakLabel={"..."}
										breakClassName={"break-me"}
										pageCount={this.state.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										subContainerClassName={"pages pagination"}
										activeClassName={"active"}/>
								</div>
							</div>
							
						</div>
									
							<footer id="footer" className="footer">
								<ul className="list-inline">
									<li><script type="text/JavaScript"> document.write(new Date().getFullYear()); </script> © Chanakya Mandal Pariwar. All rights reserved | Powered by <a href="https://whitecode.co.in" target="_blank">WhiteCode</a></li>
									<li><a href="#">Privacy</a></li>
									<li><a href="#">Coockies</a></li>
									<li><a href="#">Legal Notice</a></li>
								</ul>
							</footer>
						</div>
					</div>
				</div>
				);
			}
		}
		  }
	
		class ShowDiv extends Component{
			constructor(props){
				super(props)
				this.state = {
					hideDiv :true,
					courseName : '',
					courseType : '',
					courseDuration : '',
					startDate : '',
					endDate : '',
					subjectName: '',
					courseFees : '',
					courseDescription : ''
					
				}
				
				this.onChangeCourseName = this.onChangeCourseName.bind(this);
				this.onChangeCourseType = this.onChangeCourseType.bind(this);
				this.onChangeCourseDuration = this.onChangeCourseDuration.bind(this);
				this.onChangestartDate = this.onChangestartDate.bind(this);
				this.onChangeendDate = this.onChangeendDate.bind(this);
				this.onChangesubjectName = this.onChangesubjectName.bind(this);
				this.onChangecourseFees  = this.onChangecourseFees.bind(this);
				this.onChangecourseDescription = this.onChangecourseDescription.bind(this);
				this.onSubmit = this.onSubmit.bind(this);
				
			}
			onChangeCourseName(e) {
				this.setState({
					courseName :  e.target.value
				});
			}
			onChangeCourseType(e) {
				this.setState({
					courseType :  e.target.value
				});
			}
			onChangeCourseDuration(e) {
				this.setState({
					courseDuration :  e.target.value
				});
			}
			onChangestartDate(e) {
				this.setState({
					startDate :  e.target.value
				});
			}
			onChangeendDate(e) {
				this.setState({
					endDate :  e.target.value
				});
			}
			onChangesubjectName(e) {
				this.setState({
					subjectName :  e.target.value
				});
			}
			onChangecourseFees(e) {
				this.setState({
					courseFees :  e.target.value
				});
			}
			onChangecourseDescription(e) {
				this.setState({
					courseDescription :  e.target.value
				});
			}
			
			onSubmit(e){
			e.preventDefault();
			
			if(this.state.courseName  || this.props.courseName){
				if(this.state.courseName){
					if (!(this.state.courseName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.courseName.focus();
						toast.error("Valid course name is required.")
						return false;
					}
				}
				if(this.props.courseName){
					if (!(this.props.courseName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
						this.refs.courseName.focus();
						toast.error("Valid course name is required.")
						return false;
					}
				}
	
				if(this.state.courseType  || this.props.courseType){
					if(this.state.courseDuration  || this.props.courseDuration){
						if(this.state.startDate  || this.props.startDate){
							if(this.state.endDate  || this.props.endDate){
								if(this.state.startDate){
									var startDate = this.state.startDate;
								}
								if(this.props.startDate){
									var startDate = this.props.startDate;
								}
								if(this.state.endDate){
									var endDate = this.state.endDate;
								}
								if(this.props.endDate){
									var endDate = this.props.endDate;
								}
								if(Date.parse(endDate) <= Date.parse(startDate)){
	
									toast.error("Please select valid Start And End Date.");
									this.refs.startDate.focus();
									return false;
	
								}
								if(this.state.subjectName  || this.props.subjectName){
									if(this.state.subjectName){
										if (!(this.state.subjectName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
											this.refs.subjectName.focus();
											toast.error("Valid subject name is required.")
											return false;
										}
									}
									if(this.props.subjectName){
										if (!(this.props.subjectName.match(/^([a-zA-Z-',.\s]{1,30})$/))) {
											this.refs.subjectName.focus();
											toast.error("Valid subject name is required.")
											return false;
										}
									}
									if(this.state.courseFees  || this.props.courseFees){
										if(this.state.courseDescription  || this.props.courseDescription){
				
											const formData = new FormData();
											//coureName
											if(this.state.courseName){
												formData.append("courseName", this.state.courseName);
											}else{
												formData.append("courseName", this.props.courseName);
											}
	
											//courseType
											if(this.state.courseType){
												formData.append("courseType", this.state.courseType);
											}else{
												formData.append("courseType", this.props.courseType);
											}
	
											//courseDuration
											if(this.state.courseDuration){
												formData.append("courseDuration", this.state.courseDuration);
											}else{
												formData.append("courseDuration", this.props.courseDuration);
											}
	
											//startDate
											if(this.state.startDate){
												formData.append("startDate", this.state.startDate);
											}else{
												formData.append("startDate", this.props.startDate);
											}
	
											//endDate
											if(this.state.endDate){
												formData.append("endDate", this.state.endDate);
											}else{
												formData.append("endDate", this.props.endDate);
											}
											//subjectName
											if(this.state.subjectName){
												formData.append("subjectName", this.state.subjectName);
											}else{
												formData.append("subjectName", this.props.subjectName);
											}
	
											//courseFees
									
											if(this.state.courseFees){
												formData.append("courseFees", this.state.courseFees);
											}else{
												formData.append("courseFees", this.props.courseFees);
											}
											
											//courseDescription
											if(this.state.courseDescription){
												formData.append("courseDescription", this.state.courseDescription);
											}else{
												formData.append("courseDescription", this.props.courseDescription);
											}
											formData.append("courseId", this.props.courseId);
											
											if(this.props.courseId){
												axios.post(apiConfig.API_Path +'ManageCourse/updateDetails',formData,{
													// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
													headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
												})
												.then(res => 
													{
													if(res.data.status.status === '1'){
														toast.success(res.data.status.message)
														this.setState(
															{ 
																courseId : '',
																courseName : '',
																courseType : '',
																courseDuration : '',
																startDate : '',
																endDate : '',
																subjectName: '',
																courseFees : '',
																courseDescription : '',
																hideDiv : false
											
															})
															
															window.location.reload(false);
													} else{
														toast.error(res.data.status.message)
														this.setState(
														{ 
															courseName : '',
															courseType : '',
															courseDuration : '',
															startDate : '',
															endDate : '',
															subjectName: '',
															courseFees : '',
															courseDescription : '',
															courseId : '',
														})
													} 
													}
													);
											}else{
												
												
											axios.post(apiConfig.API_Path +'ManageCourse/insertDetails',formData,{
												// headers: { 'Content-Type': 'application/json,charset=UTF-8' }
												headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
												})
												.then(res => 
													{
													if(res.data.status.status === '1'){
														toast.success(res.data.status.message)
														this.setState(
															{ 
																courseName : '',
																courseType : '',
																courseDuration : '',
																startDate : '',
																endDate : '',
																subjectName: '',
																courseFees : '',
																courseDescription : '',
																hideDiv : false
											
															})
															
															window.location.reload(false);
													} else{
														toast.error(res.data.status.message)
														this.setState(
														{ 
															courseName : '',
															courseType : '',
															courseDuration : '',
															startDate : '',
															endDate : '',
															subjectName: '',
															courseFees : '',
															courseDescription : '',
	
															
														})
													} 
													}
													) ;
												}
										}else{
											this.refs.courseDescription.focus();
											toast.error("Course description is required.")
											}
									}else{
										this.refs.courseFees.focus();
										toast.error("Course fees is required.")
										}
								}else{
									this.refs.subjectName.focus();
									toast.error("Subject name is required.")
									}
							}else{
								this.refs.endDate.focus();
								toast.error("End date is required.")
								}	
						}else{
							this.refs.startDate.focus();
							toast.error("Start date is required.")
							}	
					}else{
						this.refs.courseDuration.focus();
						toast.error("Course duration is required.")
						}						
				}else{
					this.refs.courseType.focus();
					toast.error("Course type is required.")
					}				
			}else{
				this.refs.courseName.focus();
				toast.error("Course name is required.")
				}
		}
	
			HideDivFunction = () =>{
				const {hideDiv} = this.state;
				console.log(hideDiv);
				this.setState({
					hideDiv : !hideDiv
					
				}
				)
			
			}
		
			render(){
				return(
					<div>
						{this.state.hideDiv?
						<div className="col-xs-12">
							<div className="add-course">
								<div className="col-xs-12">
									<div className="box-content card white">
										{this.props.courseId? <h4 className="box-title">Update Course </h4> : <h4 className="box-title">Add Course </h4> }
										<form action="#" method="post">
											<div className="card-content">
												<div className="col-lg-4 col-md-4">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-1" className="btn btn-default"><i className="fa fa-book"></i></label></div>
														<input id="ig-1" type="text" className="form-control" placeholder="Course Name" ref="courseName" defaultValue={this.state.courseName ||this.props.courseName } onChange={(e)=>this.onChangeCourseName(e)}/>
													</div>
												</div>
												<div className="col-lg-4 col-md-4">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-4" className="btn btn-default"><i className="fa fa-adjust"></i></label></div>
														<select id="ig-4" className="form-control" ref="courseType" defaultValue={this.state.courseType ||this.props.courseType } onChange={(e)=>this.onChangeCourseType(e)}>
															<option>Select Course Type</option>
															<option value="Full">Full</option>
															<option value="Short">Short</option>
															<option value="Online">Online</option>
															<option value="Offline">Offline</option>
														</select>
													</div>
												</div>
												<div className="col-lg-4 col-md-4">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
														<input id="ig-3" type="text" className="form-control" ref="courseDuration" placeholder="Course Duration" defaultValue={this.state.courseDuration ||this.props.courseDuration } onChange={(e)=>this.onChangeCourseDuration(e)}/>
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<label>Start Date</label>
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-8" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
														<input type="date" className="form-control" ref="startDate" placeholder="mm/dd/yyyy" id="startDate" defaultValue={this.state.startDate ||this.props.startDate } onChange={(e)=>this.onChangestartDate(e)}/>
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<label>End Date</label>
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-9" className="btn btn-default"><i className="fa fa-calendar"></i></label></div>
														<input type="date" className="form-control"  ref="endDate" placeholder="mm/dd/yyyy" id="endDate" defaultValue={this.state.endDate ||this.props.endDate } onChange={(e)=>this.onChangeendDate(e)}/>
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-book"></i></label></div>
														<input id="ig-3" type="text" className="form-control" ref="subjectName" placeholder="Subject" defaultValue={this.state.subjectName ||this.props.subjectName } onChange={(e)=>this.onChangesubjectName(e)}/>
													</div>
												</div>
												<div className="col-lg-6 col-md-6">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-3" className="btn btn-default"><i className="fa fa-rupee"></i></label></div>
														<input id="ig-3" type="text" className="form-control" ref="courseFees" placeholder="Fees" defaultValue={this.state.courseFees ||this.props.courseFees } onChange={(e)=>this.onChangecourseFees(e)}/>
													</div>
												</div>
												<div className="col-lg-12 col-md-12">
													<div className="input-group margin-bottom-20">
														<div className="input-group-btn"><label htmlFor="ig-5" className="btn btn-default add-btn"><i className="fa fa-clipboard"></i></label></div>
														<textarea id="ig-5" className="form-control" ref="courseDescription" placeholder="Course Description" defaultValue={this.state.courseDescription ||this.props.courseDescription } onChange={(e)=>this.onChangecourseDescription(e)}></textarea>
													</div>
												</div>
												<div className="input-group">
													<div className="col-lg-6 col-md-6">
													{this.props.courseId? <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Update</button> : <button type="submit" className="btn btn-primary btn-sm waves-effect waves-light"  onClick={this.onSubmit}>Submit</button> }
														
													</div>
													<div className="col-lg-6 col-md-6">
														<button id="hide-add-role" type="button" className=" btn btn-danger btn-sm waves-effect waves-light" onClick={()=>this.HideDivFunction()}>Cancel</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>: null}
					</div>
				
				)
			}
		}
	
		
	
	


	export default ManageCourse;