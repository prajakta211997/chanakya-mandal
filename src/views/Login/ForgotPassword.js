import React from 'react';
import logo from '../../assets/images/logo.jpg';
import axios from 'axios';
import {toast} from 'react-toastify';
const apiConfig = require('../../api_config/apiConfig.json');


  const IconSize = {
    width: '170px',
    height : '80px',
      
  }

class ForgotPassword extends React.Component {
    constructor(props){
      super(props);
      this.onChangeEmailId = this.onChangeEmailId.bind(this);
      this.forgotPassword = this.forgotPassword.bind(this);
      this.state = {
        email_id : '',
       
      }
    }
    onChangeEmailId(e) {
      this.setState({
        email_id :  e.target.value
      });
      
    }
   
    forgotPassword(e){
      e.preventDefault();
      if(this.state.email_id !== ''){
        const formData = new FormData();
        formData.append('email_id', this.state.email_id);
          
          axios.post(apiConfig.API_Path +'Login/forgotPassword',formData,{
          // headers: { 'Content-Type': 'application/json,charset=UTF-8' }
          headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        })
          .then(res => 
            {
              if(res.data.status.status == '1'){
               toast.success(res.data.status.message)
                
              }else{
                toast.error(res.data.status.message)
              }
            });
         
        }else{
          this.refs.email_id.focus();
          toast.error("User name is required.")
        }
    }

    render() {
      return (
        <div id="single-wrapper">
            <form action="#" className="frm-single">
                <div className="inside">
                <div className="title">
                    <img src={logo} alt="Chanakya Mandal" title="Chanakya Mandal User Login" style={IconSize} />
                </div>
                <div class="frm-title">Forgot Password</div>
	
					      <p class="text-center">Enter your email address and we'll send you an email with instructions to reset your password.</p>
                <div className="frm-input"><input type="text" ref="email_id" placeholder="Enter Email Id" className="frm-inp" value={this.state.email_id} onChange={this.onChangeEmailId} /><i className="fa fa-user frm-ico"></i></div>
                <input type="submit" value="Send Email" className="frm-submit"  onClick={this.forgotPassword}/>
                <a href="Login" class="a-link"><i class="fa fa-sign-in"></i>Already have account? Login.</a>
                <div className="frm-footer">© Chanakya Mandal Pariwar <script type="text/JavaScript"> document.write(new Date().getFullYear()); </script>.</div>
            </div>
            </form>
        </div>
      );
    }
  }



export default ForgotPassword;