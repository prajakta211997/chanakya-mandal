import React, { Component } from 'react';

const apiConfig = require('../../api_config/apiConfig.json');
	
class Navigation extends React.Component {
		
    constructor(props){
        super(props)
        this.state = {
			
		}
	}
	
    render() {
        if(localStorage.getItem("userLoggedIn")==="False" || localStorage.getItem("userLoggedIn")===null){
            window.location = "login";
        }else{
            return (
            <div className="navigation">
                <h5 className="title">Navigation</h5>
                    <ul className="menu js__accordion">
                        {localStorage.getItem("Dashboard")==="True" ?
                            <li  id="AdminDashboard" className="commonRemove">
                                <a className="waves-effect" href="/#/AdminDashboard"><i className="menu-icon fa fa-dashboard"></i><span>Dashboard</span></a>
                            </li> : null
                        }
                        {localStorage.getItem("Manage User")==="True" ?
                            <li  id="ManageUser" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageUser"><i className="menu-icon fa fa-clone"></i><span>Manage Users</span></a>
                            </li> : null
                        }		
                        {localStorage.getItem("Manage Role")==="True" ?									
                            <li  id="ManageRole" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageRole"><i className="menu-icon fa fa-clone"></i><span>Manage Role</span></a>
                            </li> : null
                        }
                        {localStorage.getItem("Manage Leads")==="True" ?
                            <li  id="ManageLead" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageLead"><i className="menu-icon fa fa-clone"></i><span>Manage Leads</span></a>
                            </li> : null
                        }
                        {localStorage.getItem("Manage Source")==="True" ?
                            <li  id="ManageSource" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageSource"><i className="menu-icon fa fa-clone"></i><span>Manage Source</span></a>
                            </li> : null
                        }
                        {localStorage.getItem("Manage Course")==="True" ?
                            <li id="ManageCourse" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageCourse"><i className="menu-icon fa fa-clone"></i><span>Manage Course</span></a>
                            </li> : null 
                        }
                        {localStorage.getItem("Manage Campaign")==="True" ?
                            <li id="ManageCampaign" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageCampaign"><i className="menu-icon fa fa-clone"></i><span>Manage Campaign</span></a>
                            </li>: null 
                        }
                        {localStorage.getItem("Manage Admission")==="True" ?
                            <li id="ManageAdmission" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageAdmission"><i className="menu-icon fa fa-clone"></i><span>Manage Admissions</span></a>
                            </li>: null
                        }
                        {localStorage.getItem("Manage Access")==="True" ?
                            <li id="ManageAccess" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageAccess"><i className="menu-icon fa fa-clone"></i><span>Manage Access</span></a>
                            </li> : null
                        }
                        {localStorage.getItem("Manage Leaves")==="True" ?						
                            <li id="ManageLeaves" className="commonRemove">
                                <a className="waves-effect" href="/#/ManageLeaves"><i className="menu-icon fa fa-clone"></i><span>Manage Leaves</span></a>
                            </li>: null
                        }

                    </ul>

            </div>
                    
            );
        }
     }
    }
      
	export default Navigation;