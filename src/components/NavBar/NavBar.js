import React from 'react';
import { Link } from 'react-router-dom';

import './NavBar.css';

const NavBar = () => {
  return (
    <div>
      <ul>
        <li><Link to="/AdminDashboard">Admin Dashboard</Link></li>
        <li><Link to="/Login">Login</Link></li>
     	</ul>
      <hr />
    </div>
  );
};

export default NavBar;